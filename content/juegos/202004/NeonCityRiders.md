---
title: 'Neon City Riders'
date: '2020-04-15 22:31:00'
type: 'otros'
idiomas: ["español"]
category: ["accion"]
tags: ["Neon City Riders","Accion","Otros"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1102650/header.jpg'
download: 'https://t.me/c/1118272073/12189'
---


# Neon City Riders

{{< br >}}

¡Explora una ciudad decadente y futurista en busca de objetos, super-poderes y compañeros para liberar todos los territorios de las malvadas pandillas y unir a su gente de nuevo!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1102650/extras/NCR_steam_01.gif" >}}
{{< br >}}

¿Buscas un juego de acción-aventura 2D mezclado con pandillas urbanas súper-poderosas en un escenario neón post-cyberpunk? Bueno, si es así, ¡Neon City Riders es justo para ti!


{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1102650/extras/NCR_steam_02.gif" >}}
{{< br >}}

Toma el rol de Rick, un genial vigilante enmascarado en busca de súper-poderes y compañeros para derrotar hordas de feroces enemigos, resolver desafiantes acertijos, desbloquear nuevos caminos, completar interesantes misiones secundarias, y finalmente, ¡liberar la decadente y futurista Neon City que ha sido aterrorizada por un grupo de pandillas súper-poderosas!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1102650/extras/NCR_steam_03.gif" >}}
{{< br >}}

## Características del juego

{{< br >}}

Pelea en un combate profundo y rápido basado en súper-habilidades desbloqueables.

Explora una progresión de mundo abierto con montones de misiones secundarias para completar el juego a través de uno de los cuatro diferentes finales.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1102650/extras/NCR_steam_04.gif" >}}
{{< br >}}

Sumérgete en las gráficas y sonidos retro-futuristas inspirados en la cultura pop y las consolas de videojuegos de los 80's.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1102650/extras/NCR_steam_05.gif" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Memoria: 4 GB de RAM
* Gráficos: 512 mb video memory
* DirectX: Versión 10
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
