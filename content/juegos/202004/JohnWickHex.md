---
title: 'John Wick Hex'
date: '2020-04-07 21:32:00'
type: 'otros'
idiomas: ["español"]
category: ["accion","estrategia"]
tags: ["John Wick Hex","John Wick","Accion","Estrategia","Otros"]
img: 'https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fjohnwickhex%2Fhome%2Fscreenshot8-2560x1440-144d766640e9195f9a114e3a1e87475fbdb0b62c.jpg?'
download: 'https://t.me/c/1118272073/12071'
---

# John Wick Hex

{{< br >}}

John Wick Hex es un juego de estrategia rápido y orientado a la acción que te hace pensar y golpear como John Wick, el asesino profesional de la franquicia cinematográfica aclamada por la crítica. Creado en estrecha colaboración con los equipos creativos detrás de las películas, John Wick Hex es un juego de ajedrez coreografiado para la lucha que cobró vida como un videojuego, capturando el estilo característico de la serie de armas de fuego mientras amplía su universo de historias. Los jugadores deben tomar decisiones rápidas y elegir cada acción y ataque que realicen, teniendo en cuenta su costo inmediato y sus consecuencias.

Con una combinación única de combate estratégico basado en el impulso, John Wick Hex captura la sensación del combate táctico único de las películas y difumina la línea entre los géneros de videojuegos de estrategia y acción. Desempeña bien y progresa en el modo de historia principal (que presenta una historia original creada para el juego) para desbloquear nuevas armas, opciones de trajes y ubicaciones. Cada arma cambia las tácticas que usarás y la forma en que jugarás. La munición es finita y simulada de manera realista, así que cronometra tus recargas y aprovecha al máximo las armas que buscas en el trabajo.

Experimenta una nueva historia original ambientada antes de los eventos de las películas que ve a John en una misión desesperada para rescatar a Winston y Charon de un nuevo y peligroso adversario que busca su derecho de nacimiento. Esta historia es traída a la vida por los talentos de voz de clase mundial de Ian McShane y Lance Reddick, repitiendo sus papeles icónicos de las películas y la leyenda de la actuación de voz Troy Baker, uniéndose al elenco estelar como el villano epónimo del juego 'Hex'. John Wick Hex complementa el estilo de las películas con un diseño gráfico único de arte negro y una partitura original del reconocido compositor Austin Wintory.

{{< br >}}
{{< img src="https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fjohnwickhex%2Fhome%2FactionScreen2-2560x1440-2134191bcacbcadde9a1706339423a7d0a668c4c.jpg?" >}}
{{< br >}}
{{< img src="https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fjohnwickhex%2Fhome%2FactionScreen1-2560x1440-9a7924e1642792cd2b3b42cde536b0dc17c839fe.jpg?" >}}
{{< br >}}
{{< img src="https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fjohnwickhex%2Fhome%2Fscreenshot5-2560x1440-363bbad6982551e472d52d2ca675b1c9f9e47ce4.jpg?" >}}
{{< br >}}
{{< img src="https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fjohnwickhex%2Fhome%2FNew_Screenshot2-2560x1440-3ea6815c0fd807096ecafdd9e46afd08fbba341f.jpg?" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador Intel i3-6300 3.80Ghz
* Gráficos NVIDIA GTX 960 / AMD Radeon R9 200
* Memoria 8 GB

{{< br >}}
{{< br >}}
