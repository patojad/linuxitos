---
title: 'Stonehearth'
date: '2020-04-07 21:32:00'
type: 'otros'
idiomas: ["ingles"]
category: ["simulacion"]
tags: ["Stonehearth","Simulacion","Otros"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/253250/header.jpg'
download: 'https://t.me/c/1118272073/12278'
---

# Stonehearth

{{< br >}}

Pionero de un mundo vivo lleno de calidez, heroísmo y misterio. Ayuda a un pequeño grupo de colonos a construir una casa para ellos en una tierra olvidada. Establezca un suministro de alimentos, construya un refugio, defienda a su gente, controle su estado de ánimo y encuentre una manera de crecer y expandirse, enfrentando desafíos en cada paso.

{{< br >}}
{{< br >}}

## Construye y haz crecer tu ciudad

{{< br >}}

El corazón del juego es la construcción y gestión de la ciudad. Cuando recién esté comenzando, deberá hacer malabarismos con tareas como obtener un suministro de alimentos sostenible, construir refugios y defender su incipiente asentamiento de los asaltantes y otras amenazas.

Una vez que haya logrado establecerse en el mundo, depende de usted escribir el destino de su gente. Tienes la flexibilidad de elegir tu propio camino en este juego. ¿Quieres construir un gran imperio conquistador? ¿Una ciudad comercial vibrante? ¿Un monasterio espiritual? Realmente queremos que sienta que este es su acuerdo, y que le brinde las herramientas que lo hacen ver y operar exactamente como lo desea.

{{< br >}}
{{< br >}}

## Sube de nivel a tus colonos

{{< br >}}

Todos los colonos de tus pueblos tienen trabajo. Un trabajo es como una clase en un juego de rol. Cada trabajo tiene un papel específico como transportar materiales, construir, fabricar y pelear. A medida que sus compañeros de trabajo trabajen en un trabajo, ganarán experiencia y niveles. Algunos trabajos, cuando cumplen ciertos requisitos previos, pueden actualizarse a trabajos completamente nuevos con nuevas capacidades.

Por lo general, para asignarle a alguien un nuevo trabajo, también deberá diseñar una herramienta para ellos. El masón puede fabricar bloques, estatuas y herramientas de piedra, pero para hacerlo necesitará un mazo y un cincel hechos por el carpintero. Nuestro objetivo es tener un árbol de trabajo que sea muy amplio y muy profundo, por lo que habrá muchas cosas diferentes que hacer en el juego, pero también mucha profundidad para explorar si quieres concentrarte en cualquier área.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Intel or AMD Dual-Core, 1.7 GHz+
* Memoria: 4 GB de RAM
* Gráficos: nVidia GeForce GT 430 512MB, Radeon HD 7570M, Intel HD 4000
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
