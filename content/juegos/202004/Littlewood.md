---
title: 'Littlewood'
date: '2020-04-15 19:45:00'
type: 'gog'
idiomas: ["ingles"]
category: ["rpg"]
tags: ["Littlewood","RPG","GOG"]
img: 'https://images.gog-statics.com/e3aabae9cd29bac432f3b19796bdbe1ff2706139ec9494521f99c6f69ff8472a.jpg'
download: 'https://t.me/c/1118272073/12159'
---

# Littlewood

{{< br >}}

Has derrotado al mago oscuro. El mundo de Solemn finalmente está en paz, pero ¿a qué precio? No puedes recordar ...

{{< br >}}
{{< br >}}

## El héroe que salvó al mundo

{{< br >}}

**Explora** el vasto mundo de Solemn. Los bosques encantados, las bulliciosas ciudades de pesca y las oscuras cuevas mineras son algunos de los pocos lugares para visitar.

{{< br >}}
{{< img src="https://items.gog.com/littlewood/popfinalsmallalt0.gif" >}}
{{< br >}}

**Conoce** a Townsfolk y convéncelos para que se queden en tu ciudad. Quizás conocer gente desbloqueará tus recuerdos del pasado ...

{{< br >}}
{{< img src="https://items.gog.com/littlewood/popfinalsmallalt0.gif" >}}
{{< br >}}

¡**Recoge** objetos y materiales raros que se encuentran en todo el mundo y llévalos a tu ciudad para venderlos y fabricarlos!

{{< br >}}
{{< br >}}

## Personaliza tu ciudad

{{< br >}}

¡**Cosecha** materiales cortando madera, extrayendo minerales, atrapando insectos y pescando en ríos y estanques!

{{< br >}}
{{< img src="https://items.gog.com/littlewood/popfinalsmall1.gif" >}}
{{< br >}}

**Desbloquea** nuevas estructuras para construir conociendo nuevos Townsfolk. ¿Quieres una taberna o un aserradero? ¿Qué tal una cabaña de pesca o una biblioteca mágica? ¡El diseño de tu ciudad depende de ti!

{{< br >}}
{{< img src="https://items.gog.com/littlewood/popfinalsmall1.gif" >}}
{{< br >}}

**Cumplir** con las solicitudes de su Townsfolk. Algunos pueden querer vivir en una elevación más alta, otros pueden estar rodeados de árboles y agua. Cumplir con las solicitudes de tu Townsfolk te hará ganar sus corazones y desbloquear nuevos caminos de relación.

{{< br >}}
{{< img src="https://items.gog.com/littlewood/skillbarSmall.gif" >}}
{{< br >}}
{{< br >}}

## Domina nuevos pasatiempos y manualidades

{{< br >}}

**Recolección:** ¡Ya sea que esté recogiendo fruta o recogiendo malezas, su habilidad de recolección aumentará de nivel! Se necesita el guante del recolector para desbloquear esta habilidad.

**Minería:** ¿busca minerales brillantes para recolectar? ¿Interesado en materiales raros para la elaboración? ¡Entonces la minería es para ti!

**Tala de madera:** ¡Es necesario cortar esos molestos árboles y escombros que siguen apareciendo por toda la ciudad! La madera es un material esencial para construir nuevas estructuras y muebles.

{{< br >}}
{{< img src="https://items.gog.com/littlewood/popfinalsmall2.gif" >}}
{{< br >}}

**Captura de insectos:** crea una red de insectos y atrapa todo tipo de bichos. ¡Asegúrese de cazar insectos durante cada temporada y en diferentes momentos del día!

**Agricultura:** Planta algunas semillas en tierra labrada y espera unos días. ¡Las verduras son un ingrediente esencial para muchas recetas de cocina! Además, hacen que tu ciudad se vea bonita.

**Cocinar:** una vez que hayas construido la Taberna y te hayas convertido en un buen amigo de Bubsy, ¡podrás comenzar a cocinar deliciosas comidas para tu Townsfolk! Tal vez pueda atraer a Townsfolk raros sirviendo platos únicos ...

**Pesca:** No hay nada como relajarse junto a un estanque con una caña de pescar en la mano. Puede vender pescado a un precio decente en el distrito comercial, o utilizarlos como ingredientes para nuevos platos de cocina.

{{< br >}}
{{< img src="https://items.gog.com/littlewood/popfinalsmall2.gif" >}}
{{< br >}}

**Elaboración:** hay toneladas de estructuras, monumentos decorativos y muebles para elaborar. Muchas veces su Townsfolk solicitará objetos específicos que les gustaría colocar en su hogar. ¡Asegúrate de reunir suficientes materiales de construcción!

**Comerciante:** la venta de artículos en el distrito comercial le proporcionará grandes sumas de gotas de rocío, que es la moneda principal en Solemn. A medida que te conviertas en un mejor comerciante, ¡aparecerán artículos más raros para que los compres!

**Tarott Master:**  una vez que el Dark Wizard fue derrotado, todos sus monstruos fueron sellados en Tarott Cards. Muchos ciudadanos recolectan y luchan con estas cartas, e incluso hay una gran competencia cada año en Dirboros. ¡A recoger!

{{< br >}}
{{< br >}}

## Hacer nuevos amigos

{{< br >}}
{{< img src="https://items.gog.com/littlewood/popfinalsmall3.gif" >}}
{{< br >}}

**Willow:** Una joven alegre que quiere reconstruir Littlewood. Te encontró en el medio del bosque y te cuidó durante 3 días mientras estabas inconsciente. Le gusta trabajar con herramientas y es bastante competente en la construcción de cosas.

**Dalton**: Un niño tonto al que le encanta ir de aventuras contigo. A pesar de su olvido y su comportamiento tonto, en realidad puede contar algunos chistes muy inteligentes. Le gusta pescar, atrapar insectos y comprar muebles geniales.

{{< br >}}
{{< img src="https://items.gog.com/littlewood/popfinalsmall3.gif" >}}
{{< br >}}

**Lilith:** una maga que se graduó como la mejor de su clase en la Academia Mágica de Alejandría. Cuando no está leyendo un libro, está mezclando cosas al azar para hacer pociones que pueden explotar o no. Además, ella es tu mayor fan.

¡Y muchos más!

{{< br >}}
{{< img src="https://items.gog.com/littlewood/portraitbar.png" >}}
{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* Sistema: Ubuntu 16.04+
* Procesador: 2,0 Ghz
* Memoria: 1 GB de RAM
* Gráficos: Memoria de video de 128mb, compatible con OpenGL 3.0+ (2.1 con extensiones ARB aceptables)
* Almacenamiento: 1 GB de espacio disponible

{{< br >}}
{{< br >}}
