---
title: 'Bloody Rally Show'
date: '2020-04-12 15:12:00'
type: 'gog'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["Bloody Rally Show","Aventura","GOG"]
img: 'https://images.gog-statics.com/8e2bcb9d8d0fff1587d3b0eb6abbf5634dd75511a4d03c17e6658d375f70b123_bg_crop_1366x655.jpg'
download: 'https://t.me/c/1118272073/12121'
---

# LUNA The Shadow Dust

{{< br >}}

Inspirado en los juegos de aventuras de antaño, LUNA The Shadow Dust es una conmovedora historia de dos compañeros jugables reunidos en una aventura de rompecabezas animada a mano, con una banda sonora original impresionante y hermosas cinemáticas en 2D.

{{< br >}}
{{< br >}}
{{< img src="https://items.gog.com/luna_the_shadow_dust/Page_Title_Story_EN.png" >}}
{{< br >}}

## Encender una vela es proyectar una sombra

{{< br >}}

Detrás de la sombra de la realidad, un mundo encantado espera la iluminación. Experimente el viaje mágico de un niño y su compañero mientras resuelven acertijos y remontan los recuerdos de antaño, traídos a la vida con cinemáticas sin palabras y visualmente impresionantes.
Ingrese a la antigua torre que se encuentra en el borde del mundo y descubra las cinemáticas dibujadas a mano, los intrincados rompecabezas y la música inquietante de esta joya indie.

{{< br >}}
{{< br >}}
{{< img src="https://items.gog.com/luna_the_shadow_dust/Page_Title_KeyFeatures_EN.png" >}}
{{< br >}}

## Animación tradicional de personaje cuadro por cuadro

{{< br >}}

12 cuadros por segundo, 3 capas por cuadro. Más de 250 animaciones y 20 minutos de cinemática. Un proceso nostálgico y lento, pero realmente vale la pena el desafío para un pequeño equipo independiente de cuatro.

{{< br >}}
{{< br >}}

## Modo para un jugador con control de doble personaje

{{< br >}}

Resuelve acertijos desde diferentes perspectivas. Descubre una historia de verdadero coraje con dos personajes jugables unidos a través de un vínculo inseparable.

{{< br >}}
{{< br >}}

## Piensa fuera de la caja

{{< br >}}

Despierta tu imaginación y resuelve una gran variedad de rompecabezas, presentados en magníficos entornos dibujados a mano. Los acertijos funcionan de forma independiente y no requieren 'elementos ocultos' ni retroceso de jugador.

{{< br >}}
{{< br >}}

## MENOS lectura, MÁS cinemática

{{< br >}}

Reemplazamos un sistema de diálogo con una serie de cinemáticas bellamente animadas a mano que cuentan una historia misteriosa.

{{< br >}}
{{< br >}}

## Involucrar banda sonora original

{{< br >}}

Nuestro compositor creó una banda sonora original, diseñada para sumergir al jugador en los altibajos emocionales de la historia, avivando la piel de gallina a lo largo del viaje.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema: Ubuntu 16.04, 18.04 o posterior
* Procesador: 1,2 GHz
* Memoria: 4 GB de RAM
* Gráficos: 1024 MB de VRAM
* Almacenamiento: 4 GB de espacio disponible

{{< br >}}
{{< br >}}
