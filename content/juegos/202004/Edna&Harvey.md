---
title: 'Edna & Harvey: The Breakout - Anniversary Edition'
date: '2020-04-19 17:45:00'
type: 'gog'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["Edna & Harvey: The Breakout","Anniversary Edition","Aventura","GOG"]
img: 'https://images.gog-statics.com/dc86884f633ef8ad0b23e2a229c4415f1373516950cae9f1376872060af48b26.jpg'
download: 'https://t.me/c/1118272073/12205'
---

# Edna & Harvey: The Breakout - Anniversary Edition

{{< br >}}

Al principio, la joven Edna se despierta en la celda acolchada de un manicomio sin recordar su pasado. Ella no tiene la menor idea de cómo terminó allí, pero una cosa es segura, ¡quiere salir! Después de todo, se siente perfectamente cuerda, y su conejito de peluche Harvey está completamente de acuerdo. Juntos, intentan escapar y, al hacerlo, se encuentran con sus pacientes extremadamente extraños, desde el hombre disfrazado de abeja hasta los gemelos pseudo-siameses Hoti y Moti. Pero el Dr. Marcel, jefe del manicomio, no se detendrá ante nada para evitar que Edna escape. ¿Qué ha planeado para ella? ¿Y por qué se borró su memoria? Gradualmente, Edna recupera su memoria del tiempo antes de su detención en su celda ...

{{< br >}}

* Más de 20 horas de humor retorcido.
* Personajes increíblemente divertidos y diálogos locos.
* Estilo cómico único.

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* Procesador: 2 GHz
* Memoria: 1 GB de RAM
* Gráficos: Tarjeta gráfica de 256 MB compatible con DirectX 9
* Almacenamiento: 3GB HDD

{{< br >}}
{{< br >}}
