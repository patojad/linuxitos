---
title: 'In Other Waters: Xenobiologist Edition'
date: '2020-04-07 15:21:00'
type: 'steam'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["In Other Waters","Xenobiologist Edition","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/890720/header.jpg'
download: 'https://t.me/c/1118272073/12054'
---

# In Other Waters: Xenobiologist Edition

Eres una inteligencia artificial que guía a una xenobióloga a la deriva en un hermoso y misterioso océano extraterrestre. ¿Qué descubriréis juntos?

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/890720/extras/IOW_slowed_header_-_600px_wide.gif" >}}
{{< br >}}

En otras aguas cuenta la historia de Ellery Vas, una xenobióloga que descubre vida extraterrestre mientras busca a su compañera desaparecida.

Ellery recibe la llamada de Minae Nomura para que se dirija al planeta Gliese 677Cc, al llegar solo encuentra una base abandonada, se encuentra a la deriva en un océano de secretos, con poco más que un traje de buceo que a penas funciona y una extraña IA que la guía. Esa IA eres tú.

Guía a Ellery y mantenla a salvo mientras os adentráis y exploráis las profundidades del océano alienígena. Las formas de vida únicas del planeta y su oscura historia están a tu disposición para ser desveladas, el lazo entre Ellery y tú se pondrá a prueba con cada secreto que descubráis. La vibrante narrativa de En otras aguas plantea preguntas sobre la naturaleza y las formas de vida «naturales» y «artificiales», e indaga en lo que supone ser un humano en plena época de destrucción medioambiental extrema. Para que la vida continúe, esto debe cambiar.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/890720/extras/Divider.png" >}}
{{< br >}}

## Explora un océano extraterrestre

{{< br >}}

Muévete con libertad en cualquier lugar y descubre nuevas zonas por el camino. Estos descubrimientos te conducirán hasta mejoras y muestras que desbloquearán a su vez nuevas zonas en lugares que ya hayas visitado, misiones secundarias adicionales y conversaciones únicas con Ellery.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/890720/extras/IOW_Convo_brine_600px.gif?t=1586190433" >}}
{{< br >}}

## Observa el mundo desde la perspectiva de una IA

{{< br >}}

Navega por una interfaz de usuario elegante e intuitiva, ya sea con controles táctiles o con el ratón. Interpreta señales, marca el rumbo y experimenta y déjate llevar por la intuición para mapear el océano.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/890720/extras/IOW_Propulsion_short_600px.gif" >}}
{{< br >}}

## Conviértete en una xenobióloga

{{< br >}}

Descubre y cataloga especies por observación, mediante escáner o tomando muestras e interactuando con la vida extraterrestre. Lee las notas de Ellery, observa sus esquemas y ayúdala a clasificar un nuevo ecosistema.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/890720/extras/IOW_Analysis2_600px.gif" >}}
{{< br >}}

## Planea tus inmersiones

{{< br >}}

Elige entre estudiar criaturas, investigar secretos o bucear en zonas nuevas; después vuelve a la base y estudia las muestras en el laboratorio, habla con Ellery o lee su diario para entender mejor su mundo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/890720/extras/IOW_Bloom_dive_600px.gif" >}}
{{< br >}}

## Construye una relación duradera

{{< br >}}

A pesar de tener comunicación limitada, conoce mejor a la persona que lleva el traje en el que habitas y ayúdala a tomar decisiones difíciles. Todo lo que experimentéis os unirá cada vez más.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/890720/extras/IOW_question_animated_600px.gif" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Requiere un procesador y un sistema operativo de 64 bits
* Procesador: 2.0 GHz
* Memoria: 4 GB de RAM
* Gráficos: 2 GB VRAM
* Almacenamiento: 700 MB de espacio disponible

{{< br >}}
{{< br >}}
