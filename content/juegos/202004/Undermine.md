---
title: 'Undermine'
date: '2020-04-15 22:19:00'
type: 'otros'
idiomas: ["español"]
category: ["accion","rpg","aventura"]
tags: ["Undermine","Accion","RPG","Aventura","Otros"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/656350/header.jpg?t=1585338849'
download: 'https://t.me/c/1118272073/12172'
---


# Undermine

{{< br >}}

Un roguelike de acción y aventura con un poco de juego de rol. Aventúrate en las profundidades de UnderMine y descubre poderosas reliquias, enemigos mortales, secretos ocultos y algunos amigos para ayudar en el camino.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/656350/extras/Witch.jpg" >}}
{{< br >}}

Combinando combate y mazmorras arrastrándose de roguelikes como "The Binding of Isaac", con la progresión de roguelites como "Rogue Legacy", UnderMine ofrece una nueva entrada al género. Extrae oro en cada carrera y aumenta el poder del campesino al encontrar poderosas reliquias. Rescata a los NPC y devuélvelos al centro donde abrirán la tienda y ofrecerán poderosas actualizaciones para futuras carreras. Profundiza, explora cada nueva área, descubre sus muchos secretos y finalmente vence a su poderoso jefe.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/656350/extras/BuildItemCombos.gif" >}}
{{< br >}}

Descubre reliquias, pociones, oraciones, bendiciones e incluso maldiciones para forjar esa carrera perfecta. Observe cómo los elementos aparecen y se combinan entre sí para convertir a un dios en un campesino de la destrucción.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/656350/extras/RescueCharacters.gif" >}}
{{< br >}}

Descubre personajes amigables (y algunos hostiles) que necesitan rescate. Después de devolverlos a salvo al centro de la mina, ofrecerán actualizaciones potentes que se pueden utilizar de una ejecución a otra.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/656350/extras/DiscoverSecrets.gif" >}}
{{< br >}}

Explora cada rincón y grieta para descubrir cientos de secretos. Nuevas reliquias, pociones, personajes y fragmentos de historias yacen detrás de las estatuas, rocas y paredes de cada piso.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/656350/extras/DeadlyBosses.gif" >}}
{{< br >}}

Cada área de UnderMine contiene uno (¡o más!) Jefe mortal que pondrá a prueba la planificación, la paciencia, la estrategia y la habilidad. ¡Prepárate bien, porque una prueba te espera!

{{< br >}}
{{< br >}}

## Darklyhearted

{{< br >}}

UnderMine tiene un mundo rico para explorar y descubrir con personajes encantadores y una trama llena de intriga. Experimente la difícil situación del campesino pobre enviado a la mina de oro. Su trabajo es buscar un jefe que no se preocupe por él, en un ambiente abiertamente hostil y entre criaturas que quieran arrancarle la carne de los huesos. Una situación bastante divertida cuando lo piensas y es algo con lo que todos podemos relacionarnos.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04
* Procesador: 2.4 GHz
* Memoria: 4 GB de RAM
* Gráficos: Integrated GPU or better (1024 MB)
* Almacenamiento: 2 GB de espacio disponible
* Notas adicionales: 1080p, 16:9 recommended

{{< br >}}
{{< br >}}
