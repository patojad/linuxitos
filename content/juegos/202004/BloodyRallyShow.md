---
title: 'Bloody Rally Show'
date: '2020-04-10 13:53:00'
type: 'steam'
idiomas: ["ingles"]
category: ["deportes","carreras"]
tags: ["Bloody Rally Show","Deportes","Carreras","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/926860/header.jpg'
download: 'https://t.me/c/1118272073/12106'
---

# Bloody Rally Show

{{< br >}}

Es un juego distópico de carreras de combate roguelita con una variedad infinita de pistas de carreras, campañas, misiones, tablas de clasificación, desafíos diarios, editor de pistas, editor de automóviles, ajuste de automóviles, personalización, batallas de automóviles y múltiples modos de juego, incluido uno en el que eres un peatonal.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/926860/extras/BRSSteamRace.gif" >}}
{{< br >}}

Una combinación única de carreras, coches de combate y roguelite te ofrece una variedad infinita de pistas de carreras y campañas generadas por procedimientos con historias distópicas y conversaciones, misiones y desafíos generados por la IA.

Conduce coches rápidos con controles satisfactorios en una variedad infinita de pistas generadas al azar y hechas a mano.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/926860/extras/BRSSteamFight1.gif" >}}
{{< br >}}

Usa armas y la inercia de tu coche para luchar contra otros corredores en el modo de batalla estilo libre. Agrega zonas de armas a las razas regulares para darle vida a las cosas

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/926860/extras/BRSSteamDrift.gif" >}}
{{< br >}}

Cuidadosamente ajustado, el manejo del automóvil basado en la física está en el punto óptimo entre arcade y simulación, fácil de manejar, difícil de dominar. Aprenda a manejar y derivar sus autos en diferentes superficies, sentir cuando su auto pierde agarre y recupera la tracción.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/926860/extras/BRSSteamCreate.gif" >}}
{{< br >}}

Crea tus propias pistas de carreras y autos con editores avanzados en el juego que se usaron para crear todo el contenido del juego. Las herramientas completas para desarrolladores están al alcance de su mano, y están hechas lo más fácil de usar posible.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/926860/extras/BRSSteamCustomize.gif" >}}
{{< br >}}

Usa tus propias imágenes para avatares de corredores y trabajos de pintura de automóviles, cambia los nombres de tus oponentes en modo campaña o carrera personalizada. Personalice el trabajo de pintura de su automóvil agregando capas de detalles. Haz tuyo este juego.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/926860/extras/BRSConsole.gif" >}}
{{< br >}}

Usa un sistema operativo incorporado (OK, OK, es solo una consola), donde puedes realizar experimentos que ajustan la física del juego, cambian la lógica del juego y hacen otras cosas geniales.

{{< br >}}
{{< br >}}

## Características clave:

{{< br >}}

* Física de autos estilo arcade / sim satisfactoria (manejo, deriva, velocidad)
* Cantidad infinita de circuitos de carreras generados aleatoriamente
* Modo de campaña con misiones, desafíos y narrador de IA
* Modo de batalla de coches con área de reducción de estilo Battle Royal
* ¡Modo peatonal, nunca antes visto en otros juegos de carreras!
* Mecánico estratégico de impulso nitro
* Carreras con armas y poderes especiales.
* Juega campañas en modo individual, cooperativo local en sofá o Steam Remote Play
* Racer XP y subir de nivel
* Tuning y actualizaciones de automóviles
* Los oponentes de IA adaptativos que tienen los mismos controles basados ​​en la física que el jugador, tienen emociones y carácter, y hacen cosas estúpidas de vez en cuando
* Casi 50 logros Gran cantidad de diferentes tablas de clasificación
* Los nombres de los mejores jugadores de tablas de clasificación aparecen en las campañas de jugadores como corredores de IA
* Desafío diario con una combinación diferente de pista / automóvil para competir contra otros jugadores.
* Configuración para apagar la sangre (para sesiones familiares de carreras de pantalla dividida)
* ¡Carreras con 100 oponentes!
* Rampas y saltos, charcos, derrames de petróleo, parches de hielo, barriles explosivos y otros peligros.
* Apoyos basados ​​en física destruible y móvil
* OST de alta calidad con docenas de canciones completas
* Referencias al pan spelta sin gluten y otros chistes malos
* ¡Sin DRM!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04+, SteamOS+
* Procesador: Dual-Core Intel or AMD processor
* Memoria: 4 GB de RAM
* Gráficos: Graphics card with DX10 (shader model 4.0) capabilities
* Almacenamiento: 220 MB de espacio disponible
* Notas adicionales: Potato friendly

{{< br >}}
{{< br >}}
