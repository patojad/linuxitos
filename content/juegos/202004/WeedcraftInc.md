---
title: 'Weedcraft Inc'
date: '2020-04-30 19:07:00'
type: 'steam'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Weedcraft Inc","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/622720/header.jpg'
download: 'https://t.me/c/1118272073/12257'
---

# Weedcraft Inc

{{< br >}}

Explora el negocio de la producción, cultivo y venta de marihuana en Estados Unidos ahondando en los aspectos financieros, políticos y culturales de la compleja relación del país con esta problemática y prometedora planta.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/622720/extras/grow23.gif" >}}
{{< br >}}

Los futuros magnates de la marihuana tendrán que priorizar sus recursos cuidadosamente mientras controlan la producción y distribución de su hierba. Cultiva plantas brutales, crúzalas entre sí para crear cepas únicas de la lechuga del Diablo, contrata y gestiona el crecimiento de tu personal y estáte preparado para enfrentarte a la competencia. Como en la vida real, el negocio de la hierba está repleto de oportunidades pero también de todo tipo de problemas. Decide el cómo y cuándo asumir algún riesgo extra para maximizar tus beneficios pero ten cuidado, la policía y los políticos también trabajan para su propio beneficio.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/622720/extras/deliver5.gif" >}}
{{< br >}}

Weedcraft Inc presenta un importante, y controvertido, tema de una forma detallada y profunda que supondrá un reto para todos aquellos que legislarían los porros, ofreciendo una serie de diversos escenarios y personajes únicos con los que trabajar (o evitar) mientras tu empresa de la hierba crece. Tanto si eliges codearte con los políticos, untar a la policía, luchar por los derechos de la marihuana medicinal o trabajar en los márgenes de la legalidad será todo en el nombre de los dedos pegajosos y tus despiadados beneficios económicos.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/622720/extras/money1.gif" >}}
{{< br >}}
{{< br >}}

## Caracteristicas

{{< br >}}

* Incluye un modo sandbox personalizable, tres nuevos escenarios y el modo Relax que lo hace todo más sencillo.
* Controversia presentada con conocimiento de causa.
* Gestiona la economía y la distribución de tu negocio de hierba.
* Las elecciones importan. Los personajes tienen sus propias motivaciones y su propia trama contada con humor y pasión.
* Equipa tu sala de cultivo para lograr la mejor cosecha posible y controla todos los factores; ajusta los nutrientes, el agua y poda tus plantas, ¡incluso puedes crear cepas completamente nuevas!
* Cambia las leyes, cambia la sociedad y cambia la manera en la que el mundo a tu alrededor percibe tu producto.
* Acata las normas, rómpelas o simplemente dóblalas un poco. La moralidad es una escala cambiante en Weedcraft Inc.
* Contrata y despide empleados, molesta a tu competencia y a la ley con todos los medios a tu alcance. O mantén a tus enemigos cerca y haz nuevos amigos, la decisión es tuya.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/622720/extras/cult3.gif" >}}
{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Intel Core i3-4160 (2 * 3600) or equivalent / AMD Athlon II X4 645 AM3 (4 * 3100) or equivalent
* Memoria: 4 GB de RAM
* Gráficos: GeForce GTX 460 (1024 MB) / Radeon R7 260X (2048 MB) / Intel HD 530
* DirectX: Versión 10
* Almacenamiento: 6 GB de espacio disponible

{{< br >}}
{{< br >}}
