---
title: 'The Letter'
date: '2020-04-26 13:58:00'
type: 'steam'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["The Letter","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/460430/header.jpg'
download: 'https://t.me/c/1118272073/12222'
---

# The Letter

{{< br >}}

Cuando siete personas caen presas de una maldición cruel, descubrirán que el horror no descansa de las sombras que acechan, sino dentro de sí mismos. ¿A qué fines irá uno para sobrevivir?

{{< br >}}
{{< br >}}

## ADVERTENCIA DE CONTENIDO

{{< br >}}

Este juego trata temas maduros. Muestra escenas que algunos pueden encontrar inquietantes, poco éticas y desencadenantes. También contiene lenguaje ofensivo, insultos raciales, temas sexuales, gore y violencia. Se aconseja discreción del jugador. Para obtener la lista completa de advertencias de activación, consulte nuestros foros de Steam.

{{< br >}}
{{< br >}}

## ACERCA DE The Letter

{{< br >}}

Es una novela visual interactiva de terror/drama inspirada en películas de terror asiáticas clásicas, Ju-ON: The Grudge y The Ring. Contada a través de una narrativa inmersiva y ramificada, el juego pone a los jugadores en la piel de siete personajes, ya que se encuentran cayendo presa de una cruel maldición que asola la Mansión Ermengarde durante siglos. En este juego, tus elecciones darán forma e impulsarán la historia: ¿repararás una relación al borde del colapso, o la dejarás desmoronarse? ¿Te arriesgarás a salvarlos, a pesar de las consecuencias, o los dejarás morir de una muerte espantosa? El destino de todos los personajes está en tus manos.

{{< br >}}
{{< br >}}

## CARACTERISTICAS

{{< br >}}

* Esta es una novela visual hardcore con más de 700K palabras.
* ¡Espera mucha lectura! Romance, amistad y drama; Además del horror, el juego pone gran énfasis en las relaciones y el desarrollo del personaje.
* Narración no cronológica con 7 personajes jugables, cada uno con personalidad variable y enfoque a situaciones difíciles.
* Innumerables efectos de mariposa; Sus elecciones afectan en gran medida la historia.
* Interpretación completa de voz en inglés (opcional).
* Fondos animados, CG y sprites.
* Estilo de arte bellamente pintado, con más de 80 fondos, 100 CG y 50 CG de Epílogo (variaciones incluidas).
* Banda sonora original, con temas de apertura, final y verdadero final.
* Eventos de tiempo rápido (opcional).
* Un árbol ramificado que realiza un seguimiento de su progreso/ruta.
* Un diario que narra los eventos en orden cronológico y anota las pistas encontradas sobre la Mansión Ermengarde.

{{< br >}}
{{< br >}}

## REQUISITOS DEL SISTEMA

{{< br >}}

* SO: Ubuntu 12.04 or newer
* Procesador: Intel® Core 2 Duo
* Memoria: 2 GB de RAM
* Gráficos: GTX 260 or equivalent
* Almacenamiento: 5 GB de espacio disponible

{{< br >}}
{{< br >}}
