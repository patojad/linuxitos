---
title: 'Scythe: Digital Edition'
date: '2020-04-14 22:25:00'
type: 'steam'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Scythe","Digital Edition","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/718560/header.jpg'
download: 'https://t.me/c/1118272073/12143'
---


# Scythe: Digital Edition

{{< br >}}

Scythe te traslada a una realidad alternativa, con una Europa devastada por la Primera Guerra Mundial durante la década de los años 20. Lidera una de las cinco facciones existentes y lánzate a la conquista de la codiciada Fábrica. ¡Despliega tus «mechs» en una guerra por la supremacía de tu pueblo!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/718560/extras/logo-scythe-animated.gif" >}}
{{< br >}}

Europa, años 20. En una realidad alternativa, la Gran Guerra se libró ya hace años, pero las ascuas del conflicto nunca terminaron de apagarse y los tambores de guerra vuelven a sonar. El primer asalto de la contienda alumbró unas increíbles máquinas de guerra llamadas «mechs».

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/718560/extras/scythe-keyart.gif" >}}
{{< br >}}

Estas monstruosidades tecnológicas son ensambladas por la Fábrica, una ciudad-Estado independiente que desde entonces se ha convertido en objeto de deseo de todas las potencias, y campan por las zonas nevadas de Europa.
Cinco son las facciones en combate: el Imperio de Sajonia, el Kanato de Crimea, la Unión Rusoviética, la República de Polania y los Reinos Nórdicos. Conviértete en el héroe de una de ellas y guíala en estos tiempos aciagos hasta que se alce como la nación más próspera y poderosa de toda Europa oriental. Las claves de la victoria pasan por explorar y conquistar nuevos territorios, incorporar nuevos reclutas y desarrollar formidables «mechs» de combate para desplegarlos. Revive la historia desde un punto de vista ficticio lleno de criaturas mecánicas y tecnología, donde cada decisión que tomes tendrá consecuencias críticas. No elijas a la ligera qué batallas afrontar, ya que en Scythe el camino a la victoria se anda de la mano de tu pueblo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/718560/extras/20190325-scythe-steam-newlook-factions.png" >}}

{{< br >}}
{{< br >}}

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/718560/extras/main-features-SP.png" >}}

{{< br >}}

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/718560/extras/features-SP.png" >}}

{{< br >}}
{{< br >}}

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/718560/extras/gameplay-SP.png" >}}
{{< br >}}

**Asimetría**: Cada jugador comienza con recursos diferentes (energía, monedas, aptitud para la batalla, popularidad, etc.), un punto de origen diferente y un objetivo secreto. Los puntos de origen se fijan específicamente para resaltar la naturaleza única de cada facción y el componente asimétrico del juego.

**Estrategia**: Scythe pone por completo en las manos de los jugadores su propio destino. Lo único que dejamos al azar, aparte de las cartas de objetivo secreto individuales, son las cartas de encuentro, que los jugadores descubren para interactuar con los ciudadanos de los nuevos territorios explorados. El combate también se desarrolla a partir de decisiones, nunca del azar o de la suerte.

**Construcción**: Los jugadores pueden mejorar sus habilidades de construcción para ser más eficientes, construir estructuras que refuercen su presencia en el mapa, incorporar a nuevos reclutas a su facción, activar sus «mechs» para evitar invasiones y lanzarse a la conquista de nuevos territorios para hacerse con más recursos de distintos tipos. Esta característica genera una sensación de energía y progreso mientras se juega. El orden en el que los jugadores eligen desarrollar la economía y la tecnología aporta otro toque único a cada partida, por lo que incluso jugar con una misma facción nunca es igual.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Dual Core 3.0 GHz
* Memoria: 4 GB de RAM
* Gráficos: DirectX 11 class GPU with 1024MB VRAM
* DirectX: Versión 11
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
