---
title: 'Bud Spencer & Terence Hill - Slaps And Beans'
date: '2020-04-26 14:56:00'
type: 'steam'
idiomas: ["español"]
category: ["accion"]
tags: ["Bud Spencer & Terence Hill","Slaps And Beans","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/564050/header.jpg'
download: 'https://t.me/c/1118272073/12241'
---

# Bud Spencer & Terence Hill - Slaps And Beans

{{< br >}}

En su primera aventura virtual. Tortazos y Judías es un beat 'em up para uno o dos jugadores en modo cooperativo, con plataformas y minijuegos, en el que te meterás en el papel de los personajes de Bud Spencer y Terence Hill.

{{< br >}}
{{< br >}}

## ¡El primer videojuego OFICIAL de Bud Spencer y Terence Hill!

{{< br >}}

Bud Spencer y Terence Hill en su primera aventura. ¡Una historia totalmente nueva! Ya sea en el Saloon del Viejo Oeste, el centro de Miami, o el parque de atracciones, tendrás una dosis completa de diálogos descacharrantes, tortazos, y por supuesto peleas en grupo.

Tortazos y Judías es un beat 'em up para uno o dos jugadores en modo cooperativo, con plataformas y minijuegos, en el que te meterás en el papel de los personajes de Bud Spencer y Terence Hill.

{{< br >}}
{{< br >}}

## Los ingredientes clave de Tortazos y Judías son:

{{< br >}}

* Gráficos pixelados retro
* Sistema de pelea al estilo Bud Spencer & Terence Hill
* Modo cooperativo de dos jugadores
* Minijuegos
* Un cochecito rojo con la capota amarilla
* Montones de tortazos y de judías (obviamente)

{{< br >}}
{{< br >}}

## REQUISITOS DEL SISTEMA

{{< br >}}

* SO: Ubuntu 14.04 or later
* Procesador: Intel Core2 Duo E4500 or equivalent
* Memoria: 2 GB de RAM
* Gráficos: GeForce GT 640M or equivalent
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
