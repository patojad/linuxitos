---
title: 'Infested Planet'
date: '2020-04-09 17:51:00'
type: 'otros'
idiomas: ["ingles"]
category: ["estrategia"]
tags: ["Infested Planet","Estrategia","Otros"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/204530/header.jpg'
download: 'https://t.me/c/1118272073/12089'
---

# Infested Planet

{{< br >}}

Dirige un equipo de 5 soldados de élite contra una horda alienígena de 100.000. Rodeado por todos lados, debes superar y superar al enemigo.

El enemigo se está acercando a tu alrededor. Sus soldados están siendo flanqueados y el perímetro se derrumba lentamente bajo un asalto alienígena vicioso. Las torretas robóticas no durarán mucho más. Solo un poco más y los insectos pulularán en tu base desprotegida. ¿Cuáles son tus pedidos?

Ordena a tus soldados que vuelvan a un punto de estrangulamiento defensivo y reconstruyan tus defensas. Sacrificar a los escuadrones delanteros para ganar algo de tiempo extra. Reúna a su equipo mientras los equipa con lanzallamas y pistolas láser. Comienza a construir un cañón de asedio para apoyo de fuego. Luego, mientras los alienígenas están ocupados, flanquean sus colmenas principales para destruirlos y capturarlos.

Acaba de lograr una victoria táctica, pero no hay tiempo para descansar.

Los extraterrestres están mutando constantemente. En un momento tus escopetas están atravesando la horda, al siguiente los alienígenas crecen armadura dura a prueba de balas. Debes adaptarte también. Equipa a tu equipo con trajes de sigilo, miniguns, lanzagranadas: tus comandos decidirán el resultado de la batalla.

{{< br >}}
{{< br >}}

## Caracteristicas

{{< br >}}

* Combate táctico en tiempo real
* Una larga campaña donde desarrollas nuevas armas
* Los mapas de procedimiento y el sistema de mutación aleatoria mantienen el juego fresco
* 21 armas y edificios humanos para que los manejes
* 33 mutaciones alienígenas para mantenerte alerta
* Lucha contra miles de enemigos a la vez

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Processor: 2.2 GHz
* Memory: 1 GB RAM
* Graphics: 256MB OpenGL 2.1 Compartible
* Hard Drive: 300 MB HD space

{{< br >}}
{{< br >}}
