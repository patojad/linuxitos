---
title: 'Hades'
date: '2020-04-02 18:28:00'
type: 'otros'
idiomas: ["español"]
category: ["accion"]
tags: ["Hades","Accion","Otros"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1145360/header_alt_assets_2.jpg'
download: 'https://t.me/c/1118272073/12006'
---

# Hades

{{< br >}}

Desafía al dios de los muertos mientras cortas y recortes del inframundo en este rastreador de mazmorras de los creadores de Bastion, Transistor y Pyre.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1145360/extras/HadesSteamA2.gif" >}}
{{< br >}}

Hades es un rastreador de mazmorras como un dios mafioso que combina los mejores aspectos de los títulos aclamados por la crítica de Supergiant, incluida la acción trepidante de Bastion, la rica atmósfera y la profundidad de Transistor, y la narración de Pyre basada en los personajes.

{{< br >}}
{{< br >}}

## BATALLA FUERA DEL INFIERNO

{{< br >}}

Como el Príncipe inmortal del inframundo, ejercerás los poderes y las armas míticas del Olimpo para liberarte de las garras del dios de los muertos, mientras te fortaleces y desentrañas más de la historia con cada intento de escape único.

{{< br >}}
{{< br >}}

## Desata la furia de Olympus

{{< br >}}

¡Los olímpicos te respaldan! Conoce a Zeus, Atenea, Poseidón y muchos más, y elige entre sus docenas de poderosas bendiciones que mejoran tus habilidades. Hay miles de construcciones de personajes viables para descubrir a medida que avanzas.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1145360/extras/ThreeHeadedBoy_Short.gif" >}}
{{< br >}}

## AMIGOS DIOSES, FANTASMAS Y MONSTRUOS

{{< br >}}

¡Un elenco de personajes coloridos y llenos de vida te espera para conocerte! Haga crecer sus relaciones con ellos y experimente cientos de eventos de historias únicas a medida que aprende sobre lo que realmente está en juego para esta gran familia disfuncional.

{{< br >}}
{{< br >}}

## CONSTRUIDO PARA REPETIBILIDAD

{{< br >}}

Nuevas sorpresas te esperan cada vez que profundizas en el siempre cambiante Inframundo, cuyos jefes guardianes te recordarán. Usa el poderoso Espejo de la noche para fortalecerte permanentemente y date una ventaja la próxima vez que huyas de casa.

{{< br >}}
{{< br >}}

## NADA ES IMPOSIBLE

{{< br >}}

Las actualizaciones permanentes significan que no tienes que ser un dios para experimentar el emocionante combate y la apasionante historia. Sin embargo, si usted es uno, aumente el desafío y prepárese para una acción de nudillos blancos que pondrá a prueba sus habilidades bien practicadas.

{{< br >}}
{{< br >}}

## FIRMA ESTILO SUPERGIANO

{{< br >}}

La presentación rica y atmosférica y la combinación única de jugabilidad y narrativa que ha sido fundamental para los juegos de Supergiant está aquí con toda su fuerza: los espectaculares entornos pintados a mano del Inframundo y una partitura original que bombea sangre dan vida al Inframundo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1145360/extras/CharacterBanner_01.png" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Dual Core 3.0ghz
* Gráficos: 1GB VRAM / OpenGL 2.1+ support
* Almacenamiento: 10 GB de espacio disponible

{{< br >}}
{{< br >}}
