---
title: 'Tales of MajEyal'
date: '2020-02-28 19:11:00'
type: 'steam'
idiomas: ["ingles"]
category: ["rpg"]
tags: ["Tales of MajEyal","RPG","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/259680/header.jpg'
download: 'https://t.me/c/1118272073/11399'
---

# Tales of Maj'Eyal

{{< br >}}

Tales of Maj’Eyal es un juego de rol roguelike, con combate táctico por turnos y construcción avanzada de personajes. Juega como una de las muchas razas y clases únicas en el mundo lleno de tradición de Eyal, explorando mazmorras aleatorias, enfrentando batallas desafiantes y desarrollando personajes con tu propia combinación personalizada de habilidades y poderes.

Con una interfaz gráfica y personalizable moderna, control intuitivo del mouse, mecánica optimizada y combate profundo y desafiante, estadísticas en línea y hojas de personajes, Tales of Maj’Eyal ofrece una jugabilidad roguelike atractiva para el siglo XXI.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Memoria: 512 MB de RAM
* Gráficos: OpenGL 2.1+
* Almacenamiento: 512 MB de espacio disponible

{{< br >}}
{{< br >}}
