---
title: 'The Pedestrian'
date: '2020-02-01 19:46:00'
type: 'gog'
idiomas: ["ingles"]
category: ["puzzles","plataformas"]
tags: ["The Pedestrian","Puzzles","Plataformas","GOG"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/466630/header.jpg'
download: 'https://t.me/c/1118272073/11225'
---

# The Pedestrian

{{< br >}}

Ahora puedo ver las señales en mi sueño, siempre guiándome más cerca. ¿Esto realmente funcionará? ¿Casi hemos logrado lo imposible?

{{< br >}}

The Pedestrian es un juego de plataformas de rompecabezas de desplazamiento lateral 2.5D. Eres el peatón! Ingrese a un mundo 3D dinámico con gráficos impresionantes y rompecabezas desafiantes.
Juegas reorganizando y volviendo a conectar los letreros públicos para explorar y avanzar a través de cada entorno atractivo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466630/extras/hub_banner.gif?t=1580615355" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466630/extras/Warehouse_Banner.gif?t=1580615355" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466630/extras/Inner_banner.gif?t=1580615355" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466630/extras/sub_banner.gif?t=1580615355" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466630/extras/Riotsmallest.gif?t=1580615355" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Intel i3
* Memoria: 4 GB de RAM
* Gráficos: NVIDIA Geforce GTX 460
* Almacenamiento: 4 GB de espacio disponible

{{< br >}}
{{< br >}}
