---
title: 'Heliborne'
date: '2020-02-02 16:20:00'
type: 'steam'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Heliborne","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/433530/header.jpg'
download: 'https://t.me/c/1118272073/11239'
---

# Heliborne

{{< br >}}

Vuela con algunos de los mejores helicópteros del mundo, desde las máquinas clásicas de la década de 1950 hasta las modernas naves de combate del siglo XXI. ¡Juega misiones con tus amigos y compite con jugadores de todo el mundo en varios modos multijugador!

Podrás participar en un campo de batalla dinámico donde apoyarás el movimiento de las fuerzas terrestres que fortalecen posiciones y crean líneas de suministro.

{{< br >}}
{{< br >}}

## EL REGRESO DE LOS JUEGOS DE COMBATE EN HELICÓPTERO

{{< br >}}

Heliborne marca el gran regreso de un género casi olvidado: ¡los juegos de acción en helicóptero! Aquí podrás sumergirte en conflictos históricos, explorar mapas inspirados en ubicaciones del mundo real y participar en despiadados combates aire-aire y aire-tierra.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/433530/extras/hangar.jpg?t=1576838543" >}}
{{< br >}}

Al volar sobre 40 helicópteros distribuidos en 3 clases diferentes, lucharás contra otros pilotos, así como tanques, tropas y artillería antiaérea. Pondrás a prueba tu habilidad en modos multijugador, como Escaramuza (modo de estilo Rey de la colina) o Primera línea (cuando los jugadores capturan las bases, los vehículos terrestres se generan en convoyes y se mueven por el mapa para fortalecer bases y posiciones), o jugar solo las nuevas misiones para un jugador.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/433530/extras/maps600.jpg?t=1576838543" >}}
{{< br >}}

En Heliborne queríamos recrear fielmente las realidades de los conflictos armados y producir un modelo de juego que sea dinámico y realista. También tendrá la oportunidad de aprender más sobre el desarrollo de aviones de ala giratoria. El juego muestra diferentes períodos históricos, centrándose en la innovación tecnológica que cambió el combate de helicópteros a lo largo de los años. Por ejemplo, las cargas para cada helicóptero se basan completamente en sistemas de armas que se almacenan o se habrían empleado en teatros de combate y operaciones activas. **¡No verá una carga de armas en nuestros helicópteros que no es históricamente precisa!**

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04+ / SteamOS+
* Procesador: 2.3 GHz
* Memoria: 4 GB de RAM
* Gráficos: Shader Model 3.0 Compatible GPU, 986 MB VRAM
* Red: Conexión de banda ancha a Internet
* Almacenamiento: 4 GB de espacio disponible

{{< br >}}
{{< br >}}
