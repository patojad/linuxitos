---
title: 'Dead Cells: The Bad Seed'
date: '2020-02-23 18:37:00'
type: 'gog'
idiomas: ["español"]
category: ["accion"]
tags: ["Dead Cells","The Bad Seed","GOG"]
img: 'https://images.gog-statics.com/99d4af9bbc4f53e34f84278eb8990cab863059902370f6b32b59fb1bd6882c04_bg_crop_1366x655.jpg'
download: 'https://t.me/c/1118272073/11377'
---

# Dead Cells: The Bad Seed

{{< br >}}
{{< img src="https://items.gog.com/dead_cells/remparts.gif" >}}
{{< br >}}

## Roguelita? Metroidvania? Roguevania!

{{< br >}}

¿Creciste con los roguelikes, viste el surgimiento de los roguelitas e incluso el nacimiento de los roguelitas? Ahora nos gustaría presentar para su consideración nuestro RogueVania, el hijo ilegítimo de un Roguelite moderno (Rogue Legacy, Binding of Isaac, Enter the Gungeon, Spelunky, etc.) y un MetroidVania de la vieja escuela (Castlevania: SotN y sus secuaces). )

{{< br >}}
{{< img src="https://items.gog.com/dead_cells/graveyard.gif" >}}
{{< br >}}

## Caracteristicas:

{{< br >}}
{{< br >}}

* RogueVania : La exploración progresiva de un mundo interconectado, con la capacidad de reproducción de un rogue-lite y la amenaza de adrenalina de permadeath.
* Acción 2D Souls-lite : combate duro pero justo, más de cincuenta armas y hechizos con un juego único y, por supuesto, el pánico de emergencia para sacarte de problemas.
* Progresión no lineal : alcantarillas, osarios o remparts? Una vez desbloqueadas, las habilidades permanentes especiales te permiten acceder a nuevos caminos para alcanzar tu objetivo. Opta por el camino que se adapte a tu construcción actual, tu estilo de juego o solo tu estado de ánimo.
* Exploración : habitaciones secretas, pasajes ocultos, paisajes encantadores. Tómese un momento para pasear por las torres y respirar el aire fresco infundido de niebla marina ...

{{< br >}}
{{< br >}}

Los niveles interconectados y el desbloqueo progresivo del acceso a la isla le brindan un incentivo real para explorar sus alrededores. Agregue un grado de evolución para su personaje y actualizaciones permanentes de armas y podrá ver dónde Dead Cells toma prestado de la larga línea de MetroidVanias que lo precede.

{{< br >}}

Sin embargo, al final del día, ¡son TUS habilidades como jugador lo que más importa! Los roguelitas tratan de mejorar constantemente, hasta que lo que era un obstáculo insuperable se convierte en una caminata en el parque. El combate implacable combinado con la ausencia de cualquier tipo de red de seguridad lo convierte en un paseo de bombeo de adrenalina en cada carrera y una jugabilidad sin igual.

{{< br >}}
{{< img src="https://items.gog.com/dead_cells/ossuary_spawner.gif" >}}
{{< br >}}
{{< br >}}
