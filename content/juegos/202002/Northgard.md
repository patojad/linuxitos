---
title: 'Northgard: The Viking Age Edition'
date: '2020-02-01 19:35:00'
type: 'steam'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Amnesia","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/466560/header.jpg'
download: 'https://t.me/c/1118272073/11212'
---

# Northgard: The Viking Age Edition

{{< br >}}

Northgard es un juego de estrategia basado en la mitología nórdica en el que controlas un clan de vikingos que compiten por el control de un misterioso continente recién descubierto.

{{< br >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466560/extras/northgard$.png?t=1580399880" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466560/extras/wall1600x300.png?t=1580399880" >}}
{{< br >}}

Después de años de exploraciones incansables, los valientes vikingos han descubierto una nueva tierra llena de misterio, peligro y riquezas: Northgard.

Los norteños más audaces han zarpado para explorar y conquistar estas nuevas costas, traer fama a su clan y escribir la historia a través de la conquista, el comercio o la devoción a los dioses.
Eso si pueden sobrevivir a los terribles lobos y los guerreros zombis que vagan por la tierra, hacerse amigos de los gigantes o derrotarlos y sobrevivir a los inviernos más duros jamás vistos en el Norte.

{{< br >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466560/extras/v2_features.png?t=1580399880" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466560/extras/wallfeatures.png?t=1580399880" >}}
{{< br >}}

* Construye tu asentamiento en el continente recién descubierto de Northgard
* Asigna varios trabajos a tus vikingos (granjero, guerrero, marinero, sabio...)
* Gestiona tus recursos con cuidado y sobrevive a los duros inviernos y los enemigos feroces
* Expande y descubre nuevos territorios con oportunidades estratégicas únicas
* Logra diferentes condiciones de victoria (conquista, fama, sabiduría, comercio...)
* Juega contra tus amigos o contra una IA con diferentes niveles de dificultad y personalidades
* ¡Disfruta de servidores dedicados y asciende para alcanzar el rango final de dios nórdico!

{{< br >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466560/extras/v2_storymode.png?t=1580399880" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466560/extras/wallstory.png?t=1580399880" >}}
{{< br >}}

El gran rey vikingo ha sido asesinado y un hombre llamado HAGEN ha robado su CUERNO REGIO. Con este suceso comienza una saga que llevará a RIG, su hijo y heredero acompañado por su mano derecha BRAND, a través del nuevo continente de NORTHGARD, donde hará nuevos amigos y enemigos y descubrirá una amenaza mucho mayor que HAGEN, así como las razones del asesinato de su padre.

{{< br >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466560/extras/v2_clan.png?t=1580399880  " >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/466560/extras/clans_with_kraken.png?t=1580399880" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 18.04 64 bits
* Procesador: Intel 2.0ghz Core 2 Duo or equivalent
* Memoria: 1 GB de RAM
* Gráficos: Nvidia 450 GTS / Radeon HD 5750 or better
* Almacenamiento: 1 GB de espacio disponible
* Notas adicionales: Minimum display resolution : 1366x768

{{< br >}}
{{< br >}}
