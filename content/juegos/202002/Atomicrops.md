---
title: 'Atomicrops'
date: '2020-02-07 20:27:00'
type: 'epicgames'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Atomicrops","Accion","Epicgames"]
img: 'https://cdn2.unrealengine.com/Diesel%2Fproductv2%2Fatomicrops%2Fhome%2FupdateGif_megaton_x4-03c0124ccaebf224c7974a42c01b5e6532e73708.gif?h=1080&resize=1&w=1920'
download: 'https://t.me/c/1118272073/11295'
---

# Atomicrops

{{< br >}}
{{< img sec="https://www.atomicrops.com/s/siteGif_03.gif" >}}
{{< br >}}

Es un simulador de agricultura roguelita de acción. Cultiva cultivos ultra-OGM, limpia las tierras baldías del post apocalipsis, recluta una manada de animales de granja útiles y tal vez incluso encuentres amor mientras luchas con enjambres de bestias mutantes para defender tu granja; La última granja en la Tierra.

{{< br >}}
{{< img src="https://www.atomicrops.com/s/siteGif_02.gif" >}}
{{< br >}}

* **GRANJA** Coseche una cosecha abundante para mantener a la humanidad ... y obtenga grandes ganancias. Gaste su dinero en mejoras de armas y equipos para aumentar su rendimiento.
* **CASARSE** y casarse con un espíritu afín de la ciudad local para luchar y cultivar junto a usted.
* **MATAR**   Lucha contra las extrañas plagas postnucleares que mueren de hambre por un pedazo de tus cultivos. Convierte a tus enemigos en fertilizantes para maximizar la calidad de tu cosecha.
* **ACTUALIZACIÓN**   Explore los lejanos confines de los biomas para encontrar reliquias útiles de granjas del pasado. Descubra equipos de granja oxidados, herramientas de jardinería antiguas y tomos de guías de jardinería para aumentar su poder de cultivo.
* **OFRECER AMISTAD**   Reclute un séquito de amigos de cerdo, amigos de pollo y colegas de vacas para automatizar las tareas agrícolas. Cultivar atomicrops es un trabajo difícil, necesitarás toda la ayuda que puedas obtener.

{{< br >}}
{{< img src="https://www.atomicrops.com/s/siteGif_04.gif" >}}
{{< br >}}
{{< br >}}
