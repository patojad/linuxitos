---
title: 'Duke it out in D.C.'
date: '2020-02-23 17:40:00'
type: 'otros'
idiomas: ["ingles"]
category: ["accion","shooter"]
tags: ["Duke","Accion", Shooter,"Otros"]
img: 'https://vignette.wikia.nocookie.net/dukenukem/images/4/4c/3028-duke-it-out-in-d-c-dos-front-cover.jpg/revision/latest?cb=20161016032550'
download: 'https://t.me/c/1118272073/11361'
---

# Duke it out in D.C.

{{< br >}}

Duke It Out In DC es un paquete de expansión para el popular shooter en primera persona, Duke Nukem 3D , desarrollado por Sunstorm Interactive y publicado por WizardWorks .

{{< br >}}
{{< br >}}

## Trama

{{< br >}}

Los alienígenas han aterrizado en el edificio de la capital y han comenzado una invasión masiva de DC, capturando y destruyendo ubicaciones gubernamentales cruciales. Se llama a un hombre para que elimine a los bichos alienígenas: Duke Nukem . Como Duke , los jugadores pasarán por un nuevo episodio en un intento de derrotar a los alienígenas y rescatar al presidente .

{{< br >}}
{{< br >}}

## Diseño de niveles

{{< br >}}

Mientras que muchos han felicitado a Duke It Out en el diseño de niveles de DC , otros han criticado los niveles por ser demasiado grandes y, en algunos casos, confusos. Un mapa que tiene quizás las opiniones más encontradas es el Smithsonian Terror , un museo masivo basado en el lugar real, ya que algunos critican su tamaño y desconciertan mientras que otros alaban los interiores realistas y las ideas creativas de diseño.

{{< br >}}

A medida que el juego se desarrolla en Washington, DC, que cuenta con una gran cantidad de puntos de referencia tales como la Casa Blanca , Capitolio de Estados Unidos , Monumento a Washington , el Lincoln Memorial y el Smithsonian Institution (o Smithsonian terror como se le llama en el juego), todo en todo su esplendor para que el jugador explore.

{{< br >}}
{{< br >}}

## Recepción Editar

{{< br >}}

Duke It Out In DC ha tenido resultados mixtos con los jugadores. Si bien algunos consideran que es la mejor de las expansiones, también es considerada por muchos como la peor debido a su diseño de nivel frustrante. Además, no contiene armas nuevas ni enemigos que no le gustaron a algunos.

{{< br >}}
{{< br >}}
