---
title: 'Through the Darkest of Times'
date: '2020-02-08 16:00:00'
type: 'steam'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Through the Darkest of Times","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1003090/header.jpg'
download: 'https://t.me/c/1118272073/11308'
---

# Through the Darkest of Times

{{< br >}}

Berlín, enero 1933. “¡Adolf Hitler es canciller!” Todos sabemos a dónde nos llevará ese mensaje, a un mundo de horrores y sufrimientos. Muy pocos se oponen ante la monstruosidad del Reich alemán. ¿Tú podrías? Lidera a un grupo de resistencia clandestino a través del más oscuro de los tiempos.

Los tiempos oscuros significan miedo y riesgo. El riesgo de ser atrapado por alguna patrulla de nacionalsocialistas, en busca de personas que se opongan públicamente a su punto de vista. El riesgo de ser apaleado o incluso asesinado por el ejército alemán porque nos oponemos al régimen. El riesgo de perderlo todo, incluido nuestros seres queridos. Así es como vivimos. Así es como tratamos de sobrevivir. A través del más oscuro de los tiempos.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1003090/extras/TTDOT_banner_BERLIN.gif" >}}
{{< br >}}

Eres el líder de un pequeño grupo de resistencia en Berlín en el año 1933, de gente ordinaria, desde judíos hasta católicos y desde comunistas hasta patriotas que simplemente no pueden mantanerse al margen. Tu objetivo es tratar de dar pequeños golpes al régimen: lanzar folletos para dar a conocer qué están haciendo los nazis entre la gente realmente, pintar mensajes en las paredes, sabotear, recopilar información y reclutar más entusiastas. Y todo eso mientras permaneces encubierto: si las fuerzas del régimen se enteran de tu grupo, la vida de cada miembro estaría en grave peligro.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1003090/extras/TTDOT_photo_collage_SteamPage.png" >}}
{{< br >}}

Through the Darkest of Times es un juego de estrategia de resistencia histórica, que se centra en transmitir el humor sombrío del período y los conflictos tan reales de la gente mediocre que vivía en el 3er Reich. La precisión histórica significa que tu pequeño grupo de luchadores de la resistencia no cambiará el resultado de la guerra, ni evitará todas las atrocidades de los nazis, pero claro que puedes hacer todo lo que esté en tu mano para salvar tantas vidas como sea posible y oponerse al fascista sistema siempre que sea posible.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1003090/extras/TTDOT_banner_red.gif" >}}
{{< br >}}

## Características

{{< br >}}

* Experimenta lo más oscuro de los tiempos en 4 capítulos
* Lucha por la libertad, debilita el régimen y lidera tu grupo de resistencia
* Planifica actividades, encuentra colaboradores e intenta no ser atrapado
* Siente el peso de la responsabilidad mientras tomas decisiones difíciles y enfréntate las terribles consecuencias
* Escenas y eventos expresionistas bellamente ilustrados.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: 2.4 GHz
* Memoria: 4 GB de RAM
* Gráficos: 1 GB
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
