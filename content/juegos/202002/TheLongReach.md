---
title: 'The Long Reach'
date: '2020-02-02 19:30:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura","plataformas"]
tags: ["The Long Reach","Aventura","Plataformas","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/584990/header.jpg'
download: 'https://t.me/c/1118272073/11260'
---

# The Long Reach

{{< br >}}

Es un juego de aventuras lleno de coloridos personajes, puzles y el horror del descubrimiento. Espera, ¿no se supone que es "el placer del descubrimiento"? Esto no puede estar bien.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/584990/ss_3fecf7110f95c3a064e9236d2d69aef17733a8fb.600x338.jpg?t=1572357729" >}}
{{< br >}}

The Long Reach está ambientado en New Hampshire, en la ciudad ficticia de Baervox. El juego está inspirado en Lone Survivor, The Cave y todos los demás clásicos juegos de aventuras (pero The Long Reach está ambientado en la actualidad). No juegas en un mundo posapocalíptico ni exploras un mundo de fantasía lleno de hadas. Estás en la boca del lobo, a la vuelta de la esquina del sitio al que vas a hacer la compra.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/584990/extras/Banner.png?t=1572357729" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 17 or above
* Procesador: Intel i3 2.6GHz
* Memoria: 2 GB de RAM
* Gráficos: Intel HD 4000 or better
* Almacenamiento: 500 MB de espacio disponible

{{< br >}}
{{< br >}}
