---
title: 'Urtuk: The Desolation'
date: '2020-02-29 20:15:00'
type: 'steam'
idiomas: ["ingles"]
category: ["rpg"]
tags: ["Urtuk","The Desolation","RPG","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1181830/header.jpg'
download: 'https://t.me/c/1118272073/11432'
---


# Urtuk: The Desolation

{{< br >}}

Es un juego de rol táctico basado en turnos de mundo abierto en un entorno de baja fantasía. Guía a tu grupo de aventureros a través de las ruinas de un mundo antiguo. Recluta nuevos seguidores, saquea los cadáveres de tus enemigos caídos y haz tu mejor esfuerzo para sobrevivir en este reino duro e implacable.

{{< br >}}
{{< br >}}

## Caracteristicas

{{< br >}}

* RPG de supervivencia con enfoque en combate y exploración de mundo abierto en un entorno de fantasía oscura
* Combate tácticamente por turnos en mapas grandes, con múltiples factores ambientales y un sistema de clase / habilidad cuidadosamente diseñado
* Campaña de supervivencia generada procesalmente
* Extrae mejoras de personaje directamente de tus enemigos caídos
* Todos los recursos gráficos están dibujados a mano, desde personajes, monstruos y objetos, hasta el mapa mundial y los campos de batalla.

{{< br >}}
{{< br >}}

## Batallas dinámicas

{{< br >}}

¡Golpea a tus enemigos en picos o en altos acantilados! Protege a tus aliados y contraataca a tus enemigos cuando golpeen. Usa tus arqueros para obtener apoyo a distancia mientras realizas ataques cuerpo a cuerpo. ¡O ejecuta uno de los muchos posibles ataques combinados!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1181830/extras/ram-optimized-small_cut.gif" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1181830/extras/bash_into_pit-optimized.gif" >}}
{{< br >}}

## Personajes

{{< br >}}

¡Extrae habilidades y rasgos de los enemigos caídos y aplícalos a tus unidades! Estas habilidades personalizadas pueden cambiar totalmente el papel de un personaje en combate. Las habilidades extraídos incluyen empujando al enemigo en un golpe crítico, cuando represalias ser golpeado, evitando una huelga letal, y la realización de un contraataque lifestealing en contra de su enemigo!

{{< br >}}

Tus personajes también aprenden nuevas habilidades al ejecutar acciones específicas en batallas. ¡Presta atención y se recompensado!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Fedora 25, Ubuntu 16.04, SteamOS, Mint 18
* Procesador: 1.2 Ghz
* Memoria: 1 GB de RAM
* Gráficos: OpenGL 2.0 compatible video card with 256 MB
* Almacenamiento: 600 MB de espacio disponible

{{< br >}}
{{< br >}}
