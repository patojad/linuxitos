---
title: 'Wildermyth'
date: '2020-02-07 19:23:00'
type: 'steam'
idiomas: ["ingles"]
category: ["rpg"]
tags: ["Wildermyth","RPG","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/763890/header.jpg'
download: 'https://t.me/c/1118272073/11281'
---

# Wildermyth

{{< br >}}

Un juego de rol táctico para hacer mitos. Te permite crear personajes icónicos que crecen en batallas profundas y gratificantes y narraciones interactivas.

{{< br >}}

Wildermyth es un juego de rol táctico generado por procedimientos y basado en personajes, diseñado para ayudarte a contar tus historias más salvajes. Al igual que las mejores experiencias de juego de roles de mesa, Wildermyth te da opciones y responde a todas tus decisiones con consecuencias que impulsan a tus personajes hacia adelante.

Dirige a una banda de héroes a medida que crecen de granjeros reacios a luchadores únicos y legendarios. Combate amenazas inesperadas y monstruos extraños en campos de batalla interactivos. Desvela misterios y comparte momentos pensativos en un entorno de fantasía siempre nuevo que combina verdades duras y sacrificios con humor y narraciones personales.

¿A dónde lleva tu mito? ¡Ven a ayudarnos a descubrirlo!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/763890/extras/heroLineup.png" >}}
{{< br >}}

* Con reminiscencias de juegos de rol de mesa, los héroes únicos nacen en entornos únicos en cada juego. Envejecen, se transforman, se enamoran, no están de acuerdo y hacen sacrificios desgarradores.
* Cada héroe trae consigo su propia historia y personalidad orgánica, pero sus elecciones y habilidades de combate son las que deciden sus caminos y resultados.
* Todos los héroes mueren algún día ... pero puedes conservar tus favoritos. Vuelva a presentarlos en la próxima aventura, y durante muchas vidas los mitos que haga formarán su propio panteón legendario.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/763890/extras/heroesGorgonSmall.png" >}}
{{< br >}}

## Un mundo imaginativo de Papercraft

{{< br >}}

The Yondering Lands, donde se desarrolla el juego, es un mundo rico en capas poblado de personajes y paisajes 2D pintados a mano. Hemos prestado una atención amorosa a romper moldes y explorar nuevas ideas: no hay orcos, elfos ni duendes aquí, pero ten cuidado con los dragones de insectos telepáticos y los muertos vivientes mecánicos.

{{< br >}}
{{< br >}}

## Profundidad extrema del personaje

{{< br >}}

Cada uno de tus héroes tiene su propia historia, personalidad y apariencia únicas, y formarán relaciones con tus otros héroes. Todos estos elementos cambian y se desarrollan en el transcurso de un juego, a medida que los personajes envejecen, encuentran misterios y superan desafíos.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/763890/extras/heroesRelationshipSmall.png?t=1576886792" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Memoria: 3 GB de RAM
* Gráficos: Open GL 3.2
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
