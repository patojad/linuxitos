---
title: 'Iris and the Giant'
date: '2020-02-29 19:55:00'
type: 'gog'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Iris and the Giant","Estrategia","GOG"]
img: 'https://images.gog-statics.com/a2167991b70bd7e1dde03d7449620a2277785bfefcf54f164f3ef1a5adfc1d3d.jpg'
download: 'https://t.me/c/1118272073/11414'
---


# Iris and the Giant

Es una fusión de los géneros roguelike, RPG y CCG. Juegas como Iris y descubrirás su psique y ansiedades.

{{< br >}}
{{< br >}}

## Descripción

{{< br >}}

Iris and the Giant es una fusión de los géneros roguelike, RPG y CCG. Juegas como Iris y descubrirás su psique y ansiedades.
Construye tu mazo
Con cada encuentro, desbloquea nuevas cartas y gana puntos para impulsar a Iris. ¡Cada intento te hace más fuerte y te permite llegar más lejos!

{{< br >}}
{{< img src="https://items.gog.com/iris_and_the_giant_card_deck_roguelike/talents.jpg" >}}
{{< br >}}

## Una historia poética y melancólica.

{{< br >}}

A lo largo del juego, puedes descubrir fragmentos de los recuerdos de Iris, dándote una nueva visión de las razones detrás de tu aventura en su mente.

{{< br >}}
{{< img src="https://items.gog.com/iris_and_the_giant_card_deck_roguelike/melancholy.jpg" >}}
{{< br >}}

## Caracteristicas

{{< br >}}

* Construcción de mazos: construye tu mazo a medida que progresas. Elija las mejores opciones para su estrategia a largo plazo
* Una aventura conmovedora, con mucho cuidado en la configuración gráfica y visual.
* Personalización! El juego está repleto de opciones sobre cómo especializar a tu personaje, tu mazo y tu estilo de juego.
* Una curva de dificultad progresiva que hace que el juego sea accesible para todos, pero ofrece un desafío incluso para los jugadores más duros.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema: Ubuntu 16.04, 18.04 o posterior
* Procesador: CPU de 1.3 GHz
* Memoria: 2 GB de RAM
* Gráficos: Tarjeta gráfica compatible con Shader Model 2.0
* Almacenamiento: 1 GB

{{< br >}}
{{< br >}}
