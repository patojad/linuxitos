---
title: 'Moonlighter: Complete Edition'
date: '2020-02-15 14:15:00'
type: 'gog'
idiomas: ["español"]
category: ["rpg"]
tags: ["Moonlighter","RPG","GOG","complete edition"]
img: 'https://images.gog-statics.com/33c5be2ff010b191e376feafa43f2f6fd0ae19c104418e025d363d84dc5988f1_product_card_v2_mobile_slider_639.jpg'
download: 'https://t.me/c/1118272073/11325'
---

# Moonlighter: Complete Edition

{{< br >}}

Es un juego de rol de acción con elementos pícaros que siguen las rutinas cotidianas de Will, un comerciante aventurero que sueña con convertirse en un héroe. La aventura de Will comienza cuando, durante una larga excavación arqueológica, se descubrió un conjunto de puertas.

{{< br >}}
{{< img src="https://images.gog-statics.com/8bc294c3a124343611594e4dc8d2e247c966349afba949de037ed57118120b41_product_card_v2_mobile_slider_639.jpg" >}}
{{< br >}}

## Moonlighter: Complete Edition está compuesto por:

{{< br >}}

* Moonlighter : versión completa del juego base.
* Moonlighter - Between Dimensions : la expansión que amplía la experiencia principal al llenar las mazmorras actuales con nuevas criaturas, poblar la nueva mazmorra interdimensional con mini jefes, abarrotando la aventura con nuevas armas, armerías, artículos de la tienda y anillos.

{{< br >}}
{{< img src="https://images.gog-statics.com/d644b8097f5423ab2cf8a3ded2ef337a29a6dc7e87374568c15970ae22c8f8ac_product_card_v2_mobile_slider_639.jpg" >}}
{{< br >}}

## ¡Cada aventura tiene que pagar!

{{< br >}}

La gente rápidamente se dio cuenta de que estos antiguos pasajes conducen a diferentes reinos y dimensiones, proporcionando a los valientes e imprudentes aventureros tesoros sin medida. Rynoka, un pequeño pueblo comercial, se fundó cerca del sitio de excavación que proporciona refugio y un lugar para que los aventureros vendan sus riquezas ganadas con tanto esfuerzo.

{{< br >}}
{{< img src="https://images.gog-statics.com/c44ef5343083ba5a51f0935221e0454685986ee12e9669f917fd62e08838d065_product_card_v2_mobile_slider_639.jpg" >}}
{{< br >}}

Moonlighter en pocas palabras:

{{< br >}}

* Dirige tu propia tienda
* Lucha con estilo
* Restaurar el pueblo
* Craft and Enchant
* Consigue el botín
* Explora varias mazmorras

{{< br >}}
{{< img src="https://images.gog-statics.com/7790052f9e474a1eadf56d6a4bf3592693fe8827d9d78257233683e34956aae2_product_card_v2_mobile_slider_639.jpg" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema: Ubuntu 16.04
* Procesador: Intel (R) Core (TM) 2 Quad 2.7 Ghz, AMD Phenom (TM) II X4 3 Ghz
* Memoria: 4 GB de RAM
* Gráficos: GeForce GTX 260, Radeon HD 5770, 1024 MB, Shader Model 3.0
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
