---
title: 'Shovel Knight: Treasure Trove'
date: '2020-01-06 17:40:00'
type: 'steam'
idiomas: ["español"]
category: ["plataformas"]
tags: ["Shovel Knight","Treasure Trove","Plataformas","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/250760/header.jpg?t=1578509708'
download: 'https://t.me/c/1118272073/10860'
---

# Shovel Knight: Treasure Trove

{{< br >}}

¡Shovel Knight: Treasure Trove es la colección completa de Shovel Knight y contiene los 5 juegos de esta saga épica! Cava, salta y carga en este fantástico mundo inspirado en los juegos de 8-bits. Plataformeo pixelado, personajes memorables y una jugabilidad de acción-aventura de altísimo nivel.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/250760/extras/logo_TT.png?t=1578509708" >}}
{{< br >}}

¡Shovel Knight: Treasure Trove es la edición completa de Shovel Knight, un arrasador clásico de acción-aventura con una jugabilidad apasionante, personajes memorables y estética retro de 8-bits! Corre, salta y lucha encarnando a Shovel Knight, Caballero de la Pala, en busca de su amada perdida. Acaba con los infames miembros de la Orden Sin Cuartel y enfréntate a su amenazante líder, la Hechicera.

{{< br >}}

¡Pero eso no es todo! Shovel Knight: Treasure Trove también incluye 4 juegos completos adicionales! Toma el control de Plague Knight, Specter Knight y King Knight en sus propias aventuras y lucha contra el resto en un Showdown para cuatro jugadores en modo local. ¡Todos juntos, forman una arrolladora y enorme saga!

{{< br >}}

Llévatelo todo con Shovel Knight: Treasure Trove. Juega con un amigo en la campaña cooperativa de Shovel of Hope, pon tu temple a prueba en completos modos desafío, combate contra otros jugadores en Showdown y dale la vuelta al juego con el modo Cambio de Cuerpo. ¡Defiende las virtudes de la Palería, consigue reliquias y tesoros y descubre el verdadero significado de la justicia de la pala!

{{< br >}}
{{< br >}}

## Incluye:

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/250760/extras/logo_SoH.png?t=1578509708" >}}
{{< br >}}

**Shovel Knight: Shovel of Hope** – ¡Afila tu pala y empieza a cavar en la aventura que lo empezó todo! Salta, combate y encuentra tesoros mientras tratas de derrotar a la Orden Sin Cuartel y a su vil líder, la Hechicera.

{{< br >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/250760/extras/logo_PoS.png?t=1578509708" >}}
{{< br >}}

**Shovel Knight: Plague of Shadows** – ¡Prepara tus pociones para convertirte en el maníaco alquimista Plague Knight en su aventura para conseguir la poción definitiva! Combina componentes explosivos para crear bombas personalizadas, encuentra nuevos objetos, áreas y jefes e incluso un burbujeante romance.

{{< br >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/250760/extras/logo_SoT.png?t=1578509708" >}}
{{< br >}}

**Shovel Knight: Specter of Torment** – ¡Enarbola tu guadaña en esta precuela de Shovel Knight! Toma el control de Specter Knight, domina un nuevo arsenal, derrota a tus enemigos y súbete por las paredes en niveles rediseñados de arriba abajo. ¡Conviértete en el segador, descubre tu melancólico pasado y recluta a la Orden Sin Cuartel!

{{< br >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/250760/extras/logo_KoC.png?t=1578509708" >}}
{{< br >}}

**Shovel Knight: King of Cards** – ¡Ponte las doradas botas de King Knight en el mayor y más noble juego de Shovel Knight! ¡Salta, carga con hombro, reúne súbditos y crea tu propia estrategia en esta precuela para convertirte en el Rey de Naipes!

{{< br >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/250760/extras/logo_Showdown.png?t=1578509708" >}}
{{< br >}}

**Shovel Knight Showdown** – Vence en combates locales de hasta 4 jugadores y arrambla con gemas o toma el control de tu personaje favorito para adentrarte en el Modo Historia. Domina todos los movimientos, explora una vasta cantidad de objetos y escenarios y descubre nuevas revelaciones en este juego de lucha y plataformas.

{{< br >}}
{{< br >}}

## Características:

{{< br >}}

* El bello y genuino estilo de Shovel Knight tiende puentes con el pasado. Diseño, arte y audio homenajean a los días de los 8 bits.
* La tecnología de hoy en día da a Shovel Knight toques modernos: animaciones detalladas, fondos parallax multicapa y un diseño de juego actual.
* ¡Salta sobre el vacío! ¡Pelea contra enemigos! ¡Descubre tesoros! ¡Encuentra secretos y mejoras! A jugar a Shovel Knight se aprende muy rápido, pero el reto está en todas partes.
* ¡Muchísimos temas musicales compuestos por Jake «Virt» Kaufman, junto a canciones de Manami Matsumae!
* Amasa riquezas para mejorar tu armadura y armamento.
* ¡Unos fondos y unas animaciones detalladas y vivas! ¡Enormes y bellos píxeles!
* ¡Personajes entrañables y jefes traicioneros!
* Su equilibrada jugabilidad resultará cómoda para los novatos y despertará los mejores recuerdos de los expertos.
* Una sombría historia narrada con un toque ligero, con humor y con pasión.
* ¡Revive la aventura y descubre todos sus secretos con Nueva Partida+!
* Modo Desafío – ¡Sube la apuesta en todos los sentidos con más de 100 desafíos completos!
* Cambio de Cuerpo – ¿Alguna vez deseaste que Shovel Knight fuese mujer, o que Shield Knight fuese un hombre? ¡Perfecto, porque ahora puedes decidir el género de tus personajes a tu manera y disfrutar de nuevos diseños de los personajes principales de Shovel of Hope!
* ¡Coopera! ¡Júntate con un amigo y juega a Shovel of Hope en modo de aventura cooperativo o descubrid juntos el Modo Historia de Showdown!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04 and Newer
* Procesador: Intel Core 2 Duo 2.1 ghz or equivalent
* Memoria: 2 GB de RAM
* Gráficos: 2nd Generation Intel Core HD Graphics (2000/3000), 512MB
* Almacenamiento: 250 MB de espacio disponible

{{< br >}}
{{< br >}}
