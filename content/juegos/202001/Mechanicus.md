---
title: 'Warhammer 40,000: Mechanicus'
date: '2020-01-26 18:14:00'
type: 'steam'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Warhammer 40,000","Mechanicus","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/673880/header.jpg?t=1579882881'
download: 'https://t.me/c/1118272073/11114'
---

# Warhammer 40,000: Mechanicus

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/673880/extras/Mechanicus_Reditus_Servo-skull.gif.png?t=1579882881">}}
{{< br >}}

Ponte al mando de las tropas humanas más tecnológicamente avanzadas del Imperum: el Adeptus Mechanicus. En la piel del Magos Dominus Faustinius, liderarás la expedición al nuevo planeta Silva Tenebris, que ha sido recientemente descubierto. Gestiona recursos, descubre tecnologías olvidadas, personaliza tu equipo y gestiona todos los movimientos de tus Tech-Priests.

Toda decisión que tomes afectará al desarrollo de las misiones y, en última instancia, decidirá el destino de las tropas que tienes bajo tus órdenes para superar más de 50 misiones diseñadas artesanalmente. Será importante que selecciones el camino correcto, ya que el Imperium depende de ello.

{{< br >}}

## Aumento

{{< br >}}

¡La carne es débil! Monta tu séquito y mejora a tus Tech-Priests con armas, objetos de apoyo, Servocráneos y otros aumentos mecánicos. Personaliza las disciplinas de tu equipo, con numerosas clases de personaje a elegir para crear un pelotón que se ajuste a tu estilo de juego.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/673880/extras/Augment_v3.gif.png?t=1579882881" >}}
{{< br >}}

## Sistema de combate único

{{< br >}}

Participa en combates envolventes y estratégicos, que pondrá a prueba tu valía en condiciones de presión extrema al sufrir una emboscada. Usa tus puntos de conocimiento para acceder a las armas y habilidades más potentes y ataca a tu enemigo sin cesar. No temas, el Omnissiah apoya a los valerosos.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/673880/extras/Library_of_Immersion_v2.gif.png?t=1579882881" >}}
{{< br >}}

## La biblioteca de la inmersión

{{< br >}}

Una historia fascinante escrita por Ben Counter, autor de Black Library. Esta historia ha sido escrita específicamente para que se amolde a la personalidad única de la facción del Adeptus Mechanicus, de tal modo que cada personaje tendrá su propia personalidad y sus propios objetivos. Siente la tensión y admira la devoción con unos efectos visuales únicos y un sonido impresionante.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/673880/extras/Difficult_Decisions_v2.gif.png?t=1579882881" >}}
{{< br >}}

## Decisiones difíciles

{{< br >}}

Toma decisiones difíciles que determinarán el futuro de tu Ark Mechanicus, The Caestus Metalican. Gracias a tus decisiones, cada partida será única y tendrás varios finales alternativos. Eso sí, ten en cuenta que, cuanto más explores cada tumba, mayores serán las recompensas… ¡pero también las amenazas!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/673880/extras/Canticle_v1.gif.png?t=1579882881" >}}
{{< br >}}

## Cánticos del Omnissiah

{{< br >}}

Consigue logros en el juego y se te recompensará con potentes habilidades que podrás usar una vez por misión para que te ayuden en el combate.

{{< br >}}
{{< img src="" >}}
{{< br >}}

## y mucho más

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/673880/extras/Combat_Necron_v1.gif.png?t=1579882881" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: SteamOS, Ubuntu 16.04 (64bit)
* Procesador: Intel Core i7 3.0 GHz+
* Memoria: 8 GB de RAM
* Gráficos: 2GB ATI Radeon HD 7970, 2GB NVIDIA GeForce GTX 770 or better
* Almacenamiento: 8 GB de espacio disponible

{{< br >}}
{{< br >}}
