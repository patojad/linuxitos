---
title: 'Mutazione'
date: '2020-01-25 18:21:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura"]
tags: ["Mutazione","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1080750/header.jpg?t=1579945838'
download: 'https://t.me/c/1118272073/11043'
---

# Mutazione

{{< br >}}

**Una telenovela mutante en la que los cotilleos de un pequeño pueblo conviven con lo sobrenatural...**

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1080750/extras/Banner_2V2.gif?t=1579945838" >}}
{{< br >}}

Es un juego en el que los interesantes dramas personales son tan importantes como las arriesgadas aventuras de la historia.Explora la comunidad a medida que Kai, de 15 años, recorre la extraña y misteriosa Mutazione para cuidar de su abuelo enfermo, Nonno.

{{< br >}}

Haz nuevas amistades; cultiva jardines musicales; participa en barbacoas, noches de bandas y viajes por alta mar; y embárcate en un viaje espiritual para salvar a todos de la misteriosa oscuridad que os acecha.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1080750/extras/Banner_5.gif?t=1579945838">}}
{{< br >}}

Hace más de 100 años, el meteorito Dragón Lunar cayó sobre una localidad turística tropical. La mayoría de sus habitantes fallecieron, mientras que los supervivientes empezaron a desarrollar extrañas mutaciones... Las misiones de rescate pronto se cancelaron y aquellos que quedaron en el entorno mutante fundaron la pequeña y aislada comunidad de Mutazione.

{{< br >}}

Ya en la época actual, jugarás como Kai, una niña de 15 años, y viajarás hasta Mutazione para cuidar de tu abuelo enfermo. Pero las cosas no son tan sencillas como parecen... Nonno tiene otros planes para Kai; los secretos y las traiciones se esconden bajo la superficie cordial de la comunidad; y una espeluznante figura con forma de pájaro no deja de aparecer en los sueños de Kai.

{{< br >}}

Han sobrevivido al apocalíptico impacto de un meteorito, pero... ¿podrán sobrevivir a los dramas de su pequeña ciudad?

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1080750/extras/Banner_1.gif?t=1579945838" >}}
{{< br >}}

## Características

{{< br >}}

* Un mundo exuberante e ilustrado a mano que podrás explorar
* Un elenco de adorables personajes mutantes
* Una historia con giros dramáticos e impredecibles
* Jardines que podrás personalizar para crear paisajes sonoros únicos y relajantes.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: 2.6 GHz
* Memoria: 4 GB de RAM
* Gráficos: GeForce 700 Series
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
