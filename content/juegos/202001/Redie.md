---
title: 'Redie'
date: '2020-01-04 15:22:00'
type: 'steam'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Redie","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/536990/header.jpg?t=1573644261'
download: 'https://t.me/c/1118272073/10783'
---

# Redie

{{< br >}}

Redie es un desafiante juego de disparos en 3D de un jugador. Lucha contra un ejército de terroristas y soldados bien entrenados en combate cuerpo a cuerpo repleto de acción. Prepárate para (re)morir.

{{< br >}}
{{< br >}}

## Caracteristicas

{{< br >}}

* Desafiante - ¡Enemigos con puntería implacable y reacciones inhumanas!
* Sin historia - ¡un juego lleno de acción sin filtro!
* Armas mortales - 13 armas únicas que pueden matar con un solo disparo.
* Toneladas de acción - más de 25 niveles detallados con desafíos de dominio opcionales lo mantienen ocupado.
* Física - objetos destructibles, partículas, muñecos de trapo y explosiones poderosas.
* Optimizado - controles ajustados y altas velocidades de cuadros, incluso en computadoras de gama baja, utilizando un motor hecho a sí mismo.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04 (64-Bit)
* Procesador: 2 GHz Dual Core
* Memoria: 1 GB de RAM
* Gráficos: 512MB VRAM, OpenGL 3.3
* Almacenamiento: 400 MB de espacio disponible
* Tarjeta de sonido: OpenAL Compatible Sound Card

{{< br >}}
{{< br >}}
