---
title: 'Vector 36'
date: '2020-01-06 13:30:00'
type: 'steam'
idiomas: ["ingles"]
category: ["deportes","carreras"]
tags: ["Vector","Deportes","Carreras","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/346460/header.jpg?t=1563160805'
download: 'https://t.me/c/1118272073/10840'
---

# Vector 36

{{< br >}}

Corre por la superficie terraformada de Marte en tu Skimmer, un vehículo de carreras flotante completamente personalizable. La habilidad de pilotaje y la ingeniería marcan la diferencia entre ganar y perder en Vector 36, un corredor basado en la física.

{{< br >}}

Compite con otros pilotos a través de varias pistas en Marte. Compite en los modos Sprint, Circuito o Torneo para ganar créditos, partes o nuevos Skimmers.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/346460/extras/Steam_Desc1.jpg?t=1563160805" >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/346460/extras/Steam_Desc_PB.jpg?t=1563160805" >}}
{{< br >}}

Cada parte de su Skimmer está sujeta a las leyes de la física. El movimiento de los componentes internos desplazará el centro de gravedad, el ajuste de los ángulos a los propulsores afectará sus características de vuelo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/346460/extras/Steam_Desc_Cust.jpg?t=1563160805" >}}
{{< br >}}

El ajuste fino es la diferencia entre ganar y perder. Elija entre una amplia selección de piezas, accesorios, armas pasivas o equipo de soporte para ajustar su Skimmer a su tarea. Ya sea que se trate de un sprint rápido con un impulso total, una carrera de resistencia con partes endurecidas o una escalada en una colina donde la elevación es más importante que el empuje; evolucionar o quedarse atrás.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* **SO:** Ubuntu 12.04+, SteamOS+
* **Procesador:** i5 or equivelent
* **Memoria:** 4 GB de RAM
* **Gráficos:** 1 GB card
* **Almacenamiento:** 3 GB de espacio disponible

{{< br >}}
{{< br >}}
