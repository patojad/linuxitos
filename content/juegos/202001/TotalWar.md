---
title: 'Total War: WARHAMMER II'
date: '2020-01-28 20:11:00'
type: 'steam'
idiomas: ["ingles"]
category: ["estrategia"]
tags: ["Total War", "WARHAMMER II","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/594570/header.jpg'
download: 'https://t.me/c/1118272073/11193'
---

# Total War: WARHAMMER II

{{< br >}}

Es un juego de estrategia de proporciones épicas. Elige de entre cuatro exclusivas facciones y decide cómo librar la guerra: lanzando una campaña de conquista para salvar este enorme y fantástico mundo o destruirlo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/594570/extras/AccoladesGIF.jpg?t=1580144883" >}}
{{< br >}}

## Defiende tu mundo. Destruye el suyo

{{< br >}}

Este juego posee dos partes diferenciadas: una de ellas es la campaña de mundo abierto por turnos y la segunda consiste en batallas tácticas a tiempo real, ambientadas en los fantásticos paisajes del Nuevo Mundo.

Elige cómo jugar: te sumergirás en una intensa y atractiva campaña con la posibilidad de jugar muchas veces y desafiar al mundo en modo multijugador con un ejército personalizado, compuesto por tus unidades favoritas. Total War: WARHAMMER II te ofrece cientos de horas de juego, donde no habrá dos partidas iguales.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/594570/extras/Cavalry_ChargeGIF.jpg?t=1580144883" >}}
{{< br >}}

## Una conquista mundial

{{< br >}}

Emplea tu arte para gobernar, entabla relaciones diplomáticas, explora y construye tu imperio turno tras turno. Captura, construye y gestiona florecientes asentamientos y recluta enormes ejércitos. Haz que tus héroes y Señores legendarios suban de nivel y equípalos con armas y armaduras míticas. Negocia alianzas o declara la guerra total para someter a todo aquel que se interponga en tu camino.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/594570/extras/Duelling_DragonsGIF.jpg?t=1580144883" >}}
{{< br >}}

## Épicas batallas en tiempo real

{{< br >}}

Dirige gigantescas legiones de guerreros en trepidantes batallas tácticas. Ordena atacar a feroces y deformes monstruos, dragones que escupen fuego y domina la poderosa magia. Emplea estrategias militares, tiende emboscadas o usa la fuerza bruta para cambiar el curso de una batalla y llevar a tus tropas a la victoria.
Total War: WARHAMMER II, la segunda entrega de la trilogía y secuela de Total War: WARHAMMER, ofrece a los jugadores una nueva e imponente campaña narrativa que se desarrolla en los enormes continentes de Lustria, Ulthuan, Naggaroth y las Tierras del Sur. La campaña del Gran Vórtice va tomando forma hasta culminar en un final insuperable, una experiencia sin igual en cualquier otro título de Total War hasta la fecha.

Al jugar con uno de los 8 Señores legendarios de las 4 icónicas razas del mundo de Warhammer Fantasy Battles, los jugadores podrán realizar una serie de poderosos y arcanos rituales para estabilizar o perturbar el Gran Vórtice, a la vez que obstaculizan el avance de otras razas.

Cada Señor legendario tiene una posición inicial geográfica única y cada raza ofrece un estilo de juego, una mecánica, una narrativa, unas tácticas de guerra, unos ejércitos, unos monstruos, Saberes de la Magia y personajes legendarios tan únicos como asombrosos, además de nuevas habilidades para el ejército en el campo de batalla.

Justo después de su lanzamiento, quienes posean el juego original, además de Total War: WARHAMMER II, podrán acceder a la tercera y colosal campaña. Los jugadores explorarán el épico mapa de un mundo abierto que comprende tanto el Viejo como el Nuevo Mundo y podrán embarcarse en monumentales campañas con cualquiera de las razas que posean de los dos títulos.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/594570/extras/Campaign_Map_EagleGIF.jpg?t=1580144883" >}}
{{< br >}}

## Conquistad el mundo, juntos

{{< br >}}

Cada una de las razas de Total War: WARHAMMER II será plenamente jugable en las campaña para un jugador, multijugador y, también, en las batallas personalizadas y multijugador. Al igual que los dos Señores legendarios de cada raza cuentan con sus propias posiciones de salida, podrás jugar a una campaña cooperativa para dos jugadores con la misma raza. Si posees la primera y la segunda entrega, podrás jugar en multijugador con cualquiera de las razas que tengas

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/594570/extras/Map_ZoomGIF.jpg?t=1580144883" >}}
{{< br >}}

## El mundo de Total War: WARHAMMER II

{{< br >}}

Hace milenios, asediado por una invasión del Caos, un cónclave de magos Altos Elfos forjó un enorme vórtice arcano. Su propósito era extraer los Vientos de la Magia del mundo, generar un gran remolino que los absorbiera para enviar a las hordas demoniacas de vuelta al Reino del Caos. Ahora el Gran Vórtice pierde fuerza y el mundo vuelve a estar al borde de la ruina una vez más.

Poderosas fuerzas deciden restaurar el vórtice y evitar de este modo la catástrofe. Sin embargo, otros desean utilizar su terrible energía para sus malvados fines. Comienza la competición y el mismísimo destino del mundo estará en las manos del vencedor.

El príncipe Tyrion, defensor de Ulthuan, guía a los Altos Elfos en un esfuerzo desesperado por estabilizar el vórtice mientras este gira enloquecedoramente sobre su continente.

Sentado en su trono palanquín, Mazdamundi, el Mago Sacerdote Slann, dirige las huestes de guerra de los Hombres Lagarto hacia el norte desde Lustria. También alberga la esperanza de evitar el cataclismo, aunque los métodos de los Ancestrales deben prevalecer.

Por su parte, el Rey Brujo Malekith y sus sádicas hordas de Elfos Oscuros han partido de Naggaroth y sus laberínticas Arcas Negras. Siente la gran debilidad del vórtice, cuya desaparición les brindaría una gran oportunidad.

Por su parte, los Skaven, liderados por Queek el Coleccionista de Cabezas, se revuelven en sus fétidos y subterráneos túneles. Allí, se multiplican a su antojo y vigilan la superficie con mucha ansia y unos motivos nada claros. Se acerca el momento de la revelación...

Cuatro razas, cuatro desenlaces, un solo objetivo: controlar el Gran Vórtice para bien o para mal.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 18.04 64-bit
* Procesador: 3.4GHz Intel Core i3-4130
* Memoria: 6 GB de RAM
* Gráficos: 2GB Nvidia GTX 680 or better, 2GB AMD R9 285 (GCN 3rd Gen) or better
* Almacenamiento: 52 GB de espacio disponible
* Requires Vulkan
* Nvidia graphics cards require driver version 396.54 (tested)
* AMD GPU's require Mesa 18.1.5 or better (tested)
* Intel GPU's are not supported at time of release

{{< br >}}
{{< br >}}
