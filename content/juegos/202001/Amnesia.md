---
title: 'Amnesia'
date: '2020-01-26 19:20:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura "]
tags: ["Amnesia","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/57300/header.jpg?t=1574253555'
download: 'https://t.me/c/1118272073/11149'
---

# Amnesia

{{< br >}}

Los últimos recuerdos se desvanecen en las tinieblas. Tu mente es un caos y lo único que queda es esa sensación de ser la presa. Debes escapar.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/57300/ss_b27b47bb43ebd1ab3b26e3d74cf2db088e928a6d.600x338.jpg?t=1574253555" >}}
{{< br >}}

## Despierta...

{{< br >}}

Amnesia: The Dark Descent es un juego de horror y supervivencia en primera persona. Un juego sobre la inmersión, el descubrimiento y la vida dentro de una pesadilla. Una experiencia que te helará la sangre.
Vas tropezando por los estrechos pasillos cuando escuchas un grito en la lejanía.
Se está acercando.

{{< br >}}
{{< br >}}

## Explora...

{{< br >}}

Amnesia: The Dark Descent te pone en las botas de Daniel, que despierta en un lúgubre castillo recordando a duras penas algún destello sobre su pasado. Además de explorar los fantasmagóricos senderos, deberás implicarte en los perturbados recuerdos de Daniel. El horror no sólo viene del exterior, también puebla su atormentada mente. Te aguarda una inquietante odisea por los más oscuros rincones de la mente humana.
¿Ese sonido es de pies que se arrastran?. ¿O tu mente te está jugando una mala pasada?

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/57300/ss_db5cf1ca6aa0198d45b9ea892c202bd2bb451e74.600x338.jpg?t=1574253555" >}}
{{< br >}}

## Experimenta...

{{< br >}}

Mediante un mundo totalmente simulado a nivel físico, gráficos 3D de última generación y un sistema dinámico de sonido, el juego no escatima recursos para tratar de absorberte. Una vez que empieces, tendrás el control absoluto de principio a fin. No hay escenas cinemáticas ni saltos en el tiempo, todo lo que ocurra lo vivirás de primera mano.
Algo emerge de la oscuridad. Se aproxima. Con rapidez.

{{< br >}}
{{< br >}}

## Sobrevive...

{{< br >}}

Amnesia: The Dark Descent te lanza de cabeza a un mundo peligroso donde los problemas te acechan tras cada esquina. Sólo te puedes defender escondiéndote, corriendo o utilizando tu buen juicio.
¿Tienes lo que hay que tener para sobrevivir?

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* OS: Ubuntu 12.04 LTS, Mint 13 LTS, Fedora 16, fully updated
* Processor: 2.0Ghz - Low budget CPUs such as Celeron or Duron needs to be at about twice the CPU speed.
* Memory: 2 GB
* Graphics: Radeon X1000/GeForce 6 - Integrated graphics and low budget cards might not work.
* Hard Drive: 3 GB

{{< br >}}
{{< br >}}
