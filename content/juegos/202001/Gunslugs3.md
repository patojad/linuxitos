---
title: 'Gunslugs 3 : Rogue Tactics'
date: '2020-01-21 18:36:00'
type: 'steam'
idiomas: ["ingles"]
category: ["estrategia"]
tags: ["Gunslugs","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1012360/header.jpg?t=1578322636'
download: 'https://t.me/c/1118272073/10960'
---

# Gunslugs 3 : Rogue Tactics

{{< br >}}

Escabúllete por los campamentos enemigos, completa tus misiones y trata de sobrevivir en los niveles, las misiones y los entornos siempre cambiantes en este juego de acción sigilosa y pícaro.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1012360/extras/gs_steam.gif?t=1578322636" >}}
{{< br >}}

Qué puedes esperar del juego: plataformas de acción que eran los juegos originales, pero introducidas con una serie de características que convierten el juego en un juego más sigiloso y táctico orientado con algunas características rogue-lite en la parte superior.

La idea es que eres libre de abordar este juego de la manera que quieras: ¿Escurrirse y tomarse su tiempo para derrotar a los enemigos? ¡usted puede hacer eso! ¿Prefieres apresurarte a través del juego de armas ardientes? ¿Eres así de bueno? ¡Pues adelante!

¿Mi propina? Pruebe un poco de ambos, y sea más táctico sobre su enfoque. A menudo es más inteligente no enfrentarse a un gran grupo de enemigos, y más bien eliminarlos uno por uno o con una bomba adhesiva bien colocada, o una mina terrestre. ¡En otras situaciones, ir al modo Rambo es probablemente la mejor oportunidad de sobrevivir y llegar al helicóptero!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1012360/extras/gs3_blowup_opti.gif?t=1578322636" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* **SO:** Ubuntu 12
* **Procesador:** 2.0 ghz Dual Core
* **Memoria:** 2 GB de RAM
* **Gráficos:** Intel HD3000 or higher with OpenGL 2.1 support
* **Almacenamiento:** 200 MB de espacio disponible
* **Tarjeta de sonido:** OpenAL supported sound card

{{< br >}}
{{< br >}}
