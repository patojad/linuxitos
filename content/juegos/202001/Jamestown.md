---
title: 'Jamestown: Legend of the Lost Colony'
date: '2020-01-03 21:45:00'
type: 'steam'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Jamestown","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/94200/header.jpg?t=1576358511'
download: 'https://t.me/c/1118272073/10767'
---

# Jamestown: Legend of the Lost Colony

{{< br >}}

es un shooter neoclásico en perspectiva cenital para un máximo de 4 jugadores que transcurre en un Marte colonial británico del siglo XVII. Cuenta con toda la intensidad, profundidad y píxeles de un shooter arcade clásico con un toque moderno: un sistema de juego cooperativo totalmente integrado.

{{< br >}}

A diferencia de la mayoría de shooters en perspectiva cenital, los cuales ofrecen a los jugadores un montón de vidas o continues, Jamestown hace algo diferente. Siempre y cuando algún jugador sobreviva, el equipo puede ser devuelto de la muerte. Al ofrecer a cada jugador el poder para rescatar a su equipo de las garras de la destrucción, Jamestown crea una estimulante experiencia cooperativa donde la cooperación de cada uno es lo que realmente importa. Ya sea ganando o perdiendo, tu equipo lo hará unido.

{{< br >}}
{{< br >}}

## Características principales:

{{< br >}}

* Ábrete paso a través de un Marte histórico con un equipo de 4 jugadores en el modo cooperativo por red local
* Maravíllate y regocíjate con las grandes extensiones de píxeles dibujadas a mano
* Escapa hacia una trepidante historia de redención llena de aventuras
* Épicos temas orquestales compuestos por Francisco Cerda, únicos en cada nivel
* Enfréntate en la hostil frontera marciana con tu selección de armamento steampunk de 1619
* Domina una innovadora mecánica cooperativa combinada con un shooter clásico en perspectiva cenital
* Pon a prueba tus habilidades contra más de 20 desafiantes niveles
* Dale un vistazo al modo arcade "Gauntlet" y juega a Jamestown como en 1996
* Aprovéchate del sistema de puntuación único para llegar a lo más alto de las tablas de clasificación online
* Desbloquea más de 30 logros de Steam... ¡si puedes!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Intel® Pentium™ 4 a 2.4 GHz o superior
* Memoria: 512 MB de RAM
* Disco Duro: 250 MB de espacio libre
* Gráficos: Tarjeta de 256 MB de VRAM compatible con OpenGL 2.0
* DirectX®: DirectX® 9.0c
* Extra: Un teclado, controlador o ratón para cada jugador. Soporta múltiples teclados y ratones. Para jugar con ratón necesitas uno de 3 botones

{{< br >}}
{{< br >}}
