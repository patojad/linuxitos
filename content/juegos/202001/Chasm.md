---
title: 'Chasm'
date: '2020-01-24 20:32:00'
type: 'steam'
idiomas: ["español"]
category: ["plataformas"]
tags: ["Chasm","Plataformas","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/312200/header.jpg?t=1578514982'
download: 'https://t.me/c/1118272073/11022'
---

# Chasm

{{< br >}}

Bienvenido a Chasm, un juego de acción y aventura en el que juegas un nuevo recluta que emprende tu primera misión para el Reino de Guildean. Emocionado de demostrar su valía como caballero, rastrea extraños rumores de que se ha cerrado una mina vital para el Reino. Pero lo que descubres en el pueblo minero es peor de lo que imaginabas: la gente del pueblo ha desaparecido, secuestrada por criaturas sobrenaturales que emergen de las profundidades.

{{< br >}}

Con el honor de resolver el misterio y restaurar la paz en el Reino, te embarcas en una aventura épica, con batallas mortales contra monstruos astutos, exploración de antiguas catacumbas y castillos, y nuevos y poderosos equipos ocultos a cada paso. Aunque la historia general es la misma para todos los jugadores, el viaje de tu héroe será único: cada una de las habitaciones ha sido diseñada a mano, y detrás de escena, Chasm une estas habitaciones en un mapa mundial único que Se tu mismo.

{{< br >}}
{{< br >}}

## Características clave

{{< br >}}

* Explore seis áreas masivas ensambladas por procedimientos desde habitaciones hechas a mano.
* Disfrute de un juego retro desafiante y un auténtico pixel art (resolución nativa de 384x216).
* Luche contra jefes masivos y descubra nuevas habilidades para llegar a áreas previamente inaccesibles.
* Personalice su personaje equipando armaduras, armas y deletrea el modo Arcade con desafíos diarios y semanales para Windows, Mac y Linux con soporte completo para Gamepad

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: glibc 2.15+, 64-bit. S3TC support is NOT required.
* Procesador: Dual Core CPU
* Memoria: 1 GB de RAM
* Gráficos: OpenGL 3.0+ support (2.1 with ARB extensions acceptable)
* Almacenamiento: 1 GB de espacio disponible

{{< br >}}
{{< br >}}
