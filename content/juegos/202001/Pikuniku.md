---
title: 'Pikuniku'
date: '2020-01-28 12:20:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura", "plataformas"]
tags: ["Pikuniku","Aventura","Steam","Plataformas"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/572890/header.jpg?t=1572998508'
download: 'https://t.me/c/1118272073/11172'
---

# Pikuniku

{{< br >}}

Es un juego de exploración absurdamente maravilloso que tiene lugar en un mundo extraño pero lúdico en el que no todo es tan alegre como parece. ¡Ayuda a extraños personajes a superar dificultades y empieza una pequeña revolución en esta deliciosa aventura distópica!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/572890/ss_debaf976a994b833851ec930c52a369322f781ca.600x338.jpg?t=1572998508" >}}
{{< br >}}

* Una dinámica aventura para todos: Explora, a tu propio ritmo, un mundo lleno de color. Ayuda a estrafalarios personajes en sus inusuales misiones, y resuelve inteligentes puzzles que desafiarán tanto a pequeños como a mayores.
* Un elenco encantador: conoce en tu viaje a un elenco de personajes memorables, cada uno con sus propios problemas y excentricidades, a las que tendrás que acomodarte a lo largo del camino.
* Modo cooperación: únete a la familia y amigos para una diversión local con varios jugadores, con niveles cooperación customizados y desafíos.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/572890/ss_036e07e0a2f3f78f4745c6c65e06a60b0865e6e7.600x338.jpg?t=1572998508" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 16.04, 18.04, 18.10, Mint 19 x86/x64
* Procesador: Intel Core2 Duo E4300 (2 * 1800) or equivalent
* Memoria: 2 GB de RAM
* Gráficos: GeForce 9600 GT (512 MB)
* Almacenamiento: 300 MB de espacio disponible
* Notas adicionales: Controller Recommended

{{< br >}}
{{< br >}}
