---
title: 'Space Robinson'
date: '2020-01-25 21:56:00'
type: 'steam'
idiomas: ["español"]
category: ["accion"]
tags: ["Space Robinson","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/953870/header.jpg?t=1577074349'
download: 'https://t.me/c/1118272073/11065'
---

# Space Robinson

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/953870/extras/GIF_SPACE_ROBINSON.gif?t=1577074349" >}}
{{< br >}}

Usted es un ingeniero ordinario enviado a la Colonia 21 ubicada en un planeta distante para realizar trabajos de mantenimiento. Terminas sobreviviendo a un accidente de nave espacial y llegas a la Base de Colonia abandonada. Mientras intentas encontrar las respuestas, entiendes que este lugar no es tan amigable como parece. Eres como un Robinson, tratando de sobrevivir en un planeta hostil perdido en el espacio exterior.

{{< br >}}
{{< br >}}

## HARDCORE

{{< br >}}

Este es un juego como un pícaro. Morirás. Morirás. ¡¡¡MORIRÁS!!! Un millón de veces. Luego, el tutorial termina y tendrás la oportunidad de luchar contra monstruos reales.

{{< br >}}
{{< br >}}

## ¡SIEMPRE NUEVO!

{{< br >}}

Cada nivel en 3 ubicaciones grandes se genera de manera procesal, por lo que siempre encontrarás un nuevo lugar para morir. ¿Todavía crees que vas a sobrevivir?

¡Pero espera! Si eres lo suficientemente fuerte como para asaltar un par de niveles, podrías llegar a un puesto avanzado donde puedas repostar y encontrar pistas sobre lo que les sucedió a los colonos.

{{< br >}}
{{< br >}}

## CAMBIO NOCTURNO

{{< br >}}

¿De alguna manera has logrado sobrevivir al día? ¡No se preocupe, este planeta tiene algo de vida nocturna para ofrecer! Experimenta un modo nocturno completamente diferente que te hará ... ¡MORIR, por supuesto!

{{< br >}}
{{< br >}}

## BOTS ASISTENTES

{{< br >}}

Si tienes la suerte, ¡puedes encontrar artefactos y crear bots para ayudarte! Comienza con un lindo cachorro que se convierte en una bestia furiosa una vez que los monstruos están a la vista.

{{< br >}}
{{< br >}}

## ARTESANÍA Y ACTUALIZACIÓN

{{< br >}}

Buenas noticias: ¡la muerte es útil! Una vez que mueras, la IA de la Colonia usará el ADN que has dejado en la Base para crear una copia exacta de ti mismo. También retendrá todos los cristales y artefactos que haya reunido (no nos pregunte cómo sucede; los colonos pueden saber la respuesta, pregúnteles cuando los encuentre). Repara las unidades de la colonia con artefactos y mejora el reactor con cristales para energizar la base. Cada unidad reacondicionada mejorará tus estadísticas y aumentará tus posibilidades de sobrevivir otro minuto.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/953870/extras/EndDescription.gif?t=1577074349" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: 2.4Ghz+
* Memoria: 2 GB de RAM
* Gráficos: 512MB +
* Almacenamiento: 500 MB de espacio disponible

{{< br >}}
{{< br >}}
