---
title: 'Bunker Punks'
date: '2020-01-19 13:36:00'
type: 'steam'
idiomas: ["ingles"]
category: ["shooter"]
tags: ["Bunker Punks","Shooter","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/446120/header.jpg?t=1568763293'
download: 'https://t.me/c/1118272073/10944'
---

# Bunker Punks

{{< br >}}

Bunker Punks es un rápido juego de FPS de luz maliciosa ambientado en un futuro distópico. Dirige un grupo de revolucionarios y asalta fortalezas corporativas para obtener suministros, armas y armaduras. Personaliza tu búnker, entrena a tu pandilla y derriba al gobierno corporativo.

{{< br >}}
{{< br >}}

## Caracteristicas

{{< br >}}

* **JUEGO CLÁSICO DE FPS:** fuertemente inspirado en los juegos clásicos de FPS de principios de los 90: acción de alta velocidad, bombardeo en círculo y descarga de un torrente de balas a los enemigos.
* **EDIFICIO DE BUNKER:** construye y mejora nuevas habitaciones en tu búnker para mejorar tus punks, mejorar tu economía y habilitar nuevas habilidades. Es un árbol de habilidades no lineal para una amplia variedad de estilos de juego.
* **NIVELES GENERADOS ALEATORIO:** cada nivel se genera de forma procesal. El diseño es diferente, los enemigos son diferentes, el botín es diferente, incluso los carteles de propaganda en las paredes son diferentes. Nunca se sabe lo que hay a la vuelta de la esquina.
* **PERSONALIZACIÓN:** combina el botín, los atributos de los personajes y las habitaciones de búnker para personalizar cada personaje de tu pandilla Bunker Punks

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* **Procesador:** 2.5 GHz dual core
* **Memoria:** 2 GB de RAM
* **Gráficos:** GeForce 8600 or equivalent, 256 MB memory
* **Almacenamiento:** 500 MB de espacio disponible

{{< br >}}
{{< br >}}
