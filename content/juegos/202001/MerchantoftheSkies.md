---
title: 'Merchant of the Skies'
date: '2020-01-25 22:18:00'
type: 'steam'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Merchant of the Skies","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1040070/header.jpg?t=1579794974'
download: 'https://t.me/c/1118272073/11081'
---

# Merchant of the Skies

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1040070/extras/gif_option_2.gif?t=1579794974">}}
{{< br >}}

¡En Merchant of the Skies eres el capitán de una aeronave! ¡Vende artículos, reúne recursos y construye tu propio imperio comercial en las nubes! Merchant of the Skies es el juego comercial con elementos ligeros de construcción de bases y magnates. Empiezas como capitán de una pequeña nave espacial y trabajas para establecer tu propia empresa comercial.

Merchant of the skies es un juego experimental: no tiene un género distinto, pero trata de capturar la sensación de viajar por el mundo y establecer tu propia compañía. Eso significa que intentamos incorporar la exploración, la construcción de bases, el comercio y algunos elementos menores de rol en el juego.

{{< br >}}
{{< br >}}

## Características

{{< br >}}

* ¡Conviértase en comerciante! Encuentre las mejores rutas comerciales y benefíciese de ellas
* ¡Descubra el mundo! Viaje entre islas voladoras y enfrente encuentros diferentes
* Juego de Sandbox: haga lo que quiera a su propio ritmo
* Obtenga experiencia y sea más inteligente cuando se trata de comerciar, luego comience su propia producción de mercancías
* Establezca rutas comerciales para transportar y vender mercancías sin su participación directa
* Sea rico y construya su propia mansión con más de 200 bloques de construcción diferentes

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1040070/extras/gif_below.gif?t=1579794974">}}
{{< br >}}

Merchant of the Skies está actualizado en Acceso anticipado. Eso significa que queremos confiar en gran medida en sus comentarios, ¡así que sugiera las mejoras que desee ver y trataremos de acomodarlas lo mejor que podamos! Puede encontrar la hoja de ruta de desarrollo en los foros. Si desea obtener una experiencia completa, espere la versión final.

En función de sus comentarios, decidiremos qué características queremos enfatizar más: exploración, construcción de bases, comercio o jugabilidad basada en objetivos. Planeamos trabajar en todos ellos, pero las prioridades dependerán principalmente de lo que tú, jugador, quieras. La experiencia final dependerá de ti.

La versión final tendrá un objetivo claro y distinguible de unir las islas flotantes y restaurar el mundo a su antigua gloria. Pero por ahora, el juego presenta misiones y objetivos separados basados en metas (¡así como el modo de caja de arena infinita, por supuesto!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 16.04+ / SteamOS
* Procesador: SSE2 instruction set support
* Memoria: 1024 MB de RAM
* Gráficos: shader 3.0 supporting card (i.e. Intel HD Graphics 4000)
* Almacenamiento: 512 MB de espacio disponible

{{< br >}}
{{< br >}}
