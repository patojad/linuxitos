---
title: 'Graveyard Keeper + DLC : Stranger Sins'
date: '2020-01-04 17:39:00'
type: 'steam'
idiomas: ["español"]
category: ["rpg"]
tags: ["Graveyard Keeper","RPG","Steam", "DLC"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/599140/header.jpg?t=1578090505'
download: 'https://t.me/c/1118272073/10800'
---

# Graveyard Keeper + DLC : Stranger Sins

{{< br >}}

Los creadores de Punch Club nos traen Graveyard Keeper, el simulador de gestión de cementerios medievales más impreciso de la historia.

{{< br >}}

Graveyard Keeper es el simulador de gestión de cementerios medievales más impreciso de la historia. Construye y gestiona tu propio cementerio mientras encuentras formas de reducir costes, meterte en otras empresas y usar todos los recursos que encuentres. Este es un juego de capitalismo y de hacer lo que haga falta para conseguir un negocio próspero. Aunque también es una historia de amor.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/599140/extras/body_throw_river.png?t=1578090505" >}}
{{< br >}}

Enfréntate a dilemas éticos. ¿Seguro que quieres comprar carne buena para las hamburguesas del festival de la quema de brujas? ¿O aprovecharás los recursos que tienes a tu alrededor?

{{< br >}}

Reúne valiosos recursos y fabrica objetos nuevos. Amplía tu cementerio para convertirlo en un negocio boyante, reúne recursos valiosos que están desperdigados por las zonas circundantes y explora lo que esta tierra puede ofrecerte.

{{< br >}}

Misiones y cadáveres. Estos cadáveres no necesitarán todos esos órganos, ¿no? ¿Por qué no los trituras y se los vendes al carnicero? O, si te gustan los juegos de rol, no dudes en afrontar misiones.

{{< br >}}

Explora mazmorras misteriosas. Ningún juego medieval estaría completo sin ellas. Viaja a lo desconocido y encuentra nuevos ingredientes útiles, que podrían envenenar (o no) a un montón de aldeanos cercanos.

{{< br >}}
{{< br >}}

## Requisitos del Sistema
{{< br >}}

* SO: Ubuntu 12.04
* Procesador: Intel core i5, 1.5 GHz and up
* Memoria: 4 GB de RAM
* Gráficos: 1 Gb dedicated video card, shader model 3.0+
* Almacenamiento: 1 GB de espacio disponible

{{< br >}}
{{< br >}}
