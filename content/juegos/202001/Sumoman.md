---
title: 'Sumoman'
date: '2020-01-05 17:58:00'
type: 'steam'
idiomas: ["ingles"]
category: ["plataformas"]
tags: ["Sumoman","Plataformas","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/552970/header.jpg?t=1562414087'
download: 'https://t.me/c/1118272073/10820'
---

# Sumoman

{{< br >}}

Es una aventura lógica de plataforma con físicas avanzadas, objetos destructibles y la habilidad de retroceder el tiempo. Trata de resolver los distintos rompecabezas, tratando de mantener al imparable Sumoman en pie durante los desafiantes entornos.

{{< br >}}

Un joven Sumoman regresa a casa desde el torneo de sumo, solo para encontrar a sus compatriotas en un grave peligro. Alguien ha encantado a las habitantes de la isla, condenándolos a un sueño eterno. Nuestro torpe, pero intrépido héroe comienza su viaje a través del hermoso Japón antiguo, para encontrar la fuente de aquella terrible maldición del sueño eterno. Tendrá que superar los enigmáticos artefactos y amenazas que lo rodean para romper el maleficio del sueño y despertar a las personas hechizadas. Sumoman trata de no apresurarse, porque no hay nada peor para un noble héroe, que caerse y quedarse en el suelo indefenso.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/552970/extras/sumoman_gif1.png?t=1562414087" >}}
{{< br >}}

## Сaracterísticas principales

{{< br >}}

* Un personaje principal imparable. Sumoman no puede levantarse si se cae.
* Habilidad de retroceder el tiempo. Cada luchador de sumo puede retroceder el tiempo. ¿No lo sabías?
* Rompecabezas. ¿Puedes resolver algo tan simple como evitar que Sumoman se caiga todo el tiempo?
* Mucho humor y huevos de pascua.
* Pantalla compartida para el modo de 2 jugadores en el que encontrarás retos especiales. Sé más rápido que tu compañero en las carreras de sumos o derrótalo en el modo "Rey de la colina".

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* **SO:** SteamOS, Ubuntu 14.04 64bit
* **Procesador:** Intel Core 2 Quad
* **Memoria:** 2 GB de RAM
* **Gráficos:** NVIDIA GTX 260
* **Almacenamiento:** 2 GB de espacio disponible

{{< br >}}
{{< br >}}
