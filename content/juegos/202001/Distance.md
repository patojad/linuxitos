---
title: 'Distance'
date: '2020-01-08 10:45:00'
type: 'steam'
idiomas: ["ingles"]
category: ["deportes","carreras"]
tags: ["Distance","Deportes","Carreras","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/233610/header.jpg?t=1567725507'
download: 'https://t.me/c/1118272073/10891'
---

# Distance

{{< br >}}

Es un juego de plataformas atmosférico de carreras. Fusionando las carreras de arcade futuristas con parkour, sobrevive en una ciudad mortal, misteriosa y llena de neón saltando, girando y volando.

{{< br >}}

Los caminos son traicioneros con obstáculos en cada esquina. En lugar de hacer vueltas en un bucle, sobrevive hasta el final en el menor tiempo.¡Su automóvil tiene habilidades que no solo le permiten conducir en la pista, sino también al revés y en edificios y paredes! También puedes volar para descubrir nuevos atajos y caminos.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* **SO:** Ubuntu 14.04
* **Procesador:** Intel Core i5 2.5 GHz or AMD FX 4.0 GHz
* **Memoria:** 4 GB de RAM
* **Gráficos:** NVIDIA GeForce GTX 560 or AMD Radeon HD 7750
* **Almacenamiento:** 5 GB de espacio disponible

{{< br >}}
{{< br >}}
