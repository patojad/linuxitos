---
title: 'Slay The Spire'
date: '2020-01-21 21:19:00'
type: 'steam'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Slay The Spire","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/646570/header.jpg?t=1576536521'
download: 'https://t.me/c/1118272073/10978'
---

# Slay The Spire

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/646570/extras/store_silent.gif?t=1576536521" >}}
{{< br >}}

Hemos fusionado juegos de cartas y roguelikes para crear el mejor juego de cartas para un solo jugador que pudiéramos. Crea un mazo único, encuentra criaturas extrañas, descubre reliquias de inmenso poder y mata La Aguja.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/646570/extras/store_ironclad.gif?t=1576536521" >}}
{{< br >}}

## Características

{{< br >}}

* **Construcción dinámica de mazos:** Elige tus cartas sabiamente! Descubre cientos de cartas para añadir a tu baraja con cada intento de escalar la Aguja. Selecciona cartas que funcionen juntas para despachar eficazmente a los enemigos y llegar a la cima.
* **Una Aguja siempre cambiante:** Cada vez que te embarcas en un viaje por la Aguja, el diseño difiere de las anteriores. Elige un camino arriesgado o seguro, enfréntate a enemigos diferentes, elige cartas diferentes, descubre reliquias diferentes e incluso lucha contra jefes diferentes.
* **Reliquias poderosas a descubrir:** Poderosos objetos conocidos como reliquias se pueden encontrar en toda la Aguja. Los efectos de estas reliquias pueden mejorar mucho tu baraja a través de poderosas interacciones. Pero ten cuidado, obtener una reliquia puede costarte más que sólo oro....

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/646570/extras/store_defect.gif?t=1576536521" >}}
{{< br >}}

## Slay the Spire salió de Early Access y viene con:

{{< br >}}

* Tres personajes principales, cada uno de los cuales tiene su propio set de cartas..
* Más de 250 tarjetas totalmente implementadas.
* Más de 150 diferentes objetos a encontrar.
* Más de 50 encuentros de combate únicos.
* Más de 50 eventos misteriosos que pueden ayudarte o dañarte.
* Las Partidas Diarias te permiten compararte con todos los demás jugadores del mundo.
* El Modo Personalizado te permite mezclar y hacer coincidir varios modificadores de partidas locas.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/646570/extras/store_merchant.gif?t=1576536521" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* **SO:** Ubuntu 14.04 LTS
* **Procesador:** 2.0 Ghz
* **Memoria:** 2 GB de RAM
* **Gráficos:** 1Gb Video Memory, capable of OpenGL 3.0+ support (2.1 with ARB extensions acceptable)
* **Almacenamiento:** 1 GB de espacio disponible

{{< br >}}
{{< br >}}
