---
title: 'Tower Of Time'
date: '2020-01-14 18:50:00'
type: 'steam'
idiomas: ["ingles"]
category: ["rpg"]
tags: ["Tower Of Time","RPG","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/617480/header.jpg?t=1579642335'
download: 'https://t.me/c/1118272073/10912'
---

# Tower Of Time

{{< br >}}

Tower of Time es una increíble aventura con más de 50 horas de juego, niveles hechos a mano y una interesante historia narrada mediante escenas cinematográficas animadas. Tower of Time lleva al RPG clásico al siguiente nivel gracias a su desarrollo de personaje flexible, sus miles de piezas de botín y equipamiento, y su sistema de combate en tiempo real complejo y táctico.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/617480/extras/_Titan_1.jpg?t=1579642335" >}}
{{< br >}}

## INTERESANTE HISTORIA DE PROPORCIONES ÉPICAS

{{< br >}}

Sumérgete en un mundo en el que la tecnología y la magia se unen con consecuencias devastadoras. Reúne a un grupo de distintos campeones y lidéralos a través de la torre. Conforme tu grupo de aventureros se acerca a su destino, cientos de libros de saber e informaciones te revelarán poco a poco la historia de Artara.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/617480/extras/C1_st.jpg?t=1579642335" >}}
{{< br >}}

## COMBATE EN TIEMPO REAL AVANZADO

{{< br >}}

Hazte con todo el control estratégico en cada batalla con el sistema de combate Arrow-Time que te permite ralentizar la acción y pausarla. Planea con antelación y coloca a tu grupo donde lo necesitas para conseguir el efecto definitivo. Usa Arrow-Time para reaccionar ante nuevas amenazas, desplegar nuevos hechizos y ataques devastadores, y contraatacar a los enemigos. Nunca hay dos encuentros iguales.

{{< br >}}

* 7 clases de personaje únicas, cada una con distintos puntos fuertes y débiles
* Complejo sistema de habilidades con dos árboles de aumento exclusivos y multitud de opciones
* Gestos de hechizos: dibuja algunos hechizos como quieras.
* Manipulación de la gravedad: salta grandes distancias para dispersar a tus enemigos o atraerlos
* Completo sistema de equipamiento
* 150 enemigos con habilidades y tácticas únicas.
* 50 jefes con distintas habilidades y hechizos.
* Cinco niveles de dificultad, entre los que se incluye la dificultad épica (no recomendada para los jugadores nuevos).
* Múltiples modos de combate que aseguran que hay suficientes retos nuevos.
* Viaja por niveles hechos a mano que han sido diseñados cuidadosamente para ponerte a prueba.
* Nuevo sistema de progreso de personaje.
* Sistema de alineamiento de grupo único.
* Retos de combate diseñados para poner a prueba tus habilidades tácticas y de formación de grupos.
* Creación, encantamiento y objetos legendarios.
* Ciudad mejorable.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* **Procesador:** Intel i5 series or AMD equivalent
* **Memoria:** 16 GB de RAM
* **Gráficos:** NVIDIA GeForce GTX 770 or equivalent
* **DirectX:** Versión 11
* **Almacenamiento:** 12 GB de espacio disponible
* **Tarjeta de sonido:** DirectX compatible sound card
* **Notas adicionales:** 8GB+ RAM decreases loading times to almost zero

{{< br >}}
{{< br >}}
