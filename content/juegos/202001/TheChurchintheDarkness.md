---
title: 'The Church in the Darkness'
date: '2020-01-22 22:38:00'
type: 'steam'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["The Church in the Darkness","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/339830/header.jpg?t=1577207612'
download: 'https://t.me/c/1118272073/10990'
---

# The Church in the Darkness

{{< br >}}

Es el año 1977. Tu sobrino se ha unido a una secta religiosa y se ha ido a una selva de Sudamérica para vivir en Freedom Town. Ahora todo depende de ti: cuélate en la comuna, descubre qué es lo que pasa ahí y decide si necesitas rescatar a tu sobrino o no.

{{< br >}}
{{< img src="" >}}
{{< br >}}

Una secta no hace que te sientas obligado a unirte a ella. Pero te da la bienvenida. Te entiende. Te envuelve hasta que sus palabras se convierten en la verdad absoluta. Te completa.

{{< br >}}

A finales de los años 70, los carismáticos Isaac y Rebecca Walker lideraron la Misión de Justicia Colectiva. Etiquetados como radicales y perseguidos por el gobierno de los Estados Unidos, se trasladaron junto con sus seguidores al lugar donde creyeron que crearían una utopía socialista: las selvas de Sudamérica. Allí levantaron Freedom Town. Pero los familiares de los seguidores, abandonados en los Estados Unidos, comenzaron a preocuparse: ¿qué está pasando exactamente en ese complejo de la selva?

{{< br >}}
{{< img src="" >}}
{{< br >}}

Jugarás como Vic, un exagente de policía que se ha colado en Freedom Town para averiguar el estado de su sobrino Alex. Ya decidas hacerlo por las buenas o por las malas, tendrás que infiltrarte en la comuna, descubrir qué es lo que pasa y localizar a tu sobrino antes de que sea demasiado tarde.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* **Procesador:** Dual Core CPU - 2GHz
* **Memoria:** 4 GB de RAM
* **Almacenamiento:** 2 MB de espacio disponible

{{< br >}}
{{< br >}}
