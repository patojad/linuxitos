---
title: 'Danganronpa 2: Goodbye Despair'
date: '2019-12-30 21:45:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura"]
tags: ["Danganronpa 2","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/413420/header.jpg?t=1576494809'
download: 'https://t.me/c/1118272073/10719'
---

# Danganronpa 2: Goodbye Despair

{{< br >}}

Isla Jabberwock: una vez un popular destino turístico, esta isla ahora deshabitada sigue siendo extrañamente virgen. Usted y sus compañeros de clase en la élite Hope's Peak Academy han sido traídos a esta isla por su maestra súper linda para un "viaje escolar amoroso y palpitante". Todos parecen divertirse bajo el sol ... ¡hasta que Monokuma regresa para reiniciar su juego asesino! Atrapado en esta isla de asesinatos mutuos, su única esperanza de escapar radica en resolver los misterios de la isla. Pero ten cuidado, a veces la verdad puede ser su propia desesperación ...

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/413420/ss_e118a801498f04bf89a3cc5ece78baa95cad0e98.600x338.jpg?t=1576494809" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Windows 7
* Procesador: 2.8 GHz Intel Core 2 Duo or better
* Memoria: 3 GB de RAM
* Gráficos: OpenGL 3.2 compatible GPU with at least 1GB of VRAM
* DirectX: Versión 9.0c
* Almacenamiento: 5 GB de espacio disponible
* Notas adicionales: PS4 or Xbox 360 Controller or Direct Input compatible controller

{{< br >}}
{{< br >}}
