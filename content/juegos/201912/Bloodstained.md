---
title: 'Bloodstained: Ritual of the Night'
date: '2019-12-24 12:03:00'
type: 'steam'
idiomas: ["español"]
category: ["rpg"]
tags: ["Bloodstained","RPG","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/692850/header.jpg?t=1574788164'
download: 'https://t.me/c/1118272073/10600'
---

# Bloodstained: Ritual of the Night

{{< br >}}

Bloodstained es un RPG de acción de desplazamiento lateral centrado en la exploración, y contiene los elementos más populares del género Metroidvania en un solo juego repleto de contenido.

{{< br >}}

En él encarnas a Miriam, una huérfana trágicamente marcada por la maldición de un alquimista, que cristaliza lentamente su cuerpo. Para poder salvarse a sí misma y a toda la humanidad, debe recorrer luchando un castillo infestado de demonios invocados por Gebel, su viejo amigo cuyo cuerpo y mente ya son más cristal que carne.

{{< br >}}
{{< br >}}

## ¿QUÉ OFRECE EL JUEGO?

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/692850/extras/Castle_Dips.gif?t=1574788164" >}}
{{< br >}}

Banda sonora a cargo de la veterana de la industria y de este género Michiru Yamane, e interpretada en directo por una orquesta al completo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/692850/extras/Action_Jackson.gif?t=1574788164" >}}
{{< br >}}

Adictivas mecánicas de juego fluidas y satisfactorias con una fascinante historia nueva al estilo de fantasía gótica de Koji Igarashi, el célebre precursor del género "Igavania".

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/692850/extras/Itsme_yourboy_Gebes.gif?t=1574788164" >}}
{{< br >}}

Cinemáticas e interacciones con PNJ totalmente dobladas por un reparto de estrellas, como los actores David Hayter, Ray Chase y Erica Lindbeck, entre otros. (En serio, en este juego tenemos a un montón de estrellas del doblaje, es una locura...)

Tres personajes jugables en total, dos de los cuales aún no hemos desvelado...

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/692850/extras/Dress_Up.gif?t=1574788164" >}}
{{< br >}}

Literalmente, una tonelada de armas y hechizos que puedes mejorar, subir de nivel y cuyo aspecto puedes personalizar.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/692850/extras/TheOleDragonShootSamuraiGame.gif?t=1574788164" >}}
{{< br >}}

Hermosos gráficos en 2,5D: modelos en 3D diseñados a mano con mucho amor y cuidado, situados en un entorno en 2D. Con un montón de truquitos geniales que no permite el 2D "normal"...

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: AMD FX-4350 / Intel Core i5-4460
* Memoria: 4 GB de RAM
* Gráficos: AMD Radeon R9 280X / GeForce GTX 760
* Almacenamiento: 10 GB de espacio disponible
* Notas adicionales: If you have a potato PC or above, you're ok.

{{< br >}}
{{< br >}}
