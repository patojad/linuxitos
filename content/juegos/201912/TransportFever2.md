---
title: 'Transport Fever 2'
date: '2019-12-17 22:55:00'
type: 'steam'
idiomas: ["español"]
category: ["simulacion"]
tags: ["Transport Fever","Simulacion","steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1066780/header.jpg?t=1576783593'
download: 'https://t.me/c/1118272073/10467'
---

# Transport Fever 2

{{< br >}}

El clásico género de simulación de transporte tiene un nuevo estándar de referencia con Transport Fever 2. Descubre un mundo completamente nuevo recorriendo rutas de transporte por tierra, agua y aire. ¡Abre paso al progreso y la prosperidad!

Proporciona al mundo la infraestructura de transporte que necesita y amasa una fortuna con servicios de transporte personalizados. Observa cómo tus trenes recorren las vías, tus autobuses y camiones transitan las carreteras, tus barcos atraviesan las aguas y tus aviones surcan los cielos. Lleva a la gente al trabajo o a divertirse y sé la razón por la que las ciudades crecen y prosperan. Entrega materias primas y mercancías para impulsar la economía. ¡Vive los mayores desafíos logísticos desde 1850 hasta hoy y levanta un imperio de transporte sin rival en ningún otro lugar del mundo!

El juego libre ofrece una amplia variedad de posibilidades creativas, mientras que el modo de campaña reescribe la historia del transporte en tres continentes. Transport Fever 2 ofrece una selección de más de 200 vehículos de Europa, América y Asia modelados con gran lujo de detalles. Además, con el editor de mapas integrado, puedes recrear paisajes de tres zonas climáticas diferentes. Por último, el realista simulador de transporte y economía, junto con la posibilidad de utilizar mods, ponen la guinda a la experiencia de juego.

{{< br >}}
{{< br >}}

## Ábrete al mundo

{{< br >}}

Un enorme mundo de juego aguarda a tus ferrocarriles, vehículos de carretera, aviones y barcos. Las intuitivas herramientas interactivas de construcción de vías y el sistema de construcción modular para estaciones facilitan la creación y expansión de tu imperio de transporte. Gracias a una amplia gama de opciones de configuración, cada partida en el modo libre ofrece un nuevo desafío, donde estrategias completamente diferentes pueden llevarte al éxito.

{{< br >}}
{{< br >}}

## Escribe la historia del transporte

{{< br >}}

Tres campañas históricas ambientadas en épocas distintas y ubicadas en tres continentes diferentes te pondrán a prueba con una gran variedad de objetivos... ¡Solo los magnates del transporte lograrán completar todas las misiones! Las voces en off y las escenas enriquecen la historia y contribuyen a una experiencia de juego emocionante, mientras que el modo de juego libre te desafiará con diferentes logros para desbloquear.

{{< br >}}
{{< br >}}

## Optimiza la infraestructura

{{< br >}}

Es crucial adaptar tu empresa a las necesidades de la economía y de las ciudades: puentes, túneles, cambios de aguja, señales ferroviarias, calles de sentido único, semáforos y carriles bus son solo algunas de las opciones que tienes para optimizar la infraestructura de transporte. Incluso las estaciones de tren y los aeropuertos se pueden ampliar con módulos para cumplir con diferentes requisitos. Tendrás a tu disposición varias capas de datos que te ayudarán a visualizar los volúmenes de tráfico y de emisiones, además de proporcionar información para mejoras adicionales.

{{< br >}}
{{< br >}}

## Da rienda suelta a tu creatividad

{{< br >}}

Gracias al detallado mundo del juego, puedes desarrollar tu propio paisaje ferroviario virtual: el editor de mapas brinda la posibilidad de crear innumerables mundos diferentes y el terreno en el juego se puede ajustar y pintar. Además, gracias a la posibilidad de usar mods, constantemente se desarrollan creaciones nuevas que están disponibles de forma gratuita a través de Steam Workshop y que se pueden integrar fácilmente en el juego.

{{< br >}}
{{< br >}}

## Características del juego

{{< br >}}

* Juego libre con innumerables posibilidades de configuración
* Tres campañas en tres continentes con más de 20 horas de juego
* Editores para crear mapas y modificar partidas guardadas
* Tres tipos de paisaje: moderado, seco y tropical
* Vehículos de Europa, América y Asia modelados de manera realista
* Más de 200 vehículos: trenes, autobuses, tranvías, camiones, aviones y barcos
* Estaciones de tren, paradas de autobuses y camiones, aeropuertos y puertos modulares
* Simulación de transporte realista que incluye calles de sentido único y semáforos
* Terreno editable que se puede pintar con efectos realistas
* Herramientas de construcción intuitivas para construir ferrocarriles y más
* Visualización de datos importantes, como el tráfico y las emisiones, en capas separadas
* Simulación de ciudades y de economía dinámica
* Más de diez cadenas económicas con fábricas y bienes asociados
* Ciudades con innumerables edificios residenciales, comerciales e industriales
* Mundo de juego detallado con iluminación basada en la física
* Animales terrestres, aves y peces simulados individualmente
* Más de 50 logros desafiantes en el modo de juego libre
* Posibilidad de usar mods a través de Steam Workshop

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 16.04 or higher (64-bit)
* Procesador: Intel i5-2300 or AMD FX-6300
* Memoria: 8 GB de RAM
* Gráficos: NVIDIA GeForce GTX 560 or AMD Radeon HD 7850, 2 GB VRAM

{{< br >}}
{{< br >}}
