---
title: 'Pineview Drive'
date: '2019-12-18 22:15:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura"]
tags: ["Pineview Drive","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/288880/header.jpg'
download: 'https://t.me/c/1118272073/10509'
---

# Pineview Drive

{{< br >}}

**Una sorpresa de terror única en su tipo: ¡un juego consciente de tu miedo!**

{{< br >}}

Una vieja mansión abandonada se encuentra en el callejón sin salida de Pineview Drive. Un hombre con problemas está parado en sus puertas, mirando por encima de la propiedad. Hace 20 años había visitado esta propiedad con su esposa Linda. Durante su estancia, su amada desapareció sin dejar rastro: su misteriosa desaparición sin resolver.

Durante 20 años sin pistas, durante 20 años sin paz, durante 20 años esta casa en Pineview Drive no ha renunciado a su atormentador control sobre él. Ahora, regresa con la esperanza de aceptar su pasado de pesadilla. Las siniestras leyendas que obsesionan esta cabaña junto al mar se han hecho famosas.

Abundan las historias de que nadie ha podido soportar más de 30 días dentro de la mansión desde la desaparición de Linda. Enfrentando una fuerza mayor de lo que alguna vez imaginaste, debes cruzar el umbral y enfrentar el mal que envuelve esta críptica cabaña. Para resolver el misterio de la desaparición de Linda y levantar el velo de esta inquietante historia, no hay tiempo que perder.

{{< br >}}
{{< br >}}

## Caracteristicas

{{< br >}}

El juego observa cada uno de tus movimientos y reacciones: necesitarás nervios constantes si quieres sobrevivir al juego ileso. En esta extraordinaria experiencia de juego, cualquier rastro de miedo tiene un precio: tu vida. Prepárese para un innovador enfriador de columna.

* 30 días en el juego: cada uno te permite profundizar en los secretos de la casa en Pineview Drive
* Se observa cada movimiento y reacción: no te preocupes o perderás una salud preciosa
* Escenografía y efectos de sonido premium: cada tono te dejará sin aliento
* Impresionante atmósfera de terror: enfrenta tus miedos más profundos dentro de la casa

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 14.04 (Trusty Tahr)
* Procesador: Intel® Core™ 2 Duo / AMD® Athlon™ X2, min. 2.8 GHZ
* Memoria: 4 GB de RAM
* Gráficos: Nvidia® / AMD® with 512 MB memory
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
