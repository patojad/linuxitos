---
title: 'The Witcher 2: Assassins of Kings'
date: '2019-12-21 20:30:00'
type: 'steam'
idiomas: ["español"]
category: ["rpg"]
tags: ["The Witcher 2","RPG","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/20920/header.jpg?t=1572786694'
download: 'https://t.me/c/1118272073/10550'
---

# The Witcher 2: Assassins of Kings [Enhanced Edition]

{{< br >}}

a segunda entrega de la saga RPG del cazador de monstruos profesional Geralt de Rivia, The Witcher 2: Assassins of Kings, narra una historia madura que te hará reflexionar, para producir uno de los juegos de rol más elaborados y singulares jamás publicados en PC.

{{< br >}}

Ha llegado una época de caos sin precedentes. Fuerzas poderosas se enfrentan entre bastidores en una lucha por el poder y la influencia. Los Reinos del Norte se movilizan para la guerra. Pero los ejércitos desplegados no son suficientes para detener una sanguinaria conspiración...

{{< br >}}
{{< br >}}

## CARACTERÍSTICAS PRINCIPALES

{{< br >}}
{{< br >}}

### UNA HISTORIA NO LINEAL, MADURA E INMERSIVA

{{< br >}}

* Sumérgete una enorme historia no lineal repleta de emociones y ambientada en un mundo de fantasía sin igual.
* Embárcate en una aventura compleja y extensa en la que cada decisión puede provocar consecuencias nefastas.
* Disfruta de más de 40 horas de juego basado en la narrativa, con 4 comienzos distintos y 16 finales diferentes.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/20920/extras/W2_1.gif?t=1572786694" >}}
{{< br >}}

### COMBATE TÁCTICO ESPECTACULAR Y BRUTAL

{{< br >}}

* Lucha usando un sistema de combate que combina como ninguno la acción dinámica con las mecánicas plenamente desarrolladas de un RPG.
* Utiliza una gran variedad de armas de brujo, entre las que se incluyen opciones para combate cuerpo a cuerpo y a distancia.
* Prepárate para la batalla usando un gran abanico de opciones tácticas: elaborar pociones, tender trampas y señuelos, lanzar hechizos mágicos y acercarte sigilosamente a los enemigos.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/20920/extras/W2_2.gif?t=1572786694" >}}
{{< br >}}

### UN MUNDO DE JUEGO INMENSO, CONSISTENTE Y REALISTA

{{< br >}}

* Descubre un mundo de juego enorme y rico, donde los sucesos siniestros determinan las vidas de poblaciones enteras y los monstruos sedientos de sangre arrasan todo a su paso.
* Explora numerosos y variados lugares: desde bulliciosos puestos comerciales, pasando por ajetreadas ciudades mineras, hasta imponentes castillos y fortalezas; y descubre las historias que tienen que contarte y los secretos que ocultan.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/20920/extras/W2_3.gif?t=1572786694" >}}
{{< br >}}

### TECNOLOGÍA INNOVADORA

{{< br >}}

* Experimenta un mundo vivo, que incluye preciosos gráficos y utiliza mecánicas de juego sofisticadas, que han sido posibles gracias a REDengine, la tecnología propia de CD PROJEKT RED.

{{< br >}}
{{< br >}}

## ACERCA DE THE WITCHER 2 ENHANCED EDITION

{{< br >}}

La Enhanced Edition incluye montones de contenidos nuevos y emocionantes.

{{< br >}}

* Horas adicionales de juego: nuevas aventuras ambientadas en lugares nunca antes vistos, que amplían la historia y dan a conocer a nuevos personajes, misterios y monstruos.
* Introducción y cinemáticas del juego nuevas: animaciones y secuencias totalmente nuevas, incluida una nueva cinemática prerrenderizada por el ganador del BAFTA y nominado a un premio de la Academia Tomasz Bagiński.
* Todos los DLC y mejoras introducidos en la versión 2.0 del juego, incluyendo:
* Modo Arena — un modo arcade que permite a los jugadores luchar contra oleadas interminables de enemigos y poner a prueba sus habilidades de combate.
* Un nuevo y extenso sistema de tutoriales que sumerge de forma gradual y fluida a los jugadores en el mundo del juego y las aventuras de Geralt.
* Modo Oscuro — un nivel de dificultad diseñado para los jugadores expertos, con objetos exclusivos de temática siniestra. En este nivel de dificultad se pone un mayor énfasis en la preparación de la batalla, las maniobras defensivas y los ataques oportunistas.


{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* OS: Ubuntu 14.04, Linux Mint 17, Steam OS
* Processor: Intel Core 2 Duo
* Memory: 4 GB RAM
* Graphics: GeForce 9800 GT 512MB (1280x720, low)
* Hard Drive: 25 GB HD space

{{< br >}}
{{< br >}}
