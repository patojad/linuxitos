---
title: 'Quern - Undying Thoughts'
date: '2019-12-25 19:20:00'
type: 'gog'
idiomas: ["español"]
category: ["puzzle"]
tags: ["Quern","Puzzle","GOG"]
img: 'https://images.gog.com/194ff824257c225989460bff113b8503e336549cf7637534f19684ba1d10409e_product_card_v2_thumbnail_271.jpg'
download: 'https://t.me/c/1118272073/10626'
---

# Quern - Undying Thoughts

{{< br >}}

Descubra la verdad sobre el pasado de Quern, descubra los misterios de su presente y sea el explorador que da forma a su futuro. Quern introduce la mecánica de rompecabezas reutilizable en el género clásico, haciendo que el jugador piense en el juego en su conjunto, y no solo como una serie de desafíos individuales.

{{< br >}}

Intenta aclarar su visión y ponerse de pie después de su llegada. Oyes que el portal se cierra detrás de ti mientras das un paso adelante. Estás atrapado, tu camino de regreso se ha ido.

{{< br >}}
{{< img src="https://images.gog.com/57d90e8b48a01ec02287fe0de45b975daa07b3f2bb68c70989ce678f8feffda3_product_card_v2_thumbnail_271.jpg" >}}
{{< br >}}

Sumérgete en el hermoso paisaje de la isla mientras descubres nuevos lugares. Siga los consejos del pasado a medida que profundiza en la historia para comprender la importancia de su presencia.

{{< br >}}

* Quern es un juego de aventura de rompecabezas en primera persona con una historia cautivadora, hermosos gráficos y una banda sonora atmosférica para crear el ambiente.
* Quern usa controles en primera persona, por lo que el jugador puede explorar libremente y pasear por la isla.
* La clave del éxito es comprender los principios operativos de la isla.
* El juego presenta mecánicas de rompecabezas reutilizables para el género clásico. El jugador crece para llegar al fondo de cada desafío, ya que es esencial para progresar y sumergirse en el mundo del juego.
* Un cuaderno en el juego ayuda al jugador a memorizar pistas y conexiones de imágenes de la isla.

{{< br >}}
{{< br >}}
