---
title: 'Fahrenheit: Indigo Prophecy Remastered'
date: '2019-12-29 16:17:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura"]
tags: ["Fahrenheit","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/312840/header.jpg?t=1574281669'
download: 'https://t.me/c/1118272073/10687'
---

# Fahrenheit: Indigo Prophecy Remastered

{{< br >}}

Originalmente lanzado en 2005, Fahrenheit (conocido como Indigo Prophecy en América del Norte) fue un gran avance en la narrativa interactiva, oscilando entre los mundos del cine y los juegos, al tiempo que los abarcaba, creando su propio género único en el panorama del entretenimiento. Esta edición recientemente remasterizada presenta gráficos actualizados, soporte de controlador Y se basa en la versión internacional del juego sin cortes/censura.

{{< br >}}

Con una rica narrativa multicapa, una presentación innovadora y una partitura musical escalofriante del famoso compositor de Hollywood Angelo Badalamenti, Fahrenheit: Indigo Prophecy Remastered sirve como la versión definitiva del misterio del asesinato sobrenatural y (re) presenta el título innovador para los fanáticos antiguos y nuevos por igual. .

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: SteamOS, Ubuntu 14.04
* Procesador: Intel Core i3, AMD A10
* Memoria: 4 GB de RAM
* Gráficos: NVIDIA GeForce 260 | ATI Radeon HD 5450 | Intel HD 4400
* Almacenamiento: 15 GB de espacio disponible
* Tarjeta de sonido: required
* Notas adicionales: Supported Gamepads: Microsoft Xbox 360 Controller for Windows (Wired), Microsoft Xbox 360 Games for Windows Wireless Controller with Adapter, & Logitech Wireless Gamepad F710

{{< br >}}
{{< br >}}
