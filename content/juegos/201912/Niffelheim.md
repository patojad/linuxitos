---
title: 'Niffelheim Supporter Pack'
date: '2019-12-27 19:20:00'
type: 'steam'
idiomas: ["español"]
category: ["rpg"]
tags: ["Niffelheim","RPG","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/351100/header.jpg?t=1572960922'
download: 'https://t.me/c/1118272073/10667'
---

# Niffelheim Supporter Pack

{{< br >}}

Tu avater es un valiente guerrero que ha caído en batalla. Pero en lugar de la bien merecida paz en Asgard, su alma está atrapada en el cruel mundo de Niffelheim. Sobrevive en este mundo hostil, saquea las tierras vecinas, explora las peligrosas mazmorras y encuentra tu camino a Valhalla.

{{< br >}}
{{< br >}}

## Sobrevive en el inframundo

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/351100/extras/trailer_2018_friends_sm.png.png?t=1572960922" >}}
{{< br >}}

Incluso después de morir los guerreros siguen siendo guerreros. Toma el camino de la última prueba y demuestra a los dioses lo que vales. El frío, la oscuridad y la muerte nunca te aterraron en vida, tampoco lo harán ahora. Abastécete de provisiones, defiéndete y descubre los misterios de estas tierras.

{{< br >}}
{{< br >}}

## No te conviertas en uno de los olvidados.

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/351100/extras/line_13_ex2.png?t=1572960922" >}}
{{< br >}}

Destruye a cualquiera que se atreva atacar tus tierras. ¡Cada enemigo vencido será un escalón hacia Asgard! Huesos, dientes, pieles: todo te servirá de arma o armadura. Levanta una fortaleza que no solo te protegerá de los ataques, sino también te ayudará a vencer a tus enemigos. Los Sacerdotes de la Muerte te impondrán desafíos. ¿Los cumplirás o los rechazaras para unirte a la batalla de sus siervos muertos?

{{< br >}}
{{< br >}}

## Encuentra la salida

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/351100/extras/line_13_ex2.png?t=1572960922" >}}
{{< br >}}

Reúne todos los fragmentos del portal hacia Asgard. Pero ten cuidado, pues poderosos gigantes, nomuertos y arañas mortíferas custodian cada fragmento.

{{< br >}}
{{< br >}}

## Crea

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/351100/extras/line_13_ex2.png?t=1572960922" >}}
{{< br >}}

Para ser más fuerte prepara todo tipo de pociones de setas y bestias muertas. Cocina deliciosos platos e invita a los amigos a un buen festín. Crea un veneno tan potente que sea capaz de tumbar hasta al trol más duro.

{{< br >}}
{{< br >}}

## Explora cuevas antiguas

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/351100/extras/line_13_ex2.png?t=1572960922" >}}
{{< br >}}

En el núcleo de la tierra encontrarás potentes artefactos que te ayudarán a ganar la batalla final. ¡Descubre lo que esconden las tinieblas!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04
* Procesador: 1.7 GHz Dual Core
* Memoria: 4 GB de RAM
* Gráficos: NVIDIA GeForce GTX 650Ti
* Almacenamiento: 1 GB de espacio disponible
* Notas adicionales: 4:3 and 21:9 screen ratio supported

{{< br >}}
{{< br >}}
