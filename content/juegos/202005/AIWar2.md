---
title: 'AI War 2'
date: '2020-05-25 16:22:00'
type: 'gog'
idiomas: ["ingles"]
category: ["estrategia"]
tags: ["AI War 2", "Estrategia", "GOG"]
img: 'https://images.gog-statics.com/9faf29258f33d318b7cd36cd36e94a6f8ce7b254aa274066254f663aa07d49b7.jpg'
download: 'https://t.me/c/1118272073/12913'
---

# AI War 2

{{< br >}}

Es una gran estrategia/RTS híbrido contra una galaxia que ya ha sido conquistada por una inteligencia artificial deshonesta. También es "una secuela de la enorme guerra de inteligencia artificial RTS de [Arcen], que llamamos **'uno de los mejores juegos de estrategia de este año'** en 2009" (Tom Sykes, PC Gamer)

{{< br >}}

##### La inteligencia artificial más desviada y aclamada en los juegos de estrategia vuelve ... con una gran cantidad de enemigos mutuos.

{{< br >}}

Enfréntate a una versión más avanzada de la IA original, que una vez más ha capturado toda la galaxia dejándote solo un pequeño planeta para ti. Luego ponte en marcha y encuentra una manera de burlarlo hábilmente. Todos los nuevos capturables, flotas más grandes y habilidades de pirateo seguramente ayudarán. (Lo va a necesitar).

O sumérjase en una lucha galáctica mucho más complicada que involucra nanocausto, macrófagos, esferas de Dyson y más. Cada una de las otras facciones tiene sus propios objetivos, reglas, unidades y economías completamente únicas. Haga que el escenario sea lo suficientemente complicado y puede convertirse en "La XV Guerra Mundial está en progreso, usted está en una pequeña granja en el medio, a nadie le gustas, pero si puedes matar a ese líder enemigo enojado gigante, todo será terminado."

Si eso suena exagerado, y francamente, esa parte nos está cansando de imaginar a pesar de que algunas personas lo buscan, entonces tómese un descanso y tal vez piratee el virus informático que todo lo consume para que sea su aliado, y convenza a la estrella. colmenas alienígenas de tamaño para vigilar tu espalda mientras te enfrentas a una IA repentinamente menos arrogante.

{{< br >}}
{{< br >}}

## ¿Qué tal algunos puntos destacados?

{{< br >}}

* Muchas facciones opcionales, cada una con sus propios objetivos y estrategias, crean una galaxia viva.
* Un nuevo sistema de flotas te ofrece aún más barcos que antes y te permite personalizar tu imperio más que nunca.
* Una tonelada de tipos de mapas, y con muchas subopciones para hacerlos aún más variados.
* Outguard para contratar, facciones para aliarse y montones de objetivos para capturar o piratear, con la IA o los extraterrestres.
* Las sub-flotas de Guardianes, Cazadores y Guardias Pretorianos de la IA brindan nuevos desafíos (y a veces oportunidades) en la forma en que tanto usted como la IA interactúan entre sí.
* Modificabilidad loca, con muchas palancas disponibles en XML de fácil acceso.

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* Sistema: Ubuntu 16.04+
* Procesador: CPU de doble núcleo de 64 bits (CPU de doble núcleo de 2.2+ GHz o superior)
* Memoria: 4 GB de RAM
* Gráficos: NVIDIA GTX 510+, Radeon HD5900 + o Intel HD4000 +
* Almacenamiento: 4 GB de espacio disponible
* Otro: Requiere un procesador de 64 bits y un sistema operativo

{{< br >}}
{{< br >}}
