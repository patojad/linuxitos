---
title: 'FarSky'
date: '2020-05-13 16:26:00'
type: 'steam'
idiomas: ["ingles"]
category: ["supervivencia","sandbox","aventura"]
tags: ["FarSky","Supervivencia","Sandbox","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/286340/header.jpg'
download: 'https://t.me/c/1118272073/12554'
---

# FarSky

{{< br >}}

Es un juego de supervivencia corto en las profundidades del océano. Recoge todas las piezas de tu submarino y alcanza la superficie.

FarSky es un juego de supervivencia corto con casi 2 horas de juego.

Tomas el papel de Nathan, que se perdió en el océano después del accidente de su submarino. Debes aprender a sobrevivir en las profundidades del océano.

{{< br >}}

* Usa el entorno para reunir recursos
* Construye una base para rellenar tu oxígeno y administrar tus artículos
* Crea equipos y armas para explorar y protegerte en las profundidades del océano
* Cree una granja en la base o vaya a cazar peces para alimentarse

{{< br >}}

Tu objetivo final es encontrar todas las piezas de tu submarino, arreglarlo y llegar a la superficie. También puedes elegir jugar en modo Sandbox para disfrutar del juego libremente. Todos los mapas se generan aleatoriamente para sumergirte en un mundo desconocido.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04
* Procesador: 2.2 GHz Intel Core 2 Duo
* Memoria: 2 GB de RAM
* Gráficos: Dedicated graphics card (OpenGL 2.0)
* Almacenamiento: 500 MB de espacio disponible
* Notas adicionales: Java required

{{< br >}}
{{< br >}}
