---
title: 'Shadow of the Tomb Raider Definitive Edition'
date: '2020-05-19 15:42:00'
type: 'steam'
idiomas: ["ingles"]
category: ["supervivencia","aventura","accion"]
tags: ["Shadow of the","Tomb Raider","Definitive Edition","Supervivencia","Accion","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/750920/header.jpg'
download: 'https://t.me/c/1118272073/12727'
---

# Shadow of the Tomb Raider Definitive Edition

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/750920/extras/Shadow_DE_616X_Agnostic.jpg" >}}
{{< br >}}

En Shadow of the Tomb Raider Definitive Edition, vive el capítulo final de la historia sobre el origen de Lara, en el que se convierte en la saqueadora de tumbas que está destinada a ser. Esta edición incluye el juego básico y siete tumbas de desafío de contenido descargable, así como armas, atuendos y habilidades descargables.

{{< br >}}

**Sobrevive en el lugar más mortal de la Tierra**: domina un entorno selvático inclemente. Explora entornos subacuáticos llenos de cavidades y sistemas de túneles.

**Sé uno con la selva**: en inferioridad numérica y mal equipada, Lara debe aprovechar la selva en su favor. Ataca por sorpresa y desaparece como un jaguar, camúflate en el barro y desata el pánico entre los enemigos para sembrar el caos.

**Descubre tumbas oscuras y despiadadas**: son más aterradoras que nunca, pues se requieren técnicas avanzadas para llegar a ellas y, una vez dentro, están repletas de puzles mortales.

**Desentierra la historia viva**: descubre la Ciudad Oculta y explora la mayor instalación jamás vista en un juego de Tomb Raider.

{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/750920/extras/SOTTR-Combat1-616x213_-_Copy.jpg" >}}

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* SO: Ubuntu 18.04 64-bit 
* Procesador: 3.4GHz Intel Core i3-4130 
* Memoria: 8 GB de RAM
* Gráficos: 2GB AMD R9 285 (GCN 3rd Generation), 2GB Nvidia GTX 680 
* Notas adicionales:
  * Requires Vulkan.
  * Nvidia requires 418.56 or newer drivers.
  * AMD requires Mesa 19.0.1 or newer.
  * AMD GCN 3rd Gen GPU's include the R9 285, 380, 380X, Fury, Nano, Fury X.
  * Intel GPUs are not supported.
  * Intel GPUs are not supported at time of release.

{{< br >}}
{{< br >}}
