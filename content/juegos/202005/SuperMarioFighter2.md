---
title: 'Super Mario Fighter 2 (M.U.G.E.N.)'
date: '2020-05-28 12:09:00'
type: 'otras'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Super", "Mario", "Fighter 2", "MUGEN", "Accion", "Otras"]
img: 'https://m.gjcdn.net/game-header/1400/494130-mbvnd5vm-v4.jpg'
download: 'https://t.me/c/1118272073/13039'
---

# Super Mario Fighter 2

{{< br >}}
{{< img src="https://m.gjcdn.net/game-screenshot/300/3209711-643m4qmq-v4.png" >}}
{{< br >}}

¡Mario y sus amigos regresan en un nuevo juego de lucha lleno de acción!

¡Juega como 8 personajes (y más por venir) en múltiples etapas únicas!

Cada personaje es diferente, ¡así que encontrar un personaje principal será diferente para todos!

{{< br >}}
{{< br >}}
