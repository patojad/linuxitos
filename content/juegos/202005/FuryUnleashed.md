---
title: 'Fury Unleashed'
date: '2020-05-14 16:49:00'
type: 'steam'
idiomas: ["español"]
category: ["accion"]
tags: ["Fury Unleashed","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/465200/header.jpg'
download: 'https://t.me/c/1118272073/12588'
---

# Fury Unleashed

{{< br >}}

Es un juego roguelite de plataformas y acción con combos. Cada vez que matas, aumentas tu combo. ¡Alcanza ciertos límites y se activarán tu resistencia al daño y tus poderes curativos! Es un juego que incluso puedes acabar con un solo combo definitivo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/465200/extras/steam_gif_02_big.gif" >}}
{{< br >}}

**Fury Unleashed** se creó combinando la inspiración de modernos juegos de plataformas roguelite (como **Dead Cells** y Rogue Legacy) con recuerdos nostálgicos de juegos de plataformas de la vieja escuela (como **Contra** y **Metal Slug**). Hemos estado puliendo nuestra creación durante cinco años para asegurarnos de que tu experiencia con el juego sea tan memorable como con los títulos ya mencionados. Creemos de corazón que no te decepcionará.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/465200/extras/steam_gif_01_big.gif" >}}
{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}


* SO: Ubuntu 18.04 or newer
* Procesador: Intel Core i5 3.0GHz
* Memoria: 4 GB de RAM
* Gráficos: Nvidia GeForce GTX 650 / Radeon HD 7510
* Almacenamiento: 1600 MB de espacio disponible

{{< br >}}
{{< br >}}
