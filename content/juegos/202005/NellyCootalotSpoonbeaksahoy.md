---
title: 'Nelly Cootalot: Spoonbeaks ahoy!'
date: '2020-05-23 17:05:00'
type: 'otras'
idiomas: ["español"]
category: ["aventura"]
tags: ["Nelly Cootalot", "Spoonbeaks ahoy!", "Aventura", "Otras"]
img: 'https://www.adventuregamestudio.co.uk/images/games/860_1_thumb_200.jpg'
download: 'https://t.me/c/1118272073/12875'
---

# Nelly Cootalot: Spoonbeaks ahoy!

{{< br >}}

Nelly Cootalot, temible pirata y amante de criaturas diminutas y adorables, es acusada por el espíritu de un bucanero muerto para investigar la misteriosa desaparición de una flota de pájaros conocidos como cucharillas.

Su aventura la enfrentará con el nefasto barón Widebeard mientras descubre la corrupción y los castigos desenfrenados en la Baronía de Meeth.

Este es mi primer juego AGS completo. Es de longitud media, y debería tomar algunas horas para jugar. Lo hice para, y sobre, mi novia. ¡Ella lo disfrutó y espero que tú también lo hagas!

{{< br >}}
{{< br >}}
