---
title: 'Dead Age'
date: '2020-05-27 16:10:00'
type: 'steam'
idiomas: ["español"]
category: ["rpg"]
tags: ["Dead Age","RPG","steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/363930/header.jpg'
download: 'https://t.me/c/1118272073/13004'
---

# Dead Age

{{< br >}}

Maneja a los supervivientes, crea materiales y toma decisiones imposibles. Defiende tu campamento contra hordas de zombis para prevalecer en este juego no linear (comparable a FTL). ¡Innovador juego indie de supervivencia-RPG!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/363930/extras/logo_deadage_ohneblut_neu.png" >}}
{{< br >}}

¡Sobrevive el apocalipsis zombi con combates por turnos y muertes permanentes! Maneja supervivientes, sal a saquear en corridas peligrosas, construye alianzas, crea equipos, toma decisiones que impactan en la historia, defiende tu campamento contra hordas de zombis y experimenta un juego con elementos de rouge. ¡Innovador juego indie de supervivencia-RPG!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/363930/extras/survive_SP.png" >}}
{{< br >}}

Justo luego del brote zombi pudiste unirte a un grupo de supervivientes y esconderte en su campamento. ¡Pero eso no evita que haya peligro! La comida se acaba, los supervivientes lastimados deben ser atendidos y necesitas materiales para mantener el campamento. La amenaza zombi aumenta continuamente y grupos de supervivientes hostiles te mantienen a la defensiva.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/363930/extras/FF_combat_SP.png" >}}
{{< br >}}

Cada superviviente debería estar protegido porque los necesitas para defenderte de los peligros del apocalipsis. Consigue suministros en áreas infestadas de zombis, crea equipos para sobrevivir y compra y vende objetos para mantener a todos vivos el tiempo suficiente como para aprender las habilidades necesarias para sobrevivir. El sistema de combate tiene sus raíces en juegos de rol clásicos. Puedes luchar estratégicamente, aprender a utilizar distintos tipos de armas y usar una variedad de bombas o trampas.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/363930/extras/rogue_lite_SP.png" >}}
{{< br >}}

Si fallas y no sobrevives el apocalipsis, puedes comprar mejoras con las medallas que hayas conseguido en las partidas anteriores para darle a tu nuevo personaje ciertas herramientas. Desbloquea profesiones pre-apocalípticas para tu personaje y especialízate en varios trabajos. Cada nueva partida ofrece distintas misiones aleatorias y supervivientes para conocer.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/363930/extras/endings_SP.png" >}}
{{< br >}}

Una característica alabada del Dead Age es su historia no linear con consecuencias reales. Las decisiones que tomas en situaciones conflictivas afectan el futuro. Puedes elegir ser un héroe y salvar a más supervivientes o dejar que mueran y ahorrar suministros. Puedes formar parejas con otros o empezar rivalidades que pueden traer desastrosas consecuencias.
Los eventos diarios ofrecen nuevos niveles de peligro en situaciones en las cuales debes tomar decisiones que afectan las posibilidades de que sobreviva tu campamento. Vive lo suficiente y podrás desbloquear los seis posibles finales.

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* SO: Ubuntu 12.04 or later
* Procesador: 2.4ghz Intel Core 2 Duo or equivalent
* Memoria: 2 GB de RAM
* Gráficos: NVIDIA GeForce GTX 260 or Radeon HD 4850 (512 MB VRAM)
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
