---
title: 'Fort Triumph'
date: '2020-05-03 19:08:00'
type: 'gog'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Fort Triumph","Estrategia","GOG"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/612570/header.jpg'
download: 'https://t.me/c/1118272073/12365'
---

# Fort Triumph

{{< br >}}

Juego de estrategia que combina los combates por turnos de XCOM con la exploración de HOMM. ¡Construye ciudades, consigue artefactos, mejora a los héroes y usa las físicas para valerte del entorno!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/612570/extras/Fort_Triumph_Steam_Description_GIF_5.gif" >}}
{{< br >}}

Fort Triumph es un juego de fantasía y estrategia por turnos en el que la muerte permanente está a la vuelta de la esquina. ¿Sientes la presión?

Lidera a una tropa de héroes con habilidades variopintas y disfruta de una parodia de fantasía. Te esperan misiones con varios objetivos en mapas divididos en casillas. ¡Prepárate para una gran aventura en el modo historia y en las escaramuzas!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/612570/extras/Fort_Triumph_Steam_Description_GIF_6.gif" >}}
{{< br >}}

* Aprovéchate del entorno: ¿Un árbol? ¿Una piedra? Aquí vale todo como arma.
* Elabora estrategias: Elige entre cuatro facciones y clases. ¡Construye una base, consigue recursos y aprende habilidades!
* Mejora a los héroes: ¡Adquiere rasgos y habilidades para crear héroes únicos!
* Explora mapas generados por procedimientos: Fort Triumph cuenta con mapas versátiles y con ubicaciones y eventos aleatorios. ¡No habrá ni una batalla igual!
* Dale caña al volumen: Marco Valerio Antonini ha compuesto la música de este juego.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 16.04+, SteamOS+ (64bit)
* Procesador: Intel I3 4160 processor or equivalent
* Memoria: 8 GB de RAM
* Gráficos: NVIDIA GeForce GT 630 or Equivalent
* Almacenamiento: 3 GB de espacio disponible

{{< br >}}
{{< br >}}
