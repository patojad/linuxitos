---
title: 'VirtuaVerse'
date: '2020-05-16 17:56:00'
type: 'gog'
idiomas: ["español"]
category: ["aventura"]
tags: ["VirtuaVerse","Aventura","GOG"]
img: 'https://items.gog.com/virtuaverse/Nathan_city_steam.gif'
download: 'https://t.me/c/1118272073/12676'
---

# VirtuaVerse

{{< br >}}
{{< img src="https://items.gog.com/virtuaverse/Nathan_city_steam.gif" >}}
{{< br >}}

En un futuro no tan lejano, una inteligencia superior prevalece sobre todas las demás IA. La sociedad está migrando a una realidad permanentemente integrada conectada a una única red neuronal, optimizando continuamente la experiencia del usuario mediante el procesamiento de datos personales.

Nathan, un extraño, se gana la vida fuera de la red como contrabandista de hardware modificado y software descifrado. Diseñado con un auricular personalizado, se encuentra entre los pocos que pueden apagar el AVR y ver la realidad como realmente es. Nathan comparte un apartamento con su novia Jay, un talentoso artista de graffiti AVR cuyos drones rocían el tecno-color en todo el espacio aumentado de la ciudad.

Una mañana, Nathan se despierta en un departamento vacío y descubre un mensaje críptico en el espejo del baño. Habiendo roto accidentalmente sus auriculares, Nathan está desconectado pero decidido a descubrir qué le sucedió a Jay. Se embarca en un viaje increíble que involucra grupos de hackers y gremios de tecnólogos AVR.

Al atravesar el mundo, Nathan se enfrenta a cementerios de hardware, arqueología digital, tribus de criptoshamanes y libertinaje de realidad virtual.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema: Ubuntu 16.04 de 64 bits
* Procesador: 2 GHz
* Memoria: 2 GB de RAM
* Gráficos: Intel HD 3000
* Almacenamiento: 1 GB de espacio disponible

{{< br >}}
{{< br >}}
