---
title: 'Agent A: A puzzle in disguise'
date: '2020-05-11 18:10:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura"]
tags: ["Agent A","A puzzle in disguise","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/801480/header_spanish.jpg'
download: 'https://t.me/c/1118272073/12486'
---

# Agent A: A puzzle in disguise

{{< br >}}

El cuartel general acaba de enviar tu nueva misión. Una espía enemiga llamada Ruby La Rouge anda detrás de nuestros agentes secretos. Tu misión es encontrarla y capturarla.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/801480/extras/7e956710-c5e6-479e-824c-1e4ff4ced96c.gif" >}}
{{< br >}}

Jugarás como Agent A, y tu misión (si decides aceptarla) es infiltrarte en el escondite secreto de Ruby La Rouge y detener o neutralizar al objetivo.

Descubre un sofisticado mundo sesentero lleno de inventos retrofuturistas, cacharros ocultos, dispositivos e inteligentes puzles lógicos. Pero date por avisado... ¡Ruby La Rouge no es para tomársela a broma! Explora un laberinto de puzles desconcertantes en este retorcido juego del gato y el ratón en el que terminarás por preguntarte si eres el gato... ¡o el ratón!

¿Has visto algo raro o fuera de lugar? Tomar notas mentales y hacer observaciones (como haría un buen agente secreto) te ayudará a resolver puzles complicados más adelante. Cuando explores el escondite secreto de Ruby, coger objetos y usarlos de forma creativa será vital para desbloquear una serie de puzles que te irán acercando cada vez más a tu objetivo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/801480/extras/AgentA_ControlRoom.gif?t=1588265048" >}}
{{< br >}}

• Sofisticado diseño inspirado en la década de los 60
• 35 entornos para explorar
• 100 puzles basados en el inventario
• 50 pantallas de puzle
• 30 logros disponibles para el cazador de trofeos que todos llevamos dentro.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Intel i3 2.0 GHz
* Memoria: 2 GB de RAM
* Gráficos: 1GB Shader Model 3.0 Compatible (DirectX 9.0c)
* DirectX: Versión 9.0c
* Almacenamiento: 1 GB de espacio disponible

{{< br >}}
{{< br >}}
