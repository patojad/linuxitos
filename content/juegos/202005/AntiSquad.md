---
title: 'AntiSquad'
date: '2020-05-24 15:32:00'
type: 'indiegala'
idiomas: ["ingles"]
category: ["estrategia"]
tags: ["AntiSquad", "Estrategia", "IndieGala"]
img: 'https://www.indiegalacdn.com/store-img_game/games/medium/Antisquad.jpg'
download: 'https://t.me/c/1118272073/12889'
---

# AntiSquad

{{< br >}}

Ampliamente conocidos en pequeños círculos como AntiSquad, son una unidad de respuesta táctica militar. Estos profesionales militares endurecidos, cansados ​​de la burocracia, la política y el servicio a las fuerzas armadas, han establecido una formación paramilitar privada ubicada en el sur de los Estados Unidos. Siendo verdaderos profesionales militares, periódicamente ejecutan contratos para trabajos que pueden no considerarse "limpios".

El equipo consta de muchas personas complejas con su propia historia de fondo, pero no rechazan a los solicitantes mercenarios. Los miembros del equipo son idealistas; se detienen en cuestiones morales, así como cuestionan el nacionalismo ciego, etc. AntiSquad difiere fuertemente de las unidades militares. Realmente son un montón de

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* Procesador: Core 2 Duo
* Memoria: 2 GB de RAM
* Disco duro: 1 GB de espacio disponible
* Tarjeta de sonido: SB16 compatible

{{< br >}}
{{< br >}}
