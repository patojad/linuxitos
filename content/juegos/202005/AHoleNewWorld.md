---
title: 'A Hole New World'
date: '2020-05-01 17:05:00'
type: 'steam'
idiomas: ["español"]
category: ["plataformas"]
tags: ["A Hole New World","Plataformas","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/434160/header.jpg'
download: 'https://t.me/c/1118272073/12315'
---

# A Hole New World

{{< br >}}

¡La ciudad está siendo invadida por monstruos del Mundo Invertido! Encarna al Potion Master y acaba con el mal por ti mismo, sin tutoriales ni "modo fácil", ¡sólo con la ayuda de tu compañera, el hada Fäy y tus pociones!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/434160/extras/ahnw_attack_in_town_enlarge_600width_bicubicsharp.png" >}}
{{< br >}}

Salta y dispara como en los viejos tiempos de los arcades. Lucha contra enemigos en tu mundo y en el Mundo Invertido. ¡Derrota a gigantescos jefes finales y consigue sus poderes!

{{< br >}}

* ¡Ya sabes jugar! El reto está en el juego, no en complicados controles.
* ¡Modo Historia con cinco mundos distintos, Modo Historia +, Modo Jefes Finales, Modo Reto y distintos finales!
* ¡Más de 30 enemigos diferentes, 7 batallas con jefes finales y un montón de personajes secretos por buscar!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04 LTS (or later)
* Procesador: 2,16 GHz Intel Pentium Dual Core (or equivalent)
* Memoria: 4 GB de RAM
* Almacenamiento: 950 MB de espacio disponible

{{< br >}}
{{< br >}}
