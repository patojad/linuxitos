---
title: 'Super Smash Bros Crusade'
date: '2020-05-17 18:53:00'
type: 'otras'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Super Smash Bros Crusade","MUGEN","Accion","Otras"]
img: 'https://m.gjcdn.net/game-header/1400/52583-cmvgxft5-v4.jpg'
download: 'https://t.me/c/1118272073/12693'
---

# Super Smash Bros Crusade

{{< br >}}

Originalmente creado por Falcon8r, Phantom 7 y Dr.MarioX, Super Smash Bros. Crusade es un gran proyecto construido desde cero y iniciado por tres fanáticos acérrimos de Smash Bros. Nuestro objetivo es crear un juego de Smash Bros. lleno de personajes y escenarios de los videojuegos más memorables de todos los tiempos, así como crear una experiencia de Smash Bros. que combine las mejores características que todos los juegos oficiales de Smash Bros. tienen para ofrecer y más !

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* Pentium o procesador equivalente
* Tarjeta de sonido compatible con DirectX 8
* 128 MB de memoria o superior (solo se menciona en el Juego oficial Documentación del archivo de ayuda del creador)
* Resolución de pantalla de 960 × 720 o superior con colores de 16 o 32 bits

{{< br >}}
{{< br >}}
