---
title: 'Teenage Mutant Ninja Turtles: Rescue-Palooza!'
date: '2020-05-19 15:42:00'
type: 'otros'
idiomas: ["ingles"]
category: ["supervivencia","aventura","accion"]
tags: ["Teenage Mutant Ninja Turtles","Rescue-Palooza","Supervivencia","Accion","Aventura","Otros"]
img: 'https://m.gjcdn.net/game-header/1400/39658-hhqdijvp-v4.jpg'
download: 'https://t.me/c/1118272073/12812'
---

# Teenage Mutant Ninja Turtles: Rescue-Palooza!

{{< br >}}

¡Es hora de pizza! Teenage Mutant Ninja Turtles: Rescue-Palooza es un juego gratuito beat-em-up hecho por fanáticos basado en TMNT. ¡Una actualización y homenaje a los títulos clásicos de NES, con toneladas de personajes jugables por primera vez!

{{< br >}}
{{< br >}}

## Tortística vital:

{{< br >}}

* 60 personajes jugables
* 17 etapas
* 4 etapas de bonificación
* Cooperativa local para 4 jugadores
* Gráficos mejorados
* Clips de voz del programa de televisión
* Vehículos montables
* ¡Numerosas referencias a los dibujos originales y la línea de juguetes TMNT!

{{< br >}}
{{< br >}}
