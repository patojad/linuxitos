---
title: 'Resident Evil 4 HD Remake'
date: '2020-05-19 16:10:00'
type: 'otras'
idiomas: ["ingles"]
category: ["supervivencia"]
tags: ["Resident Evil","4","HD","Remake","Supervivencia","Otras"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/254700/header.jpg'
download: 'https://t.me/c/1118272073/12747'
---

# Resident Evil 4 HD Remake

{{< br >}}

en resident evil 4, al agente especial Leon S. Kennedy se le asigna la misión de rescatar a la hija del presidente de los EUA, que ha sido secuestrada. Tras llegar a una aldea rural europea, se enfrenta a nuevas amenazas que suponen retos totalmente diferentes de los clásicos enemigos zombis de pesados movimientos de las primeras entregas de esta serie. Leon lucha contra terroríficas criaturas nuevas infestadas con una nueva amenaza denominada «Las Plagas» y se enfrenta a una agresiva horda de enemigos que incluye aldeanos bajo control mental conectados a Los Iluminados, la misteriosa secta detrás del rapto.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/254700/ss_967184a307f57c18fbe140c23eaf137ba26b5282.600x338.jpg" >}}
{{< br >}}

## Características principales

{{< br >}}

* Por primera vez en alta definición a unos espectaculares y fluidos 60 fps.
* Se han renovado completamente los aspectos visuales del juego para darle a este legendario título una calidad gráfica sin precedentes.
* Se ha optimizado totalmente para pantalla panorámica, perfilado los textos y mejorado las texturas de los personajes, los escenarios y los objetos del juego.
* Compatible con la plataforma Steam: logros de Steam, Steam Cloud, cromos de Steam, marcadores mundiales, además de completamente compatible con mandos.
* La nueva versión para PC incluirá todas las características de las anteriores versiones, incluidos subtítulos en español, inglés, francés, italiano, alemán y japonés, así como extras publicados anteriormente, como el epílogo Separate Ways.

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* Procesador: Intel® CoreTM2 Duo 2,4 GHz o superior, AMD AthlonTM X2 2,8 GHz o superior
* Memoria: 2 GB de RAM
* Gráficos: NVIDIA® GeForce® 8800GTS o superior, ATI RadeonTM HD 4850 o superior
* DirectX: Versión 9.0c
* Almacenamiento: 15 GB de espacio disponible

{{< br >}}
{{< br >}}
