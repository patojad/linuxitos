---
title: 'FORCED'
date: '2020-05-14 20:31:00'
type: 'steam'
idiomas: ["español"]
category: ["accion"]
tags: ["forced","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/249990/header.jpg'
download: 'https://t.me/c/1118272073/12607'
---

# FORCED

{{< br >}}

Es un juego de acción arcade co-operativo co-operativo de uno a cuatro jugadores con elementos de RPG, puzle y estrategia. Habéis sido forzados como esclavos a luchar en el entrenamiento de gladiadores de fantasía más difícil de todos ellos, condenados a luchar como gladiadores y finalmente reclamar vuestra libertad. Os enfrentaréis a pruebas mortales y criaturas enormes, pero Balfus, vuestro Espíritu Mentor os guiará por vuestra aventura para ser reconocidos.

{{< br >}}
{{< br >}}

## Características

{{< br >}}

* Una campaña de Gladiadores con 25 Arenas y 5 Jefes Finales
* Cuatro Clases de Personajes Distintos, con 16 habilidades a desbloquear para cada uno
* Centrado en la destreza, todos los ataques enemigos pueden ser esquivados. ¡Es tu tarea averiguar cómo!
* El Espíritu Mentor, un miembro más de tu equipo, puede ser controlado entre todos los jugadores para obtener la ventaja sobre el enemigo
* Modo Supervivencia en la Arena (y más modos en camino)
* Sistema de Marcas en Combate. Aplica marcas a tus enemigos con ataques básicos y utilízalas después para obtener efectos devastadores con ataques especiales tuyos o de tus aliados.

{{< br >}}
{{< br >}}

## Historia

{{< br >}}

Forced comenzó como un proyecto en una Universidad en Copenhague. Creado desde sus inicios con la cooperación en mente, el juego tiene varias características innovadoras que llevan a los jugadores a colaborar entre ellos, dejando a los jugadores egoístas sangrando en el suelo de la arena.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

SO: Ubuntu 12.04
Procesador: 2.2 GHz Intel Core 2 Duo
Memoria: 2 GB de RAM
Gráficos: DirectX 9.0c-compatible, SM 3.0-compatible
Almacenamiento: 5 GB de espacio disponible

{{< br >}}
{{< br >}}
