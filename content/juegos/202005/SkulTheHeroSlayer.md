---
title: 'Skul: The Hero Slayer'
date: '2020-05-26 21:01:00'
type: 'steam'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Skul", "The Hero Slayer", "Accion", "Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1147560/header.jpg'
download: 'https://t.me/c/1118272073/12985'
---

# Skul: The Hero Slayer

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1147560/extras/DemonCastle_Destroyed_W620.gif" >}}
{{< br >}}

La raza humana que ataca el castillo del Rey Demonio no es nada nuevo y ha sucedido innumerables veces antes. Sin embargo, lo que hace que esta vez sea diferente es que los Aventureros decidieron unir fuerzas con el Ejército Imperial y el 'Héroe de Caerleon' para liderar un ataque total con la esperanza de eliminar a los Demonios de una vez por todas. Atacaron la fortaleza Demon con números abrumadores y lograron su destrucción total. Todos los demonios en el castillo fueron hechos prisioneros, excepto un esqueleto solitario llamado 'Skul'.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1147560/extras/Combat.gif" >}}

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* SO: Ubuntu
* Procesador: Intel i5+
* Memoria: 4 GB de RAM
* Gráficos: Nvidia 450 GTS / Radeon HD 5750 or better
* Almacenamiento: 500 MB de espacio disponible
* Notas adicionales: OpenGL 3.2+

{{< br >}}
{{< br >}}
