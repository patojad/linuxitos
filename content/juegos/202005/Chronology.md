---
title: 'Chronology'
date: '2020-05-18 15:06:00'
type: 'otras'
idiomas: ["español"]
category: ["plataformas"]
tags: ["Chronology","Plataformas","Otras"]
img: 'https://www.indiegalacdn.com/store-img_game/games/medium/269330.jpg'
download: 'https://t.me/c/1118272073/12711'
---

# Chronology

{{< br >}}

La cronología es una mezcla alucinante de rompecabezas, plataforma y aventura donde desafías el tiempo manipulando el pasado y el futuro para arreglar el presente. Juega como el viejo inventor y su compinche The Snail, y aprovecha sus habilidades especiales: viaja de un lado a otro en el tiempo, detiene el tiempo, manipula objetos y resuelve acertijos.

{{< br >}}
{{< br >}}

## Características clave

* El tiempo lo cambia todo: resuelve el rompecabezas viajando de un lado a otro o congelando el tiempo.

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* Procesador: 1.8 GHz
* Memoria: 2 GB de RAM
* Gráficos: Gráficos Intel HD 4 / 5a generación (4000/5000) o Gráficos HD AMD serie 7
* DirectX: Versión 9.0c
* Almacenamiento: 900 MB de espacio disponible.
* Tarjeta de sonido: compatible con OpenAL

{{< br >}}
{{< br >}}
