---
title: 'The King of Fighters Revision "E"'
date: '2020-05-23 16:42:00'
type: 'otras'
idiomas: ["ingles"]
category: ["plataformas","accion"]
tags: ["The King of Fighters", "E", "MURGEN", "Plataformas", "Accion", "Otras"]
img: 'https://static.wixstatic.com/media/655d8b_dd13dc3c0fce4047b7683732ad10261e~mv2.png/v1/fill/w_975,h_278,al_c,lg_1,q_85/kofe2.webp'
download: 'https://t.me/c/1118272073/12848'
---

# The King of Fighters Revision "E"

{{< br >}}

The King of Fighters E es un juego de lucha gratuito que puedes jugar en tu PC usando el motor de creación de juegos Mugen.

{{< br >}}
{{< img src="https://static.wixstatic.com/media/655d8b_1d7ae82fd2144dfb8364363bb759da88~mv2.png/v1/fill/w_596,h_446,al_c,q_85,usm_0.66_1.00_0.01/655d8b_1d7ae82fd2144dfb8364363bb759da88~mv2.webp" >}}
{{< br >}}

* 72 personajes KOF totalmente jugables que incluyen personajes nunca antes vistos en un juego KOF de la vieja escuela.
* 3 modos de lucha diferentes: uno contra uno, Tag Battle (incluido el juego cooperativo con un amigo) y Team Battle.
* Una historia clásica para el modo arcade para cada equipo.

{{< br >}}
{{< br >}}
