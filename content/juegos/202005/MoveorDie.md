---
title: 'Move or Die'
date: '2020-05-09 20:10:00'
type: 'steam'
idiomas: ["español"]
category: ["accion"]
tags: ["Move or Die","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/323850/header.jpg'
download: 'https://t.me/c/1118272073/12428'
---

# Move or Die

{{< br >}}

Es un juego multijugador de hasta 4 jugadores absurdamente vertiginosos. Podrás jugar con o sin conexión y las reglas cambiarán cada 20 segundos. La manera perfecta de arruinar amistades.

20 segundos, mecánicas de juego en constante cambio y muchos gritos. Estás son las palabras perfectas que describen una partida de Move or Die: el juego destroza-amistades para hasta 4 personas en el que las reglas cambian cada ronda. Quítale el mando a tus amigos mientras jugáis en el sofá, desafía a jugadores de todo el mundo en internet o practica sin conexión contra bots. Elige los modos de juego a los que quieres jugar de una lista interminable, creada por los desarrolladores y la comunidad; cada modo tiene unas mecánicas extravagantes y niveles diseñados para pasar horas divertidísimas en este juego arruina-amistades.

Juega en línea para ganar experiencia y subir de nivel; desbloquea personajes y modos; e intenta abrirte paso hasta la cima de las clasificaciones de los desafíos diarios en los que los jugadores compiten contra bots en partidas a muerte. Como su nombre indica, Move or Die obliga al jugador a moverse. De hecho, si no te mueves, tu personaje explotará; y con solo 20 segundos por ronda, Move or Die es un juego fácil de disfrutar, pero difícil de dejar.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/323850/extras/awesome_spa.png" >}}
{{< br >}}

**Partidas multijugador locales o en línea para hasta 4 jugadores y modos de práctica sin conexión.**
Juega como uno de los muchos personajes asombrosos en el sofá con tus colegas, en línea contra gente de todo el mundo o contra furiosos bots en el modo sin conexión.

{{< br >}}

**Montones de modos de juego**
Elige de entre una cantidad inmensa de modos de juego divertidísimos como Disparo aéreo, Motosierras y Carrera de misiles; cada uno con sus propias mecánicas de juego y niveles.

{{< br >}}

**Fácil de aprender, fácil de disfrutar**
Muévete o muere... literalmente: si no te mueves, tu personaje explota; y con solo 20 segundos por ronda, Move or Die es fácil de aprender y rápido como ningún otro juego.

{{< br >}}

**Sistema de niveles**
Consigue aspectos de lo más loco para tus personajes y desbloquea modos nuevos al aplastar a tus enemigos.

{{< br >}}

**Desafíos y misiones diarios**
Ábrete paso a través de la clasificación y gana más experiencia al completar desafíos y misiones que cambian todos los días.

{{< br >}}

**Gráficos sorprendentes y una banda sonora genial**
Haz que te sube la tensión con estos vibrantes gráficos en 2D, acompañados de una increíble banda sonora compuesta por Jacob Lincke. ¡Échale un vistazo en Bandcamp!

{{< br >}}

**Admite teclado y mando**
Juega con 4 mandos, un teclado, una plataforma de baile de DDR... o... la guitarra y batería de Guitar Hero: tú eliges. Pero bueno, nosotros recomendamos los mandos :D

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04 - 13.10
* Procesador: Si es un portátil sin GPU dedicada, la CPU deberá tener menos de 3 años.
* Memoria: 4 GB de RAM
* Gráficos: GPU dedicada recomendada
* Red: Conexión de banda ancha a Internet
* Almacenamiento: 400 MB de espacio disponible

{{< br >}}
{{< br >}}
