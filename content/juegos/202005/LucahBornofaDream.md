---
title: 'Lucah: Born of a Dream'
date: '2020-05-28 20:32:00'
type: 'steam'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Lucah", "Born of a Dream","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/896460/header.jpg'
download: 'https://t.me/c/1118272073/13056'
---

# Lucah: Born of a Dream

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/896460/extras/steamcombatgif02.JPG" >}}
{{< br >}}

¡Precaución! Este juego contiene patrones intermitentes que pueden ser perjudiciales para las personas con epilepsia fotosensible. Consulte las advertencias de activación en https://pastebin.com/3UD3Mt74

HACK, SLASH, ASCENDER. Lucah: Born of a Dream es una lucha desgarradora a través de nuestras peores pesadillas.

Has sido MARCADO, maldecido para que tus demonios internos cobren vida como pesadillas viciosas. Después de despertar a un dios falso, debes expiar. Atraviesa el reino infernal de los sueños, soporta el ataque de pesadilla y descubre la fuerza para aceptarte a ti mismo.

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* SO: Ubuntu 12.04+, SteamOS+
* Procesador: 1.8GHz or faster
* Memoria: 3 GB de RAM
* Gráficos: Any from the last 4 years
* Almacenamiento: 2 GB de espacio disponible
* Notas adicionales: Latest drivers

{{< br >}}
{{< br >}}
