---
title: 'Absolute Peace, Death!'
date: '2020-05-19 19:39:00'
type: 'steam'
idiomas: ["español"]
category: ["simulacion"]
tags: ["Absolute Peace, Death!","Simulacion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/588690/header.jpg'
download: 'https://t.me/c/1118272073/12776'
---

# Absolute Peace, Death!

{{< br >}}

**Peace, Death**! es un simulador de arcade con dificultades. En este juego, eres el Segador bajo las órdenes de tu jefe, Muerte, en Apocalipsis, Inc. El desafío: pasar por un período de prueba de siete semanas para obtener un trabajo permanente y promover los intereses de tu empleador.

**Características**. Tendrás que examinar cada característica de tu cliente para determinar su destino. Cada día tiene más características y el juego se vuelve más difícil. ¿Tu cliente tiene una pistola? Envíalo al Infierno a menos que cambie de parecer y tire su arma. ¿A tu cliente le encanta los sombreros? ¡No, no es tan fácil! Primero, quita el sombrero, y quizás veas cuernos. Ser un demonio, un asesino o un ángel también son características.

**Catástrofes**. Estos son eventos únicos, son inesperados y rápidos, dado que el Segador es presionado por el tiempo. Pero debes ubicar a tus clientes correctamente para completar una catástrofe, desbloquea nuevos clientes, ¡e incremente tu influencia con Muerte! Piratas desafortunados, epidemia de gripe de oso en Siberia, pelea de sopa gratis - estos son algunas de las catástrofes.

**Eventos**. Cada semana te enfrentarás a un nuevo evento. Mientras más días trabajes, más eventos habrán. Llamadas de aprendices, cajas fuertes con mercadería de contrabando, agentes en cubierto de Paradoja, secuestradores. Todos ellos plantean algunas dificultades pero te ayudarán a convertirte en el mejor en el negocio. ¡Incluso puedes cocinar una sopa!

**Días Temáticos**. Cada séptimo día es un día temático. Decidirás el destino de tus clientes con una banda sonora única correspondiente al espíritu del tema elegido. ¿Egipto? ¡Desde luego! ¿Madre Rusia? ¡Nostrovia! ¿Día pirata? ¡Yo-jo-jo!

**¿Qué más?** Tareas de Jinetes, frases graciosas de los clientes, muchísimas referencias e easter eggs, bonificaciones, sanciones, clientes muy especiales - ¡todo esto definirá tu futuro como el Segador que determinará el final del juego!

¡Buena suerte, Segador!

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* SO: Ubuntu 14.04
* Procesador: Intel Pentium 2.9 Ghz or equivalent
* Memoria: 1024 MB de RAM
* Gráficos: 512
* Almacenamiento: 100 MB de espacio disponible
* Notas adicionales: Mouse + Keyboard

{{< br >}}
{{< br >}}
