---
title: 'Super Mario Bros X2'
date: '2020-05-12 21:48:00'
type: 'otras'
idiomas: ["ingles"]
category: ["plataformas"]
tags: ["Super Mario Bros", "Mario","Plataformas","Otras"]
img: 'https://www.supermariobrosx.org/screen1.jpg'
download: 'https://t.me/c/1118272073/12520'
---

# Super Mario Bros X2

¡El mayor **homenaje** hecho por los fanáticos a Super Mario de todos los tiempos!

Super Mario Bros. X 1.3.0.1 es un fangame masivo de Mario que combina elementos de Super Mario 1, 2, 3 y World . Tiene muchos potenciadores, como **Ice Flower , Hammer Suit , Tanooki Suit , el zapato de Kuribo , The Billy Gun** y **Yoshi** . También puedes jugar con un amigo en el modo cooperativo para 2 jugadores , donde la pantalla se divide y combina a medida que los jugadores se separan y vuelven a unirse.

Este fangame es más notable por su extenso editor de niveles que te permite crear casi cualquier tipo de nivel que puedas imaginar. ¡El editor en tiempo real te permite editar el nivel mientras lo juegas! También puede crear su propio episodio usando el mapa mundial SMB3 o SMW, o puede crear un nivel de centro de estilo Mario 64 y hacer que los jugadores recojan estrellas para avanzar.

¿Quieres experimentar el futuro de Super Mario Bros. X? Puedes probar la última versión beta de Super Mario Bros. X2 aquí:

{{< br >}}
{{< img src="https://www.supermariobrosx.org/screen16.jpg" >}}
{{< br >}}

## Característica

{{< br >}}

* Cinco personajes jugables que incluyen: melocotón, sapo y enlace
* Más de 60 niveles ingeniosamente diseñados en los dos episodios predeterminados
* Editor de nivel en tiempo real fácil de usar con miles de objetos para elegir
* Potenciadores clásicos y nuevos como Yoshi y The Billy Gun
* Acción cooperativa para dos jugadores con una cámara inteligente que se divide y combina
* Un modo de batalla intenso que te hará volver por más

{{< br >}}
{{< br >}}
