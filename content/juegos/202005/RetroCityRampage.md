---
title: 'Retro City Rampage™ DX'
date: '2020-05-15 16:47:00'
type: 'steam'
idiomas: ["español"]
category: ["accion"]
tags: ["Retro City Rampage™ DX","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/249990/header.jpg'
download: 'https://t.me/c/1118272073/12629'
---

# Retro City Rampage™ DX

{{< br >}}

¡Regresa a la década de los 80 con esta nostálgica experiencia en 8 bits, que incluye un mundo abierto y robo de coches!
Juega a más de 60 misiones y 40 desafíos arcade. ¡"Toma prestados" vehículos y evade la ley mientras juegas a los bolos con tus conciudadanos! Consigue monedas saltando sobre los peatones, o úsalos como blancos de tiro con ¡más de veinticinco armas y POTENCIADORES! ¡GUAU!

{{< br >}}
{{< br >}}

## Características Principales

{{< br >}}

* Un mundo abierto e interactivo, lleno de tiendas, recreativas y casinos.
* Más de sesenta misiones principales - De todo tipo: conducción, disparos, sigilo, ritmo, natación, investigación, ¡y mucho más!
* Más de cuarenta desafíos arcade - Acción de juego instantánea, con tabla de puntuaciones y repeticiones online
* Invitados especiales de otros juegos - Super Meat Boy, BIT.TRIP: Runner, Epic Meal Time, Minecraft y muchos más
* Más de cincuenta vehículos que conducir o equipar - ¿Qué tal un monopatín con lanzacohetes o un carrito de la compra con nitro?
* Más de veinticinco armas y potenciadores - Un brazo biónico, una pistola de luz, un bumerán e incluso un rayo de protones para cazar fantasmas
* ¡Personajes jugables y cameos! - Juega como personajes de otros juegos, desarrolladores legendarios de juegos, personalidades televisivas o famosos
* Personaliza tu personaje con más de doscientos estilos
* Más de dos horas y media de música (trece emisoras de radio) - Con una BSO aclamada por la crítica, nominada a los IGF, compuesta por virt (Contra 4, Double Dragon: Neon) Freaky DNA (NBA Jam) y Norrin Radd. Disponible para su compra en formato digital, CD y vinilo.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Processor:1GHz processor
* Memory:256 MB RAM
* Hard Drive:30 MB HD space
* Additional:Should run on any PC released within the past 5 years. Older graphics cards (such as those in netbooks).

{{< br >}}
{{< br >}}
