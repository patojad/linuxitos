---
title: 'Distraint: Pocket Pixel Horror Deluxe Edition'
date: '2020-05-27 16:51:00'
type: 'gog'
idiomas: ["español"]
category: ["aventura"]
tags: ["Distraint", "Pocket Pixel Horror", "Deluxe Edition", "Aventura", "GOG"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/395170/header.jpg'
download: 'https://t.me/c/1118272073/13024'
---

# Distraint: Pocket Pixel Horror Deluxe Edition

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/395170/extras/sale_gif.png" >}}
{{< br >}}

DISTRAINT Es una aventura de horror psicológico 2D para PC. Para asegurarse una posición de socio en la famosa empresa para la que trabaja, Price embarga la propiedad de una anciana. En ese momento encuentra el precio de su humanidad.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/395170/extras/s_game_spa.png" >}}
{{< br >}}

**DISTRAINT** Es una aventura de horror psicológico 2D para PC.

Te metes en los zapatos de un joven ambicioso llamado Price.

Para asegurarse una posición de socio en la famosa empresa para la que trabaja, Price embarga la propiedad de una anciana.
En ese momento encuentra el precio de su humanidad.

Esta es su historia y la de sus remordimientos...

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/395170/extras/compressed_01.png" >}}

*"¡Aún no te hemos visto BAILAR!"*

{{< br >}}

---

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/395170/extras/s_features_spa.png" >}}
{{< br >}}

DISTRAINT es un juego de estilo novela de horror. Es oscuro y siniestro pero también tiene algo de humor negro.

La historia progresa rápido lo que permite tener una variedad de escenarios.
¡Tu viaje durará alrededor de dos horas!

El modo de juego es simple pero efectivo: Te mueves a la derecha e izquierda y debes resolver puzles para progresar a través de la historia.

2D de desplazamiento lateral con gráficos únicos dibujados a mano
Diseño de sonido y música ambiental
Interfaz minimalista, para tu atención se mantenga en la experiencia.
Adéntrate en una historia única llena de intrigantes giros.
El esfuerzo de un hombre - Sin girar las imágenes o trucos baratos, ¡Sólo trabajo duro!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/395170/extras/compressed_02.png" >}}

*"No hay señal... ¿Qué estará viendo esta gente?"*

{{< br >}}

---

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/395170/extras/s_deluxe_eng.png" >}}
{{< br >}}

DISTRAINT ha ido bastante bien gracias a la fantástica comunidad
Deluxe Edition es mi forma de agradecéroslo. Podéis echar un vistazo a las características nuevas:

Color dinámico - Adiós gris ¡Hola color!
No más linterna - se incrementa la luz ambiental
Animación mejorada, gráficos y efectos de luz
Se ha refinado el audio para una mejor ambientación
Interfaz de usuario mejorada

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/395170/extras/compressed_03.png" >}}

*"A estas personas las tratan como ganado."*

{{< br >}}

---

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* Procesador: Dual Core 2.0 GHz
* Memoria: 2 GB de RAM
* Gráficos: 512 MB card capable of shader 3.0
* DirectX: Versión 9.0c
* Almacenamiento: 175 MB de espacio disponible

{{< br >}}
{{< br >}}
