---
title: 'Elite Warriors: Vietnam'
date: '2020-05-24 15:36:00'
type: 'otras'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Elite Warriors", "Vietnam", "Accion", "Otras"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/514140/header.jpg'
download: 'https://t.me/c/1118272073/12899'
---

# Elite Warriors: Vietnam

{{< br >}}

Las operaciones especiales más altamente clasificadas de la Guerra de Vietnam fueron realizadas por el Grupo de Estudios y Observaciones. Consistiendo principalmente en las "Boinas Verdes" de las Fuerzas Especiales del Ejército de los EE. UU., Los guerreros SOG lucharon en misiones de alto secreto detrás de las líneas enemigas.

Infiltrarse profundamente en Laos con sus compañeros de equipo de Green Beret y Montgnard, perseguidos por rastreadores y sabuesos, superados en número a veces 100 a 1. Rescata a los pilotos derribados, embosca a los convoyes, toma prisioneros enemigos, descubre objetivos para ataques aéreos y ataques aéreos directos. Vea por qué los veteranos de SOG fundaron Delta Force y, en el trabajo de operaciones secretas, siguen siendo leyendas.

{{< br >}}
{{< br >}}

## Caracteristicas:

{{< br >}}

* Participa en el mundo secreto de la guerra no convencional "Black Ops".
* Participar en operaciones altamente clasificadas en todo el sudeste asiático.
* Ocho campañas realistas que tienen lugar en múltiples fases.
* Utiliza técnicas de patrulla de combate aprendidas en Vietnam y que todavía son válidas hoy.
* Team building: entrena y equipa a tu equipo y luego llévalo a misiones secretas.

{{< br >}}
{{< br >}}
