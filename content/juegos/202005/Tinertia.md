---
title: 'Tinertia'
date: '2020-05-10 15:36:00'
type: 'steam'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Tinertia","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/311930/header.jpg'
download: 'https://t.me/c/1118272073/12446'
---

# Tinertia

{{< br >}}

Intenta escapar de un antiguo planeta utilizando tu lanzacohetes de confianza y unos reflejos instantáneos.
Jugarás como Weldon, un robot de corta estatura pero de gran corazón. Weldon se ve capturado por un ente malvado llamado ARC. Abandonado en un mundo hostil y escalofriante, Weldon deberá planear una heroica huida para sobrevivir.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/311930/extras/Header.png" >}}
{{< br >}}

**Juego de plataformas con control doble:** Domina la técnica de saltos con cohete para navegar intrincados entornos en 3D. ¡No hay límite de velocidad ni botón de salto!

{{< br >}}

**Siete entornos:** Viaja por mundos animados a lo largo de más de 65 niveles, con fondos épicos y mecánicas de nivel únicas.

{{< br >}}

**Robojefes:** Enfréntate a siete jefes épicos para poner a prueba tu habilidad con los saltos con cohete.

{{< br >}}

**Marcadores:** Tinertia guarda tus mejores puntuaciones en marcadores globales. Compite con tus amigos o enfréntate al mejor jugador del mundo en cualquier modo.

{{< br >}}

**Sistema de repetición:** ¡Revive momentos épicos desde distintos ángulos y en cámara lenta!

{{< br >}}

**Modo ataque a los jefes:** Enfréntate a todos los jefes de la huida de Weldon con una sola vida para ganar un lugar en el marcador.

{{< br >}}

**Modo carrera:** Corre a lo largo de un mundo desde el inicio hasta la batalla del jefe para competir en el marcador.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: 1.7 GHz Intel i3
* Memoria: 2 GB de RAM
* Gráficos: Dedicated Graphics Card
* Almacenamiento: 2 GB de espacio disponible
* Notas adicionales: Controller or 2 Button Mouse recommended.

{{< br >}}
{{< br >}}
