---
title: 'Mini DOOM 2'
date: '2020-05-29 17:31:00'
type: 'otras'
idiomas: ["ingles"]
category: ["plataformas"]
tags: ["Mini DOOM 2", "Plataformas", "Otras"]
img: 'https://m.gjcdn.net/game-header/1400/233498-crop0_87_1702_550-ug9grs9d-v4.jpg'
download: 'https://t.me/c/1118272073/13076'
---

# Mini DOOM 2

{{< br >}}

¡El juego tributo que convierte a DOOM en un juego de plataformas de acción! ¡Ahora más grande, más sangriento y mejor en todos los sentidos!

{{< br >}}
{{< img src="https://m.gjcdn.net/game-screenshot/300/1250030-wdbhby9t-v4.jpg" >}}
{{< br >}}
{{< br >}}
