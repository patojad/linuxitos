---
title: 'Tales From Off-Peak City Vol. 1'
date: '2020-05-25 20:34:00'
type: 'steam'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["Tales From Off-Peak City", "Aventura", "Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1129920/header.jpg'
download: 'https://t.me/c/1118272073/12937'
---

# Tales From Off-Peak City Vol. 1

{{< br >}}

Es un brumoso domingo por la mañana en la esquina de la avenida July y la calle Yam. Caetano Grosso, un ex saxofonista y pizzero de toda la vida, acaba de calentar su horno de pizza. Los pedidos están llegando y él necesita a alguien que lo ayude con las entregas. Sientes una oportunidad, pero sin que él lo sepa, lo que realmente buscas es su viejo saxofón, y lo conseguirás de una forma u otra. Su búsqueda desencadenará una cadena de eventos en todo el vecindario, llevándolo a los mundos ocultos y la vida privada de las personas que viven allí.

Tales From Off-Peak City es una serie de juegos de aventuras en primera persona, todo en una esquina de la calle. Lleno de personajes memorables y los espacios que habitan, el juego ofrece un mundo abierto lleno de secretos y agendas ocultas. Sobre la base de las bases de juego de Off-Peak y The Norwood Suite, los jugadores explorarán las calles y sus alrededores, harán pizzas, tomarán fotos, presionarán botones, abrirán cajones que no deben, y se perderán dentro de la partitura musical atmosférica que se integra directamente en el mundo del juego.

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* SO: Ubuntu, Debian, SteamOS
* Procesador: Intel i5, 3.0Ghz
* Memoria: 8 GB de RAM
* Gráficos: Nvidia GeForce 800 series / Radeon Pro 560, 2GB
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
