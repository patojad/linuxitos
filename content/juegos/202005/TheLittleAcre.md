---
title: 'The Little Acre'
date: '2020-05-01 17:38:00'
type: 'gog'
idiomas: ["español"]
category: ["aventura"]
tags: ["The Little Acre","Aventura","GOG"]
img: 'https://images.gog-statics.com/0c1732219c4ed153d1040d26e810f476f20864494c380abb4495178a2cf73e9c.jpg'
download: 'https://t.me/c/1118272073/12332'
---

# The Little Acre

{{< br >}}

Sigue la historia de Aidan y su hija, Lily, ambientada en la Irlanda de 1950. Después de descubrir pistas sobre el paradero de su padre desaparecido, Aidan comienza a investigar hasta que, sin darse cuenta, se encuentra transportado a un mundo nuevo y extraño. Siempre héroe, Lily se lanza tras él, encontrando sus propios peligros en el camino. Con una actuación completa de voz y animación dibujada a mano, The Little Acre es un juego de aventuras memorable y cuidadosamente diseñado.

{{< br >}}

* The Little Acre es desarrollado por Pewter Games junto con el productor ejecutivo Charles Cecil (Broken Sword, Beneath a Steel Sky).
* Dos personajes jugables
* Animación tradicional dibujada a mano.
* Transiciones de perspectiva únicas
* Hermosa partitura original
* La voz actuó completamente

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema: Ubuntu 14.04, 16.04 o más reciente
* Procesador: Intel Core i5-4570 @ 3.20 GHz
* Memoria: 2048 MB de RAM
* Almacenamiento: 3500 MB de espacio disponible
* Otro: Solo binario de 64 bits

{{< br >}}
{{< br >}}
