---
title: 'Into the Breach'
date: '2020-05-30 12:57:00'
type: 'steam'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Into the Breach","Estrategia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/590380/header.jpg'
download: 'https://t.me/c/1118272073/13102'
---

# Into the Breach

{{< br >}}

Controla a poderosos mech del futuro para acabar con una amenaza alienígena. En este juego de estrategia por turnos, cada intento de salvar el mundo presenta un nuevo reto generado aleatoriamente.

Los restos de la civilización humana sufren el asedio de unas criaturas gigantescas que viven en las profundidades de la tierra. Ponte a los mandos de poderosos mechs del futuro para contener esta amenaza alienígena. En este juego de estrategia por turnos de los creadores de FTL, cada intento de salvar el mundo presenta un nuevo reto generado aleatoriamente.

{{< br >}}
{{< br >}}

## Características:

{{< br >}}

* Defiende las ciudades: Los edificios civiles suministran energía a tus mechs. ¡Defiéndelos de los vek y ten cuidado al disparar!
* Perfecciona tu estrategia: El enemigo anuncia todos sus ataques en un sistema minimalista de combate por turnos. Analiza los ataques de tus adversarios y busca la forma perfecta de contrarrestarlos en cada turno.
* Crea el mech definitivo: Encuentra nuevas y poderosas armas, así como pilotos únicos, mientras te enfrentas a la plaga de vek en varias islas nación corporativas.
* Siempre hay otra oportunidad: El fracaso no es una opción. ¡Si te derrotan, envía ayuda al pasado para salvar otra línea temporal!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubunut 14.04 or more recent
* Procesador: 1.7+ GHz or better
* Memoria: 1 GB de RAM
* Gráficos: Must support OpenGL 2.1 or higher. Intel HD 3000 or better.
* Almacenamiento: 400 MB de espacio disponible

{{< br >}}
{{< br >}}
