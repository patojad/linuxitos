---
title: 'Mortal Kombat: Defender of the Earth (M.U.G.E.N.)'
date: '2020-05-12 22:09:00'
type: 'otras'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Mortal Kombat", "Defender of the Earth", "MUGEN", "Combat Edition","Accion","Otras"]
img: 'https://4.bp.blogspot.com/-MS3FHzJObJ4/Wt0Jd7gJ6KI/AAAAAAAAA7w/vyvN1Ar54mgGie1hajTF2xI6E9vEFJsAQCLcBGAs/s1600/Caratula%2B-%2BBlog.png'
download: 'https://t.me/c/1118272073/12531'
---

# Mortal Kombat: Defender of the Earth (M.U.G.E.N.)

{{< br >}}
{{< img src="https://1.bp.blogspot.com/-Tx8MxM5UiJg/Wt0UO_lk5TI/AAAAAAAAA9I/v2oQvibZ7hI0wjms7hmxOXpOS4WMSr8RACLcBGAs/s1600/Options.png" >}}
{{< br >}}

MK Defenders of the Earth v3.3 es un juego de fans sin ánimo de lucro basado en los juegos clásicos de la saga MK. Desarrollado en Mugen, este juego utiliza la temporada 2.9 de Borg117 como base y material del MKP 4.1 del equipo MKP. Los personajes fueron creados por la comunidad mugen de MK que recibió varias modificaciones para ser montadas en este juego (esto incluye cambios escritos en el archivo "actualización de personajes - part2.5.txt" por borg117). Las etapas ya creadas por la comunidad de mugen se modificaron para ser montadas en el juego, también se crearon escenarios originales usando fan-arts, animaciones de pantalla verde gratuitas y sprites de otros escenarios. La banda sonora es un conjunto de música libre de derechos de autor con sus licencias respetadas o permisos otorgados. Hitsounds fue tomado de MKX.

{{< br >}}
{{< img src="https://4.bp.blogspot.com/-IXkw2AtPTEw/Wt0S3_cZuCI/AAAAAAAAA8Y/AjQKQitKZggzaYD2ZfqySzjBN4Y3ALSrQCLcBGAs/s1600/Gameplay%2B5.png" >}}
{{< br >}}
{{< br >}}
