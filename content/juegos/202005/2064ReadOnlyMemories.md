---
title: '2064: Read Only Memories'
date: '2020-05-22 13:26:00'
type: 'steam'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["2064","Read Only Memories","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/330820/header.jpg'
download: 'https://t.me/c/1118272073/12831'
---

# 2064: Read Only Memories

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/330820/extras/2064Logo.png" >}}
{{< br >}}

**2064: Read Only Memories** fusiona los juegos de aventuras de la vieja escuela con la narración moderna para explorar algunos de los desafíos sociales que enfrentaremos en el futuro cercano. Explore la colorida metrópolis futura de Neo-San Francisco, conozca a un enorme elenco de personajes fascinantes y resuelva un misterio de gran alcance. Piensa cuidadosamente en tus elecciones, porque nunca sabes cómo afectarán esta narrativa dinámica y ramificada más adelante.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/330820/extras/GGP.png" >}}
{{< br >}}

## El mundo esta cambiando.

{{< br >}}

La alteración genética de los humanos es ahora común, y muchos eligen alterar radicalmente sus cuerpos para expresar su individualidad. Las inteligencias virtuales llamadas ROM (Gerentes de Relaciones y Organización), creadas como simples asistentes digitales, están ejerciendo un mayor control sobre la vida de las personas. Algunos están aterrorizados por este cambio rápido, y están dispuestos a hacer todo lo posible para evitarlo.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/330820/extras/Stardust.png" >}}
{{< br >}}

Una ROM perdida llamada Turing irrumpe en el departamento de un periodista en apuros para pedir ayuda. Afirman ser la primera máquina inteligente, no una simulación de la vida, sino un ser artificial genuinamente consciente de sí mismo, y el genio técnico que las creó ha desaparecido. ¿Puedes desentrañar la conspiración impactante que amenaza con sacudir a toda una sociedad? ¿O la oscuridad que se esconde detrás de las brillantes luces de neón de Neo-SF te consumirá?

{{< br >}}
{{< br >}}

## Requisitos del sistema

{{< br >}}

* SO: Ubuntu 12.04
* Procesador: 2.0 GHz with SSE2 instruction support
* Memoria: 1 GB de RAM
* Almacenamiento: 1000 MB de espacio disponible
* Tarjeta de sonido: blip

{{< br >}}
{{< br >}}
