---
title: 'Krautscape'
date: '2020-05-01 18:09:00'
type: 'steam'
idiomas: ["ingles"]
category: ["deportes","carreras"]
tags: ["Krautscape","Deportes","Carreras","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/268360/header.jpg'
download: 'https://t.me/c/1118272073/12349'
---

# Krautscape

{{< br >}}

Es un juego de carreras lleno de acción con autos voladores con forma de pájaro y pistas de carreras construidas de manera procesal. El jugador líder construye la pista de carreras, mientras que los jugadores que persiguen pueden tomar atajos al volar fuera de la pista para superar los obstáculos. Pero la pista es necesaria para anotar y obtener velocidad: los vehículos no tienen propulsores para acelerar mientras vuelan.

Krautscape presenta tres modos de juego multijugador únicos que están diseñados para admitir un juego no lineal y un movimiento inteligente en lugar de obligarte a conducir perfectamente. Tres modos de juego para un jugador te presentan la mecánica básica de conducir, volar y construir pistas.

{{< br >}}
{{< br >}}

## Características clave

{{< br >}}

* Coches voladores: deslízate por el aire como un pájaro
* Construye una pista de carreras mientras conduces: cada carrera crea una nueva pista
* 3 modos de juego multijugador con objetivos y jugabilidad completamente diferentes: Snake, Ping Pong y Collector
* 3 modos de juego para un jugador, enfocados en conducir, volar y construir pistas
* Los modos de juego multijugador se pueden jugar en línea, en LAN o en modo de pantalla dividida (hasta 8 jugadores)
* Modo de juego gratuito sandbox Banda sonora adaptativa original

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04+
* Procesador: 2.0 GHz Core 2 Duo
* Memoria: 2 GB de RAM
* Gráficos: 512 MB Graphics Card comparable to Radeon X1600
* Red: Conexión de banda ancha a Internet
* Almacenamiento: 500 MB de espacio disponible
* Tarjeta de sonido: pulseaudio required
* Notas adicionales: Supported Controllers: XBox 360 Pad, PS3 Controller, PS4 Controller

{{< br >}}
{{< br >}}
