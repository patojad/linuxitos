---
title: 'Beautiful Desolation'
date: '2020-05-09 19:44:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura"]
tags: ["Beautiful Desolation","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/912570/header.jpg'
download: 'https://t.me/c/1118272073/12384'
---

# Beautiful Desolation

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/BD_Logo_gif6.gif" >}}
{{< br >}}

es un juego de aventuras isométrico en 2D ambientado en un futuro lejano. Explora un panorama posapocalíptico, resuelve puzles, conoce a nuevos amigos y haz enemigos poderosos, media en conflictos y lucha por tu vida conforme descubres los secretos del mundo que te rodea.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/world_A.gif" >}}
{{< br >}}

Mark, un hombre fuera de su época, busca a su hermano perdido, Don, en una época futurista lejana gobernada por tecnologías altamente avanzadas que son veneradas y odiadas por igual.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/Conv_A.gif" >}}
{{< br >}}

Tus alrededores contienen recuerdos de un pasado desolado y destellos de un futuro oscuro todavía por escribir según tus acciones. Prepárate para enfrentarte a muchas opciones complicadas que darán forma a esta tierra muchos después de que completes tu viaje.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/Inv.gif" >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/Map.gif" >}}
{{< br >}}

Los habitantes de este mundo te ayudarán y te pondrán trabas conforme realizas descubrimientos y avanzas por el espectacular paisaje inspirado en África. Negocia tu camino con líderes locales, curanderos y guerreros, o hállate envuelto en una batalla contra un ejército de robots microscópicos, escorpiones gigantes y robots equipados con misiles.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/ultra_wide_screen-2.png" >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/Tik.gif" >}}
{{< br >}}

Desde pueblos prósperos hasta ciudades derruidas, bosques petrificados y suelos marinos completamente secos, este extraño mundo nuevo contiene muchos terrenos por descubrir, renderizados de forma preciosa usando un arte isométrico en 2D.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/Conv_B.gif" >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/ultra_wide_screen-4.png" >}}
{{< br >}}

La música está compuesta magistralmente por el compositor Mick Gordon, conocido por su trabajo en Wolfenstein®, DOOM®, Prey®, Killer Instinct® y Need for Speed®.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/World_B.gif" >}}
{{< br >}}

**ENCUENTRA A TU HERMANO Y VUELVE A CASA...**

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/912570/extras/ultra_wide_screen-1.png" >}}

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Requiere un procesador y un sistema operativo de 64 bits
* SO: Ubuntu 18.04 LTS 64-bit | OpenSuSE Tumbleweed | Linux Mint | Arch Linux
* Procesador: Intel Core i3 @ 2.50 GHz now working on AMD!
* Memoria: 4 GB de RAM
* Gráficos: VIDIA GeForce 9600 GT | Radeon Supported | Intel Onboard
* Almacenamiento: 14 GB de espacio disponible
* Tarjeta de sonido: Any
* Notas adicionales: OpenSuSE Tumbleweed is working. Linux Mint is working. Arch Linux is working. Check forums for more info.

{{< br >}}
{{< br >}}
