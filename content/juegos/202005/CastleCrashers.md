---
title: 'Castle Crashers'
date: '2020-05-15 19:00:00'
type: 'otras'
idiomas: ["español"]
category: ["accion"]
tags: ["Castle Crashers","Accion","Otras"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/204360/header.jpg'
download: 'https://t.me/c/1118272073/12647'
---

# Castle Crashers

{{< br >}}

¡Ábrete paso golpeando, acuchillando y machacando hasta la victoria en esta aventura arcade en 2D de The Behemoth! Castle Crashers Incluye personajes dibujados a mano e ilustraciones visuales de alta resolución jamás vistas hasta ahora. ¡Pueden jugar cuatro amigos en casa u online para salvar a la princesa, defiende el reino y destruir varios castillos!

{{< br >}}
{{< br >}}

## Características Principales

{{< br >}}

* ¡Más de 25 personajes y de 40 armas para desbloquear!
* Sistema intuitivo de combo y magia: desbloquea un montón de ataques nuevos a medida que progresas en el juego
* Puedes mejorar al personaje y ajustar la Fuerza, la Magia, la Defensa y la Agilidad
* Te acompañan los adorables animalitos. Cada uno aporta distintas habilidades que te ayudan en tu viaje
* Modo arena: ¡lucha contra otros jugadores en combates abiertos o de equipo!
* Modo insane: pon a prueba tu habilidad en el desafío de campaña definitivo
* Necromantic Booster Pack (incluye los personajes Necromancer y Cult Minion)
* King Booster Pack (incluye The King y Open-Faced Gray Knight)
* Mini juego All You Can Quaff
* También incluye: personaje jugable Alien Hominid

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Processor:Intel Core 2 Duo 2GHz+ or better
* Memory:1 GB RAM
* Graphics:256 MB video card
* DirectX®:9.0c
* Hard Drive:255 MB HD space

{{< br >}}
{{< br >}}
