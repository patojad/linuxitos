---
title: 'Shadowplay: The Forsaken Island'
date: '2020-03-22 15:41:00'
type: 'otros'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["Shadowplay","Aventura","Otros"]
img: 'https://www.yrjie.com/images/z-pc-29681-en_screen1.jpg'
download: 'https://t.me/c/1118272073/11731'
---

# Shadowplay: The Forsaken Island

{{< br >}}

Hace ya tres años su hermano desapareció mientras trabajaba en un proyecto de alto secreto. Todos en la isla se perdió... o lo pensaste. Cuando se activa una baliza de rescate, incumbe a usted para buscar sobrevivientes. Rápidamente, las cosas toman un giro oscuro cuando extrañas criaturas comienzan a atacar! ¿Puede descubrir lo que realmente sucedió en el centro de control remoto, o se convertirá en próximo residente de la isla? ¡Descubre en este escalofriante juego de aventuras de rompecabezas de objetos escondidos!

{{< br >}}
{{< br >}}
