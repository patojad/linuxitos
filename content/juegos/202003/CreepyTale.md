---
title: 'Creepy Tale'
date: '2020-03-15 22:21:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura"]
tags: ["Creepy Tale","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1224020/header.jpg'
download: 'https://t.me/c/1118272073/11646'
---

# Creepy Tale

Descubre la oscura historia que sucedió en el una vez tranquilo y pacifico bosque, convirtiéndolo en un oscuro lugar lleno de malvadas criaturas . ¿Quizás eres tú quien podrá resolver todos los secretos del bosque y salvar a las criaturas pacíficas?

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1224020/extras/ArtWitch600.png" >}}
{{< br >}}

Mientras caminas con tu hermano, todo se convierte en horror y te arrastra a ti el héroe a una serie de terribles eventos. Tu hermano ha sido raptado, y tu quedas frente a un siniestro bosque lleno de peligros y extrañas criaturas. Resuelve los rompecabezas que encuentres en el camino, no te dejes atrapar y salva a tu hermano!
Descubre la oscura historia que sucedió en el una vez tranquilo y pacifico bosque, convirtiéndolo en un oscuro lugar lleno de malvadas criaturas . ¿Quizás eres tú quien podrá resolver todos los secretos del bosque y salvar a las criaturas pacíficas?

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1224020/extras/FeaturesSpa.png" >}}
{{< br >}}

1. Un tenebroso y atmosférico rompecabezas.
2. Elementos de una aventura clásica: encuentra objetos y úsalos en una gran variedad de situaciones.
3. Pero esto no es solo una aventura, tendrás que esconderte, huir, dar grandes saltos, e incluso...aprender a tocar la flauta.
4. Una banda sonora verdaderamente espeluznante y un hermoso arte 2D te llevaran a una oscura historia y te dará unas cuantas horas inolvidables!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 16.04+, SteamOS+
* Procesador: 1.2 GHz
* Memoria: 2 GB de RAM
* Almacenamiento: 2 GB de espacio disponible

{{< br >}}
{{< br >}}
