---
title: 'Stoneshard'
date: '2020-03-01 17:55:00'
type: 'steam'
idiomas: ["ingles"]
category: ["rpg"]
tags: ["Stoneshard","RPG","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/625960/header.jpg'
download: 'https://t.me/c/1118272073/11482'
---

# Stoneshard

{{< br >}}

Es un RPG por turnos en el que exploras un mundo medieval amplio y generado por procedimientos. Cuida tus heridas y permanece cuerdo para manejar tu propia caravana y sobrevivir. ¡Embárcate en una aventura épica y decide el destino de un reino en esta sombría aventura!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/625960/extras/steam_roadmap2020.png" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 18.04.2
* Procesador: Intel Core 2 Duo E6320 or equivalent
* Memoria: 2 GB de RAM
* Gráficos: GeForce 7600 512 Mb or equivalent
* Almacenamiento: 500 MB de espacio disponible

{{< br >}}
{{< br >}}
