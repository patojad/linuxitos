---
title: 'The Longing'
date: '2020-03-13 20:42:00'
type: 'gog'
idiomas: ["español"]
category: ["aventura"]
tags: ["The Longing","Aventura","GOG"]
img: 'https://images.gog-statics.com/53e9af3eee7d838bb57bf7c1de3a6212e7a8ee1f448da878a581ffedcda3f91c.jpg'
download: 'https://t.me/c/1118272073/11587'
---

# The Longing

{{< br >}}

Juega como una Sombra solitaria, el último sirviente de un rey que una vez gobernó un reino subterráneo. Los poderes del rey se han desvanecido y se queda dormido durante 400 días para recuperar su poder. Es tu deber quedarte en el palacio de tierra hasta que despierte.

Tan pronto como comiences, el juego inevitablemente contará los 400 días, incluso cuando dejes de jugar y salgas del juego. Ahora depende de usted decidir qué hacer con su existencia solitaria debajo del suelo. No te estreses, tienes mucho tiempo.

{{< br >}}
{{< br >}}

## Elige tu estilo de juego:

{{< br >}}

Comienza el juego y simplemente regresa después de 400 días para ver cómo termina. En realidad no tienes que jugar el juego en absoluto. Pero la Sombra estará aún más sola sin ti.

{{< br >}}
{{< img src="https://items.gog.com/the_longing/longing_king.gif" >}}
{{< br >}}

O explore las cuevas y recolecte artículos para su cómoda sala de estar subterránea. Simplemente envíe la Sombra a dar un paseo: la velocidad de caminata es lenta, pero afortunadamente no hay necesidad de apurarse.

{{< br >}}
{{< img src="https://items.gog.com/the_longing/longing_labyrinth.gif" >}}
{{< br >}}

Lee toneladas de literatura clásica desde Nietzsche hasta Moby Dick directamente en el juego, o al menos haz que la sombra los lea. Después de todo, el tiempo pasa más rápido si aprende a mantener su mente ocupada. (La pequeña Sombra en el juego encuentra SOLO libros en inglés por ahora).

{{< br >}}
{{< img src="https://items.gog.com/the_longing/longing_reading.gif" >}}
{{< br >}}

Ignora las órdenes del rey y avanza a las regiones exteriores de la cueva. Será un largo y peligroso viaje a la oscuridad ...

{{< br >}}
{{< img src="https://items.gog.com/the_longing/longing_darkness.gif" >}}
{{< br >}}

## Caracteristicas

{{< br >}}

* Lenta exploración de una vasta cueva dibujada a mano.
* Banda sonora Atmospheric Dungeon Synth.
* Varios finales.
* Muchos secretos bien escondidos.
* Rompecabezas basados ​​en el tiempo.
* Un protagonista solitario pero lindo.

{{< br >}}
{{< br >}}
