---
title: 'Dead End Road'
date: '2020-03-01 15:37:00'
type: 'steam'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Dead End Road","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/448580/header.jpg'
download: 'https://t.me/c/1118272073/11448'
---

# Dead End Road

{{< br >}}

Es un juego de conducción atmosférico, con una estética de baja fidelidad y elementos generados por procedimientos. Completa tu viaje para buscar la ayuda de la vieja bruja en Dead End Road; Llegue antes del amanecer y tal vez, solo tal vez, pueda escapar de esta pesadilla.

{{< br >}}

Te habían advertido que no te metieras en lo oculto, que no jugaras con conocimientos antiguos que no podrías esperar entender. Sin embargo, abriste la caja de Pandora y no te gustó lo que encontraste dentro.

{{< br >}}

Ahora algo te persigue, te aterroriza: algo que no está vinculado a las reglas de nuestra realidad. Fantasmas? ¿Algún tipo de demonio? Desearías saberlo, aunque parte de ti también se alegra de no saberlo. Lo que sí sabe es que tiene que terminar lo que comenzó, y rápido.

{{< br >}}

Aterrorizada y desesperada, lo único que se te ocurre es buscar la ayuda de la anciana que primero te habló del ritual. Ella es la única que puede ayudarte a terminar con esta pesadilla.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu or equivalent
* Memoria: 4 GB de RAM
* Gráficos: NVIDIA GeForce GTS 450 or equivalent
* Almacenamiento: 200 MB de espacio disponible
* Tarjeta de sonido: Onboard

{{< br >}}
{{< br >}}
