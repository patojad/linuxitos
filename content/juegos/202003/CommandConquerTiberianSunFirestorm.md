---
title: 'Command & Conquer Tiberian Sun + Firestorm'
date: '2020-03-11 15:12:00'
type: 'otros'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Command & Conquer Tiberian Sun","Firestorm","Estrategia","Otros"]
img: 'https://cdn.cnc-comm.com/assets/tiberian-sun/tsbox-thumb.png'
download: 'https://t.me/c/1118272073/11542'
---

# Command & Conquer Tiberian Sun + Firestorm

{{< br >}}

Tiberian Sun es el cuarto juego de la serie Command & Conquer. Es la secuela de Command & Conquer (Tiberian Dawn). Tiberian Sun comienza varias décadas después de la conclusión del primer juego.

Tiberian Sun no fue tan bien recibido como los otros juegos de la serie C&C. Fue criticado por ser lento, y demasiado parecido a sus contrapartes más antiguas, sin mencionar los errores, problemas de equilibrio y las características prometidas que nunca se implementaron. Sin embargo, muchos de los problemas con Tiberian Sun desaparecieron después de la caída de los parches.

{{< br >}}
{{< br >}}

## LA TRAMA

{{< br >}}

Han pasado unos 35 años desde el final de Tiberian Dawn. Kane se presume muerto y la Hermandad está en desorden. Nod lucha contra sí mismo; hermano contra hermano. Nod ha sido corrompido incluso a través de sus niveles más altos; un títere controlado por GDI llamado Hassan está a cargo. Con la Hermandad dividida y dispersa, el GDI ha puesto su foco en otra parte; que contiene la amenaza de tiberio. En las últimas tres décadas, se ha hecho evidente que Tiberium está terraformando el planeta al consumir ecosistemas terrestres y engendrar sus propios ecosistemas alienígenas.

El tiberio no es el único problema de GDI; Los seguidores de Kane todavía existen y no se detendrán ante nada hasta que hayan revelado la verdad sobre Hassan y sus seguidores. Anton Slavik, el líder de este grupo, ha sido ejecutado por Hassan por ser un "espía GDI", pero poco saben, incluso entre los soldados más elitistas de Hassan hay personas que se oponen a su gobierno y Slavik es liberado. A pesar de esto, Hassan transmite mensajes falsos sobre la supuesta muerte de Slavik, pero esto no sale según lo planeado para Hassan. Slavik se convierte en un héroe de la Hermandad, y cuando la gente descubre que todavía está vivo, se levantan contra Hassan. Hassan es arrinconado y capturado, lo llevan a las masas y en sus últimos momentos recibe la conmoción de su vida; "¡No puedes matar al Mesías!"

Después de este incidente, los puestos avanzados de GDI son atacados; en todas partes y desde todas las direcciones. Aunque parezca ridículo, no hay duda de quién ha regresado; El verdadero líder de Nod ...

¡Así comienza la Segunda Guerra del Tiberio!

{{< br >}}
{{< br >}}

## LAS FACCIONES

{{< br >}}
{{< br >}}

### LA INICIATIVA DE DEFENSA GLOBAL

{{< br >}}
{{< img src="https://cdn.cnc-comm.com/assets/tiberian-sun/gdi.png" >}}
{{< br >}}

"Bienvenido de nuevo, comandante ..." - EVA

"¡No le tengo miedo a los fantasmas, ni a ti!" - General Salomón

El GDI ha evolucionado de una fuerza de ataque a una fuerza global de mantenimiento de la paz. GDI se ha convertido en el poder militar del mundo entero y se han establecido en todo el mundo. Actuando como la policía y los militares.

Muchos de los tanques de GDI han sido reemplazados por grandes mechs bípedos. El proyecto Orca ha continuado y se ha convertido en el caza Orca, el bombardero Orca, el transporte Orca y las enormes naves orca.

Las principales prioridades del GDI son mantener la ley y el orden. Otras prioridades son principalmente la investigación de Tiberium.

El alto mando de GDI se encuentra en la estación espacial masiva llamada Filadelfia.

{{< br >}}
{{< br >}}

### LA HERMANDAD DE NOD

{{< br >}}
{{< img src="https://cdn.cnc-comm.com/assets/tiberian-sun/nod-thumb.png" >}}
{{< br >}}

"¡Hermandad! Nosotros. Estamos. ¡Enteros de nuevo! La enfermedad. ¡Ha sido eliminada!" - Anton Slavik

"Si me cortan, ¿no sangro? .." - Kane

La Hermandad ha tenido tiempos difíciles. Ya no son la gran amenaza que alguna vez fueron. Nod suele pelear entre ellos, su líder es un títere GDI. Aún así, la Hermandad ha podido evolucionar, su investigación en Tiberium les ha dado el poder de crear Cyborgs, mitad hombre, mitad máquina. También han podido crear vehículos capaces de viajar bajo tierra.

Las prioridades de Nod aún son desconocidas, pero no son tan hostiles hacia GDI como lo fueron antes.

Nod high command está controlado por el títere GDI Hassa * bzzzzzzz * [codificado] "¡Destruiremos GDI!" [revuelto] * bzzzzzk * Sin embargo, Hassan está mirando su espalda, ya que hay rumores del regreso del Mesías.

{{< br >}}
{{< br >}}

### TIBERIO

{{< br >}}
{{< img src="https://cdn.cnc-comm.com/assets/tiberian-sun/tiberium.png" >}}
{{< br >}}

"Las posibilidades de Tiberium ... son ilimitadas" - Dr. Moebius, experto mundial en Tiberium.

Tiberium, llamado así por el río Tiber donde se descubrió por primera vez, es tanto un cristal como una forma de vida que llegó a la Tierra en un meteorito. Se propaga a través de medios desconocidos, pero ha demostrado ser muy útil. Tiberium absorbe minerales del suelo causando la formación de cristales de Tiberium. Estos cristales son ricos en minerales preciosos y están disponibles a un mínimo de gasto minero.

Desde la Primera Guerra del Tiberio, las propiedades más mutagénicas del Tiberio se han vuelto más evidentes. Comenzó con árboles en flor y viceroides, pero desde entonces otras criaturas como el demonio y las venas tiberianas han sido descubiertas con informes de otras criaturas basadas en Tiberium que fluyen. Una cosa es segura de que Tiberium está cambiando nuestro planeta y, si no se combate, creará un mundo completamente ajeno a nosotros.

{{< br >}}
{{< br >}}

### LOS OLVIDADOS

{{< br >}}
{{< img src="https://cdn.cnc-comm.com/assets/tiberian-sun/mutants-thumb.png" >}}
{{< br >}}

"A diferencia de ustedes Blunts, ¡los Olvidados son un pueblo de honor!" - Umagon

A medida que Tiberium se extendió por todo el mundo, más y más personas entraron en contacto con él. La mayoría de las personas murieron por exposición y aquellos que no sufrieron el destino de convertirse en un virrey. Sin embargo, algunos sobrevivieron reteniendo tanto su libre albedrío como la humanidad, pero aún así quedaron marcados por la mutación del Tiberium. Estas personas se conocen como mutantes o espinillas después de que los cristales crecen en su piel. Mientras que algunos de estos mutantes son el resultado de una exposición más natural, otros son el resultado de la experimentación de Tiberium realizada por Nod.

Los mutantes rechazados, temidos e incomprendidos a menudo no se llevan bien con sus contrapartes "puras" y, por lo tanto, viven lejos de los asentamientos o de su propia especie. Se refieren a sí mismos como "Los Olvidados", ya que no se los considera parte de la vida de los no afectados por la mutación del Tiberio, a quienes los Olvidados se refieren como "Blunts".

{{< br >}}
{{< br >}}
