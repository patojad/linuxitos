---
title: 'Broken Lines'
date: '2020-03-28 14:04:00'
type: 'gog '
idiomas: ["español"]
category: ["estrategia"]
tags: ["Broken Lines","Estrategia","GOG"]
img: 'https://images.gog-statics.com/f53f587b110e805fc1e470ef327c04db530482bb238a29fcee24d19ada1471eb.jpg'
download: 'https://t.me/c/1118272073/11968'
---

# Broken Lines

{{< br >}}

Su escuadrón ha aterrizado en un desastre detrás de las líneas enemigas en el corazón de una versión alternativa de Europa del Este. Sin información y oficiales que los ayuden, estos soldados deben luchar para llegar a casa antes de que los horrores de la guerra terminen destrozándolos. El trabajo en equipo y la estrategia son esenciales ...

Cada soldado tiene su propia personalidad y su opinión sobre el camino a seguir. Algunos quieren investigar la causa del accidente, otros quieren mantener un perfil bajo y esperar ayuda. Incluso se escuchan susurros sobre una posible deserción ...

Usted es la "mano invisible" que debe guiar a este variopinto grupo de soldados en seguridad. Ya sea una elección tan simple como el próximo camino a seguir, o algo mucho más complejo como acercarse a sus misteriosos enemigos, cada decisión que tomes será importante.

{{< br >}}
{{< br >}}

## Características

{{< br >}}

Broken Lines es un juego de rol táctico con una historia impresionante. Tendrás que liderar a tus soldados a pesar de su fatiga debido al desgaste de la guerra, pero ¿cuál será la estrategia a adoptar: evitar al enemigo o enfrentarlos en combate directo?

Los combates son similares a los juegos de rol tácticos por turnos, pero los soldados solo se mueven cuando comienza la fase de acción. Sin embargo, se detendrá cuando aparezcan nuevos enemigos u obstáculos, lo que le permitirá modificar sus planes ante este cambio. ¿Mantendrás tu carga hacia adelante o te retirarás para refugiarte?

Las elecciones que hagas darán forma a la historia y conducirán a diferentes fines. Ten cuidado con estas elecciones: tu progreso en el juego afectará no solo a los soldados que mandas, sino también a toda una región y a todos los que están allí. También tenga en cuenta que sus soldados tienen sus propias opiniones sobre lo que es bueno para el escuadrón. Por lo tanto, tendrá que mantener un equilibrio frágil entre sus acciones y lo que debe hacerse para mantener alta la moral de su escuadrón.

“Victoria a toda costa, victoria a pesar del terror, victoria tan larga y dura como el camino nos llevará allí; porque sin victoria no hay supervivencia. "
- Winston Churchill

{{< br >}}
{{< br >}}
