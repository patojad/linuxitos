---
title: 'Zombie Night Terror Special Edition Upgrade'
date: '2020-03-11 11:23:00'
type: 'gog'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Zombie Night Terror","Special Edition Upgrade","Estrategia","GOG"]
img: 'https://images.gog-statics.com/bd25f325b6d1a9de9d71b8d2ca0ba51ee4a3c9c2cc6c31805c4feba957f1c0a5_bg_crop_1366x655.jpg'
download: 'https://t.me/c/1118272073/11519'
---

# Zombie Night Terror Special Edition Upgrade

{{< br >}}

¡Prepárate para la noche más emocionante de tu vida! Algo extraño ha sucedido y la gente en todas partes se está convirtiendo en cadáveres sedientos de sangre. Pero adivina quién es el cerebro detrás de este ejército de muertos vivientes hambrientos. ¡TÚ! Así que difunda esta pandemia para borrar a la humanidad de este planeta. ¡Porque la única forma de sobrevivir al apocalipsis zombie es SER el apocalipsis!

{{< br >}}

Pero los muertos vivientes sin cerebro son bastante tontos ... incluso más tontos de lo que cabría esperar dada su falta de materia gris. Porque cuando se les deja a sus propios dispositivos, simplemente caminarán sin parar hacia adelante y caerán en trampas explosivas permanentemente mortales colocadas por humanos no tan indefensos. Por lo tanto, para cumplir su misión de exterminio mundial, deberá ayudar a guiarlos a su próxima comida deliciosa. Afortunadamente, puedes utilizar numerosas mutaciones especiales para que tus tropas evolucionen y satisfagan su apetito por la carne. ¡Pero cuidado! Los humanos no facilitarán tu oscura voluntad ... lucharán para sobrevivir.

{{< br >}}

Extiende el terror a través de 40 niveles e inscribe a más zombis en tu ejército de muertos vivientes. En el camino tendrás que resolver acertijos y luchar contra enemigos cada vez más poderosos que están empeñados en mantenerse con vida. Cuanto más te acerques a la extinción total del mundo, más sobrevivientes lucharán para ponerte en el suelo ... para siempre. Sangre, lágrimas, gemidos no sexuales, risas inapropiadas y toneladas de acertijos que literalmente te volarán los sesos ... ¡ Esto es Zombie Night Terror!
Dirige una Horda Zombi : Tú eres el cerebro, ellos son los músculos. Su horda de zombis obedecerá sus órdenes sin ningún riesgo de ataque o quejas ... En el lado negativo; ¡ya no puedes tener conversaciones acaloradas sobre películas nocturnas o libros geniales!

{{< br >}}
{{< img src="https://images.gog-statics.com/d666e2425aedc1aef071101a9796708d36a50c463e4ceab8b3c64d780f23f0f6.jpg" >}}
{{< br >}}

## Caracteristicas

{{< br >}}

**Sistema de mutación único** : tener un ejército de zombis mortales es increíble. ¡Tener un ejército de zombis mutantes es mucho mejor! Aproveche nuestro sistema de mutación para superar las defensas del enemigo, creando nuevos y poderosos tipos de zombis. Cada uno tiene su poder y habilidades únicos que pueden ayudarlo a superar diferentes situaciones.

**Sistema combinado** : Debido a que pensabas que las mutaciones eran tu única forma de facilitar tu festín de carne, lo condimentamos permitiéndote combinarlas y ver de qué están hechos tus enemigos (literalmente). ¡Enseñemos a esos molestos humanos cómo divertirse!

**No estás solo** : tener zombies es como tener una mascota; es divertido, pero debes cuidarlos ... Para eso están los humanos, ya que son la manera perfecta de mantener a tus zombis bien alimentados y ocupados. ¡Pero cuidado! Los humanos están bien armados y no aprecian la compañía dentuda de los muertos vivientes.

**Contaminar** : ¿No tienes suficiente compañía? ¿Necesitas una horda más grande? Bueno, ¡entonces convierte a los humanos a tu causa! ¡No es necesario que traigas flores y seas encantador, ya que un solo mordisco directo en el cuello (o en cualquier lugar que te agrade) debería ser suficiente para hacer el truco!

**Entornos destructibles** : en tiempos de apocalipsis zombie, a los humanos les encanta tomarse su tiempo para jugar a las escondidas. Sin embargo, cuando el hambre de carne sabrosa es tan dolorosa, no tienes tiempo para ser tan infantil. Así que usa el poder o tu horda para destruir su patio de recreo y esparcir terror entre ellos ... ¡PEEKABOOM!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema:Ubuntu 16.04, 18.04 o más reciente
* Procesador: Intel Core 2 Duo o más rápido
* Memoria: 2 GB de RAM
* Gráficos: AMD Radeon HD 5750 / Nvidia GT 450 o superior
* Almacenamiento: 600 MB de espacio disponible

{{< br >}}
{{< br >}}
