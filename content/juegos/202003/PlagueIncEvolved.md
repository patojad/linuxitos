---
title: 'Plague Inc: Evolved'
date: '2020-03-04 22:11:00'
type: 'steam'
idiomas: ["español"]
category: ["simulacion"]
tags: ["Plague Inc","Evolved","Simulacion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/246620/header.jpg'
download: 'https://t.me/c/1118272073/11499'
---

# Plague Inc: Evolved

{{< br >}}

Es una mezcla única entre profunda estrategia y simulación tremendamente realista. Tu patógeno sólo ha infectado a un 'Paciente Cero' - ahora debes poner fin a la humanidad haciendo evolucionar una plaga mortal y global mientras la humanidad hace lo que puede...

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/246620/extras/steam_sub_header_spanish01.png" >}}
{{< br >}}

Plague Inc: Evolved incluye una mezcla única de estrategia y simulación terroríficamente realista. Tu patógeno acaba de infectar al paciente cero. Ahora tu misión es conseguir que la humanidad se extinga propagando una epidemia letal a la vez que saboteas los esfuerzos de la población para combatirla.

Plague Inc. es tan realista que los CDC (Centros para el Control y la Prevención de Enfermedades) pidieron al equipo desarrollador que les hablara sobre los modelos de infección del juego.

¡Plague Inc. ya ha infectado a más de 120 millones de jugadores! Ahora, Plague Inc: Evolved combina la aclamada mecánica del juego original con un montón de características nuevas para ordenador, como el modo multijugador, ayuda para que los usuarios creen su propio contenido, gráficos mejorados ¡y mucho más!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/246620/extras/steam_sub_header_spanish03.png" >}}
{{< br >}}

**10 tipos de enfermedad diferentes**. Domina cada patógeno, desde las bacterias hasta las armas biológicas y desde el control mental hasta los zombis. Dedica todos los medios a tu alcance para acabar con la humanidad, pero recuerda que cada enfermedad necesitará un enfoque totalmente distinto.

**20 escenarios únicos**. Adapta tu estrategia a los diferentes escenarios, que serán un reto añadido para la pandemia. ¿Cómo gestionarás una nueva cepa de gripe porcina o infectarás a un mundo sumido en la Edad de Hielo?

**Mundo hiperrealista**. Idea estrategias en el mundo real. Un avanzado sistema de inteligencia artificial y el uso de datos y eventos reales hacen de Plague Inc: Evolved un simulador realista de un patógeno capaz de acabar con la humanidad. ¡Hasta a los CDC les gusta!

**Modo multijugador competitivo**. El mundo ya tiene suficiente mala suerte con dos plagas, ¿pero cómo le ganarás a tu rival la batalla por la dominación genética? Los jugadores tendrán a su disposición evoluciones totalmente nuevas, habilidades y genes que les ayudarán a luchar por el control del mundo y destruir a su rival.
Modo cooperativo. Dos enfermedades distintas forman equipo para infectar el mundo y destruirlo, pero la humanidad tiene un par de trucos escondidos en la manga ¡y todavía se resiste! Trabaja codo a codo con tus amigos para usar genes y rasgos totalmente nuevos e idead estrategias para sabotear la búsqueda de la cura antes de que os erradiquen.

**Creador de contenido contagioso**. Ponte la bata blanca ¡y manos a la obra! Desarrolla tus escenarios personalizados, crea nuevos tipos de plaga, mundos y eventos del juego. Una gran variedad de herramientas sofisticadas ayudarán a los jugadores a dar forma a sus ideas más mortíferas y compartirlas en Steam Workshop. Con más de 10 000 escenarios ya disponibles, siempre hay algo nuevo que infectar.

**Gráficos deslumbrantes**. Los modelos de la enfermedad en 3D te acercarán más que nunca a tu plaga. Las cámaras repartidas por la ciudad muestran la lucha de la humanidad en las calles, y el escáner corporal destaca todos los efectos de tus mutaciones órgano a órgano.

**Datos letales**. Analiza estadísticas y gráficos, monitoriza los niveles de infección y mortalidad, mantén un seguimiento de las reacciones de los gobiernos y las labores de búsqueda de una cura y comprueba el éxito (o el fracaso) de tu plaga en las repeticiones de partidas completas.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 14.04 & Steam OS 1.59 (Versions Steam itself supports)
* Procesador: 2.0 GHz Dual Core Processor
* Memoria: 2 GB de RAM
* Gráficos: Intel HD Graphics 4000 or greater
* Almacenamiento: 500 MB de espacio disponible
* Notas adicionales: Unsupported video chipsets : Intel HD Graphics 3000, Intel GMA X3100, Intel GMA 950

{{< br >}}
{{< br >}}
