---
title: 'WRATH: Aeon of Ruin'
date: '2020-03-22 16:18:00'
type: 'steam'
idiomas: ["ingles"]
category: ["shooter"]
tags: ["WRATH","shooter","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1000410/header.jpg'
download: 'https://t.me/c/1118272073/11747'
---

# WRATH: Aeon of Ruin

3D Realms, los creadores de Duke Nukem 3D, Prey y Max Payne, se complacen en presentar un juego de disparos en primera persona con la tecnología del legendario Quake 1. Si te gustó la temática de nigromancia de Quake, te encantará WRATH. Pero no vivirás para contarlo...

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/ES_WRATH_ROADMAP_Update_2.png" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Title.png" >}}
{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/About.png" >}}
{{< br >}}

No eres de este mundo. Naufragaste en un mar más antiguo que el tiempo y fuiste a parar a las orillas de un mundo que agoniza. De la oscuridad que todo lo consume surge una figura vestida de blanco, una guía para las almas perdidas, que te encomienda la tarea de dar caza a los guardianes del viejo mundo. Deberás aventurarte en este mundo en penumbra, explorar ruinas antiguas, desvelar secretos olvidados y enfrentarte a los horrores que acechan.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Steam_Gif1.gif" >}}
{{< br >}}

WRATH está inspirado en la tecnología de Quake 1 y cuenta con la esencia de los juegos de disparos más populares de los años 90. WRATH utiliza elementos atemporales de títulos clásicos como DOOM, QUAKE, DUKE NUKEM 3D, BLOOD, UNREAL y HEXEN y los adapta al siglo XXI.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Steam_Gif2.gif" >}}
{{< br >}}

Equípate con un arsenal de poderosas armas y un inventario de artefactos místicos y atraviesa criptas antiguas, ruinas sumergidas, templos corruptos y bosques vivientes para dar muerte a tus adversarios. Pero nunca los subestimes, pues te superan en fuerza y en número. Deberás poner a punto tanto tu espada como tu mente si quieres sobrevivir a los peligros que te esperan.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Highlights.png" >}}
{{< br >}}

En el mundo de WRATH te esperan combates fluidos, escenarios de lo más diversos y una historia que te hipnotizará. Todos los elementos del mundo están conectados para crear una experiencia auténtica y tan atemporal como los juegos en los que está inspirado.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Steam_Gif3.gif" >}}
{{< br >}}

• **Explora** un vasto mundo sumido en la oscuridad creado por los nigromantes de Quake.

• Forja tu camino con un arsenal de **armas letales** con distintos modos de disparo.

• **Conoce a tu enemigo**. En las sombras se ocultan horrores de todo tipo sedientos de tu sangre.

• Encuentra los **artefactos** ocultos en los oscuros confines del mundo y somete a tus enemigos con su inmenso poder.

• Explota al máximo la tecnología del legendario Quake 1, que hace de WRATH un auténtico FPS clásico en todos los sentidos.

• Déjate hechizar por el sonido ambiental de las retorcidas mentes de Andrew Hulshult (Quake Champions, Rise of the Triad, Dusk y Amid Evil) y **Bjorn Jacobson** (CyberPunk 2077, Hitman, EVE Online).

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1000410/extras/Steam_Gif4.gif" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Pentium 4 3ghz
* Memoria: 1 GB de RAM
* Gráficos: Nvidia Geforce 7950GT
* DirectX: Versión 9.0
* Almacenamiento: 3 GB de espacio disponible
* Tarjeta de sonido: Integrated
* Notas adicionales: 64-bit only

{{< br >}}
{{< br >}}
