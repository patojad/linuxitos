---
title: 'The Colonists'
date: '2020-03-22 14:35:00'
type: 'gog'
idiomas: ["ingles"]
category: ["simulacion"]
tags: ["The Colonists","Simulacion","GOG"]
img: 'https://images.gog-statics.com/47bcc5b4970cfd6a77e2291eb43b18e362b04b7a8e58b0729a9fd93e09967d86.jpg'
download: 'https://t.me/c/1118272073/11715'
---

# The Colonists

{{< br >}}
{{< img src="https://images.gog-statics.com/4a4a4979dae1a021534c5a1b868d061191c7bfd2c903369bacb01295efe7ef69.jpg" >}}
{{< br >}}

The Colonists es un juego de construcción de asentamientos inspirado en títulos clásicos como The Settlers y la serie Anno.

Tomas el control de un equipo de robots autorreplicantes que han escapado de la Tierra y están buscando en la galaxia un nuevo hogar donde puedan cumplir su sueño: ser humanos.

Avanzará a través de tres Edades diferentes a medida que construye infraestructura para su colonia mediante la construcción de sistemas de transporte por carretera, barco y tren.

Aproveche los recursos naturales, establezca la agricultura y la producción de alimentos, cree expediciones para descubrir nuevas tierras e investigar nuevas tecnologías.

Con dos pistas de misión separadas, The Colonists te permite crear asentamientos en paz o competir contra las colonias de IA en escenarios militares donde el ganador se lleva todo.

¡Explore, investigue, gestione, modifique y refine el contenido de su corazón!

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema: Ubuntu 16.04, 18.04 o posterior
* Procesador: Intel Core i5-4690 o AMD Ryzen 7 2700U
* Memoria: 4 GB de RAM
* Gráficos: GeForce GTX 560 o AMD Radeon HD 7770
* Almacenamiento: 1 GB de espacio disponible

{{< br >}}
{{< br >}}
