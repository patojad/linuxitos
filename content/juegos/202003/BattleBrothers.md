---
title: 'Battle Brothers'
date: '2020-03-26 18:44:00'
type: 'otros'
idiomas: ["ingles"]
category: ["rpg","estrategia"]
tags: ["Battle Brothers","RPG","Estrategia","Otros"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/365360/header.jpg'
download: 'https://t.me/c/1118272073/11911'
---

# Battle Brothers

{{< br >}}

Es un juego de rol táctico por turnos que te lleva a liderar una compañía de mercenarios en un mundo de fantasía medieval arenoso y de baja potencia. Usted decide a dónde ir, a quién contratar o pelear, qué contratos tomar y cómo entrenar y equipar a sus hombres en una campaña de mundo abierto generada por procedimientos. ¿Tienes lo que se necesita para llevarlos a través de batallas sangrientas y a la victoria?

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/365360/extras/store_header_3.png" >}}
{{< br >}}

El juego consiste en un mapa del mundo estratégico y una capa de combate táctico. En el mapa mundial puedes viajar libremente para tomar contratos que te den una buena moneda, encontrar lugares que valgan el saqueo, enemigos que valga la pena perseguir o ciudades para reabastecer y contratar hombres. Aquí también es donde gestionas, subes de nivel y equipas a tus hermanos de batalla. Una vez que participes en una fiesta hostil, el juego cambiará a un mapa táctico donde la lucha real se lleva a cabo como un combate detallado por turnos.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/365360/extras/features.jpg" >}}
{{< br >}}

* Gestione una empresa mercenaria medieval en un mundo abierto generado por procedimientos.
* Lucha en complejas batallas tácticas por turnos con equipo histórico y heridas brutales.
* Permadeath. Todos los personajes que mueran en combate permanecerán muertos, a menos que regresen como muertos vivientes.
* Desarrollo de personajes sin un sistema de clases restrictivo. Cada personaje gana experiencia a través del combate, puede subir de nivel y adquirir ventajas poderosas.
* Equipo que importa. Diferentes armas otorgan habilidades únicas: escudos divididos con hachas, aturdir a los enemigos con mazas, formar un muro de lanza con lanzas o aplastar la armadura con un martillo de guerra.
* Diversa lista de enemigos. Todos los enemigos tienen equipos únicos, habilidades y comportamiento de IA.
* Un sistema de eventos dinámico con encuentros atmosféricos y decisiones difíciles fuera del combate.
* Tres crisis del juego tardío, una guerra entre casas nobles, una invasión de pieles verdes y un azote de muertos vivientes, agregan una amenaza inminente.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/365360/extras/about_us.jpg" >}}
{{< br >}}

Overhype Studios es un estudio de desarrollo de juegos independiente de Hamburgo, Alemania. Estamos dedicados a hacer grandes juegos que queremos jugar nosotros mismos. Con Battle Brothers nos esforzamos por reflejar la creatividad, la complejidad y la originalidad de los viejos tiempos cuando los desarrolladores de juegos eran jugadores apasionados, no empresarios corporativos. Mientras hacíamos esto, nos inspiramos mucho en algunos de los mejores juegos: el X-Com original, Warhammer: Shadow of the Horned Rat y Jagged Alliance.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: 1.2 Ghz
* Memoria: 1024 MB de RAM
* Gráficos: OpenGL 3.3 compatible video card with 512 MB
* Almacenamiento: 1500 MB de espacio disponible

{{< br >}}
{{< br >}}
