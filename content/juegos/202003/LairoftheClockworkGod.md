---
title: 'Lair of the Clockwork God'
date: '2020-03-25 15:37:00'
type: 'gog'
idiomas: ["ingles"]
category: ["aventura"]
tags: ["Lair of the Clockwork God","Aventura","GOG"]
img: 'https://images.gog-statics.com/f95271c37c46a3812a63e46e5f21b92f4572748d0738499150ec9a83ed57069c.jpg'
download: 'https://t.me/c/1118272073/11827'
---

# Lair of the Clockwork God

{{< br >}}

¿**POR QUÉ JUGAR SOLO UN GÉNERO DE JUEGO** cuando podrías estar jugando a dos ligeramente diferentes al mismo tiempo?

{{< br >}}
{{< br >}}

## BEN es un fanático de la vieja escuela LucasArts Adventure.

{{< br >}}

Tiene un pie atrapado en los años 90, sus pies nunca dejan el piso, y es más feliz recogiendo cualquier basura vieja en la que pueda poner sus manos, con la esperanza de combinarlo todo para resolver un rompecabezas satisfactorio.

{{< br >}}
{{< br >}}

## Su cohorte y compañero DAN

{{< br >}}

... tiene aspiraciones de ser el próximo gran personaje independiente de la plataforma independiente. Tiene todo lo que necesita: es moderno, sensible y su nariz tiene un color diferente al resto de su cara.


**LAIR OF THE CLOCKWORK DIOS** lo ve cambiar entre ambos personajes y usar sus habilidades únicas juntos en un esfuerzo de carrera contra el tiempo para evitar que todos los Apocalipsis ocurran simultáneamente, al enseñarle a una computadora vieja acerca de los sentimientos.

Resuelve acertijos clásicos de estilo de apuntar y hacer clic como Ben para crear elementos de actualización únicos para Dan, para que pueda saltar más alto, correr más rápido y disparar a todo con una nueva y brillante arma. ¡Luego, corre y salta como Dan para desbloquear nuevas áreas y nuevos y emocionantes rompecabezas para Ben!

{{< br >}}
{{< img src="https://items.gog.com/lair_of_the_clockwork_god/cutsomImage.png" >}}
{{< br >}}

Si no has jugado los viejos juegos de Dan y Ben (deberías), o si tienes pero no puedes recordar lo que pasó, ¡NO NECESITAS PREOCUPARTE! Lair of the Clockwork God es una aventura independiente de Dan y Ben, lo que significa que no necesitas saber absolutamente nada sobre Ben There, Dan That! o señores del tiempo, por favor! lo que. No se hace referencia a ninguna parte de la trama original, los personajes se reintroducen de manera experta, etc. Todo está bien.
