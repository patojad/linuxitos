---
title: 'Lethal League Blaze'
date: '2020-03-26 20:46:00'
type: 'steam'
idiomas: ["español"]
category: ["accion"]
tags: ["Lethal League Blaze","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/553310/header.jpg'
download: 'https://t.me/c/1118272073/11934'
---

# Lethal League Blaze

{{< br >}}

Es un juego de pelota frenético y veloz con personajes únicos y sonidos de otro mundo.
El objetivo es estamparle la pelota antigravedad al enemigo usando distintos tipos de golpes, paradas, mates y ataques especiales.
La pelota acelera con cada golpe hasta desafiar los límites de lo posible.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/553310/extras/storepage_img_switch.png" >}}
{{< br >}}

## Características

{{< br >}}

**Caos de hasta 4 jugadores**:
Juega de forma local Y por Internet con un máximo de 4 jugadores. Si activas los potenciadores, será el caos total.

**Modos y más modos**:
Juega en Todos contra todos, A golpes, Por equipos, Volea letal o lánzate por tu cuenta en los modos Arcade e Historia.

**Multijugador online**:
Descubre la competición real e invita a amigos a una partida privada o lánzate de cabeza a una partida rápida.

**Banda sonora de lujo**:
La banda sonora es el no va más, con música de Hideki Naganuma, Frank Klepacki, Pixelord, Bignic y, cómo no, Klaus Veen."

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Intel Core i3 2.5GHz or AMD Phenom 2.5GHz
* Memoria: 4 GB de RAM
* Gráficos: NVIDIA GeForce GTX 460 or ATI Radeon HD 5850
* Red: Conexión de banda ancha a Internet
* Almacenamiento: 4 GB de espacio disponible
* Notas adicionales: Intel onboard video cards are not recommended

{{< br >}}
{{< br >}}
