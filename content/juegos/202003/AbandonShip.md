---
title: 'Abandon Ship'
date: '2020-03-15 20:47:00'
type: 'gog'
idiomas: ["español"]
category: ["estrategia"]
tags: ["Abandon Ship","Estrategia","GOG"]
img: 'https://images.gog-statics.com/5d721da090ca724cc598182c1701b6a97045c921cda9434b8470fdd3423b89de.jpg'
download: 'https://t.me/c/1118272073/11618'
---

# Abandon Ship

{{< br >}}
{{< img src="https://items.gog.com/abandon_ship/Titles_Captain.png" >}}
{{< br >}}
{{< img src="" >}}
{{< br >}}

Descubre una gran cantidad de islas únicas llenas de historias a través de diversos biomas y áreas temáticas: lucha contra barcos fantasmas en los mares encantados. Evita los arácnidos gigantes en las Islas Araña. Regiones llenas de gases venenosos, icebergs, caníbales y mucho más esperan.

Enfréntate a naves enemigas, fortificaciones y monstruos marinos en combates viciosos, empleando tus mejores tácticas para maniobrar y disparar al enemigo. El clima y las condiciones ambientales afectan sus batallas: maremotos, rayos, bombardeos volcánicos y tormentas de nieve son solo algunos de los modificadores que impactan su estrategia.

Abandon Ship se centra en los barcos de 'Age of Sail' en un entorno de fantasía, enmarcado en un estilo artístico inspirado en las pinturas al óleo navales clásicas.

{{< br >}}
{{< img src="https://items.gog.com/abandon_ship/Title_Destruction.png" >}}
{{< br >}}

La vida en este mundo puede ser brutal. La muerte es permanente. Pero el viaje no termina si tu nave es destruida. Eres el Capitán, y mientras el Capitán esté vivo, siempre hay esperanza.

Al escapar a un bote salvavidas, o incluso quedar varado, solo en el agua, existe la posibilidad de sobrevivir y luchar para volver a la cima.

{{< br >}}
{{< img src="https://items.gog.com/abandon_ship/Title_Victory.png" >}}
{{< br >}}

El combate es táctico y salvaje. Cada batalla es dura, siempre al borde de la derrota. Su única posibilidad de superar las probabilidades es emplear todas las ventajas que pueda aportar.

Docenas de diversas armas y mejoras están disponibles para utilizar en las diferentes clases de barcos que puedes adquirir. Personaliza la apariencia de tus barcos capturando barcos enemigos intactos. Su tripulación de confianza gana experiencia y rasgos para ayudar a proporcionar una ventaja sobre el enemigo.

{{< br >}}
{{< img src="https://items.gog.com/abandon_ship/Title_Reap.png" >}}
{{< br >}}

Explora un mundo de fantasía que reacciona a tus acciones. Participa en la piratería y conviértete en un Capitán perseguido. Emprende misiones que pueden cambiar drásticamente el entorno, o el mundo entero. Tome decisiones que creen aliados o enemigos que regresen para ayudar o buscar venganza.

Juega varias historias, incluida la historia de las campañas principales sobre el derrocamiento de un Culto al estilo Cthulhu, un modo Freeplay sobre la reconstrucción del legado de tus antepasados ​​y cuentos más cortos, como la campaña de combate centrada en rescatar a tu hermana o Espada del Culto , donde juegas como uno de los hijos de mi padre, subes de rango hasta que te otorgan el arma definitiva: el Kraken.

{{< br >}}
{{< img src="https://items.gog.com/abandon_ship/Arrow.png" >}}
{{< br >}}
{{< br >}}
