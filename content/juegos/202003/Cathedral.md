---
title: 'Cathedral'
date: '2020-03-27 18:37:00'
type: 'gog'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Cathedral","Accion","GOG"]
img: 'https://images.gog-statics.com/dccf1082eb04d0b3a0e0acbc46fa4b699da5d94d6916954491d7ecf60bc7c864.jpg'
download: 'https://t.me/c/1118272073/11952'
---


# Cathedral

{{< br >}}

Despierta en un mundo sin recordar cómo llegaste allí. Un mundo lleno de secretos, habitaciones ocultas, mazmorras y pueblos. ¡La catedral presenta un vasto mundo destinado a explorar!

{{< br >}}
{{< img src="https://images.gog-statics.com/591318709fea6970699da5383dafb046c06d7790f1f521fc74efe4dfd5f93da7.jpg" >}}
{{< br >}}

Ábrete camino a través de más de 600 habitaciones y desentraña los secretos de tu pasado encontrando los cinco orbes elementales. Los orbes, colocados en la antigüedad por el semi-dios conocido solo como Ardur, están protegidos por cinco guardianes temibles. Ábrete camino a través de sus mazmorras, encuéntralos de frente en combate y ve si puedes superarlos.

{{< br >}}
{{< br >}}

## Lista de características

{{< br >}}

* Juego de aventura para un jugador
* Un gran mundo para explorar, lleno de secretos.
* Luchas desafiantes contra jefes
* Un sistema de mapas que hace que explorar y retroceder en el mundo sea divertido
* Muchos rompecabezas, áreas y enemigos diferentes.
* Impresionante música de 8 bits, hecha en Famitracker
* Muchas horas de juego

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema: Ubuntu 16.04, 18.04 o posterior
* Procesador: Intel Core i5 2.3GHz o equivalente. (Recomendado: Intel Core i7)
* Memoria: 4 GB
* Gráficos: Intel HD Graphics 3000, 512 MB o equivalente (Recomendado: Nvidia GM108M o equivalente)
* Almacenamiento: 50 MB (recomendado: SSD)
* Otro: Gamepad se recomienda al jugar

{{< br >}}
{{< br >}}
