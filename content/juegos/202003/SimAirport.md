---
title: 'SimAirport'
date: '2020-03-01 16:16:00'
type: 'steam'
idiomas: ["español"]
category: ["simulacion"]
tags: ["SimAirport","Simulacion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/598330/header.jpg'
download: 'https://t.me/c/1118272073/11464'
---

# SimAirport

{{< br >}}

Usted controla todo, desde las decisiones de altitud de crucero hasta los detalles más pequeños a nivel del suelo.

{{< br >}}

Ponte a prueba para crear un centro internacional eficiente y rentable en el Modo Carrera, o crea una obra maestra artística sin preocupaciones de calificación crediticia en el Modo Sandbox.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/598330/extras/Steam_Title_01.png" >}}
{{< br >}}

Construya su terminal, contrate personal, firme contratos de aerolíneas, modifique el horario de vuelo diario, configure la disponibilidad de la puerta de espera, planifique y diseñe su infraestructura (sistemas de manejo de bolsas, carreteras y calles de rodaje, sistemas de combustible, pistas, puertas, hangares, vehículos de servicio y todo lo demás. Juego profundamente simulado donde cada detalle impacta) hasta los botes de basura.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04+ or SteamOS
* Procesador: Intel i5/i7+ or AMD-FX+
* Memoria: 4 GB de RAM
* Gráficos: Dedicated GPU - Integrated GPUs might not work
* Tarjeta de sonido: Any

{{< br >}}
{{< br >}}
