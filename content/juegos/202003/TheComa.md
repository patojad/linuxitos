---
title: 'The Coma: Recut Deluxe Edition Upgrade'
date: '2020-03-26 17:35:00'
type: 'gog'
idiomas: ["español"]
category: ["aventura"]
tags: ["The Coma", "Recut Deluxe Edition Upgrade","Aventura","GOG"]
img: 'https://images.gog-statics.com/ef1724d79b598499ca23b704ad62d2982c268b7118b78a1b28200551689e24e6.jpg'
download: 'https://t.me/c/1118272073/11863'
---

# The Coma: Recut Deluxe Edition Upgrade

{{< br >}}

El Coma está de vuelta, recortado y remasterizado. Explore este festival de terror del clásico de culto independiente de Corea con una actualización visual y mecánica completamente nuevas.

{{< br >}}
{{< img src="https://items.gog.com/the_coma_gamecard_graphic.png" >}}
{{< br >}}

La buena noticia es que tu escuela está a punto de explotar. La mala noticia es ... ¡estás atrapado con ella!

El Coma está de vuelta, recortado y remasterizado. Explore este festival de terror del clásico de culto independiente de Corea con una actualización visual y mecánica completamente nuevas.

{{< br >}}

* Imágenes en 2D ilustradas a mano.
* Una infusión única de horror coreano en el género de supervivencia y aventura.
* Escuche atentamente los pasos que revelan el enfoque del asesino.
* Corre y escóndete del psicópata más implacable del mundo.
* Evita los tentáculos venenosos y las sombras de garras mientras exploras.
* Descubre notas y pistas que iluminan el misterio de Sehwa High.
* Trabaja junto con los demás que están atrapados aquí. ¿Pero se puede confiar en ellos?
* Agáchate en las sombras durante momentos intensos mientras el Asesino te busca.
* Revela el sorprendente misterio de tu escuela a través de mapas, notas y exploración.

{{< br >}}
{{< br >}}

## Correr y esconderse

{{< br >}}

Sin ningún escape a la vista, Youngho debe deambular por los pasillos de su escuela por la noche en busca de una salida. Durante este viaje se encontrará con muchas amenazas empeñadas en matarlo. Correr es la mejor manera de evitar el peligro; desafortunadamente, Youngho no es un gran atleta, por lo que no puede correr para siempre. ¡Encuentra un lugar para esconderte y mantén la boca cerrada!

{{< br >}}
{{< br >}}

## Explorar

{{< br >}}

El Coma es un mundo duro. Esta realidad retorcida de tu escuela secundaria no conoce amabilidad. Use mapas, notas y consejos de otros personajes para navegar por el mundo y descubrir cómo quedó atrapado en un lugar tan sombrío.

{{< br >}}
{{< br >}}

## Sobrevivir y escapar

{{< br >}}

Todo está listo para atraparte. Youngho tendrá que evitar un asesino persistente, tentáculos de azote y más para sobrevivir en la escuela. Desafortunadamente, la salud es escasa, y si no tienes cuidado, morirás. La buena noticia es que los puntos de guardado brindan un respiro de la avalancha de cosas que intentan matarte.

{{< br >}}
{{< br >}}

## Horror coreano

{{< br >}}

El Coma ofrece una visión, aunque horrible, de la vida de los estudiantes de secundaria con exceso de trabajo de Corea. Esperamos que disfrute de nuestra infusión de la cultura coreana en el género de terror de supervivencia.

{{< br >}}
{{< br >}}

## Como se Juega

{{< br >}}

Como Youngho, debes navegar a través de los tres edificios conectados de tu escuela secundaria usando tu ingenio. En el camino, encontrarás arte desbloqueable, notas y huevos de pascua que se expanden en el universo de Coma. Oh sí, ¿mencionamos el incesante asesino, las trampas y los inevitables juegos?

{{< br >}}
{{< img src="https://items.gog.com/the_coma_gamecard_graphic2.png" >}}
{{< br >}}

Después de pasar toda la noche estudiando para el último día de los exámenes finales de Sehwa High, Youngho tiene un sueño inquietante. Llega a la escuela a la mañana siguiente y descubre que alguien intentó suicidarse en la escuela la noche anterior. A pesar de esto, ¡la clase continúa!

Youngho se queda dormido durante la final para encontrarse atrapado en la escuela por la noche. El único problema es ... que no está solo. Tu puedes correr. Te puedes esconder ¿Puedes sobrevivir mientras reconstruyes el misterio de pesadilla de Sehwa High?

{{< br >}}
{{< img src="https://items.gog.com/the_coma_gamecard_graphic3.png" >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Sistema:Ubuntu 16.04, 18.04 o posterior
* Procesador: P4
* Memoria: 2 GB de RAM
* Gráficos: Cualquiera con aceleración 3D de hardware
* Almacenamiento: 1 GB de espacio disponible
* Sonido: Cualquiera con aceleración 3D de hardware
* Otro: ¡Auriculares!

{{< br >}}
{{< br >}}
