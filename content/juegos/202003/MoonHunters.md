---
title: 'Moon Hunters'
date: '2020-03-14 13:30:00'
type: 'gog'
idiomas: ["español"]
category: ["rpg"]
tags: ["Moon Hunters","RPG","GOG"]
img: 'https://images.gog-statics.com/f63231195507bf6e8e92cc641787c6da021014cd634a3aab92b8bca2f70f2c1d.jpg'
download: 'https://t.me/c/1118272073/11604'
---

# Moon Hunters

{{< br >}}

Un juego de rol cooperativo de "prueba de personalidad" de 1 a 4 jugadores sobre cómo convertirse en un héroe mitológico en un mundo antiguo y oculto. Los niveles y el mapa son diferentes cada vez, dándote nuevas opciones, enemigos y oportunidades para mostrar tu personalidad y convertirte en una leyenda.

{{< br >}}
{{< img src="https://images.gog-statics.com/c2154b2ab44093b79c760a6ee8cc28e4b290b00c41b906a293d28738c2692c0f.jpg" >}}
{{< br >}}

* Juega solo o cooperativo para hasta 4 jugadores (en línea o local)
* Da forma a tu personalidad y reputación a través de elecciones y acciones
* Juega varias veces para desbloquear más personajes y puntos de inicio

{{< br >}}
{{< br >}}
