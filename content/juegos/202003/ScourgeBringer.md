---
title: 'ScourgeBringer'
date: '2020-03-19 19:46:00'
type: 'steam'
idiomas: ["español"]
category: ["accion","plataforma"]
tags: ["ScourgeBringer","Accion","Plataforma","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/1037020/header.jpg'
download: 'https://t.me/c/1118272073/11698'
---

# ScourgeBringer

{{< br >}}

Es un rápido platformer roguelite con movimiento libre. Ayuda a Kyhra a explorar lo desconocido, cortando su camino a través de antiguas maquinas que resguardan su pasado, y quizás, redención para la humanidad.

De los desarrolladores de NeuroVoider, ScourgeBringer es un juego de plataformas roguelite rápido y de movimiento libre.

Ambientado en un mundo pos-apocalíptico en el que una misteriosa entidad destruyo a toda la humanidad, ScourgeBringer te pone en las botas de la guerrera mas violenta de su clan: Kyhra. Ayúdala a explorar lo desconocido y cortar su paso a través de antiguas maquinas que resguardan su pasado, y quizás, redención para la humanidad.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/1037020/extras/Key_Features.gif" >}}
{{< br >}}

* Lánzate a un rápido juego de plataformas con elementos rogue que Eurogamer ha descrito como «un cruce entre Dead Cells y Celeste»
* Hazte un camino cortando y disparando con controles súper fluidos
* Agudiza tus habilidades con un sistema de combate frenético enfocado solo en atacar
* Lanzate a la acción con los sobrenaturales sonidos adaptables de Joonas Turner (Nuclear Throne, Downwell, Broforce...)
* Desafiá hordas de enemigos innombrables y a jefes gigante que protegen los secretos del Scourge
* Explora las infinitas profundidades de un calabozo en constante cambio
* Descubre misterios y encuentra recuerdos de antiguos exploradores para desbloquear secretos que alteran la realidad

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 16.04+ 64bit, Debian 9+ 64bit
* Procesador: 64bit 1.5 GHz CPU
* Memoria: 2048 MB de RAM
* Gráficos: OpenGL 3.0 compliant graphics card and driver
* Almacenamiento: 300 MB de espacio disponible
* Notas adicionales: 64bit only

{{< br >}}
{{< br >}}
