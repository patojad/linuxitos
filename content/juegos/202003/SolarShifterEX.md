---
title: 'Solar Shifter EX'
date: '2020-03-11 18:52:00'
type: 'steam'
idiomas: ["ingles"]
category: ["accion"]
tags: ["Solar Shifter EX","Accion","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/363940/header.jpg'
download: 'https://t.me/c/1118272073/11561'
---

# Solar Shifter EX

{{< br >}}

Pasa a la acción con Solar Shifter EX. Dirige tu nave y sumérgete en feroces situaciones de combate en el espacio y sobre superficies planetarias hermosas y distantes. Solar Shifter EX es un juego de disparos de ciencia ficción con la capacidad única de 'cambiar'.

Su única arma es su nave: el Phase Shifter ha sido equipado con un motor de salto único que le permite salir de situaciones difíciles a voluntad, una técnica que lo desafiará a sus límites al sobrevivir a las masas de enemigos que vienen a ti desde todos los lados en este verdadero infierno de balas.

A la antigua usanza, te enfrentarás a innumerables formaciones de enemigos que intentarán detenerte. Sé inteligente y usa el impulso de salto de Phase Shifter para tu ventaja o recibirás un disparo.

{{< br >}}
{{< br >}}

## HISTORIA

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/363940/ss_ac99d3655621ad5cd503176a69156f1fdbd21fba.600x338.jpg" >}}
{{< br >}}

Poco después de que la humanidad dejó el sistema solar para colonizar nuevos mundos, tuvimos que enfrentar una poderosa raza alienígena. La guerra ha pasado, las pérdidas fueron devastadoras y el enemigo nos ha derrotado. Como golpe final, están a punto de destruir el sol que da vida al sistema que ahora llamamos hogar.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/363940/ss_08a4b558b239f31cd843b38938cbbea733c92d70.600x338.jpg" >}}
{{< br >}}

El caos estalló, y los asaltantes piratas están recolectando colonias humanas en el borde exterior. Su único objetivo es recolectar suficientes recursos para poder abandonar el sistema solar.

{{< br >}}
{{< br >}}

## CARACTERISTICAS

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/363940/ss_4dac1e4314d09848ad18464be8555cb94fbf2e73.600x338.jpg" >}}
{{< br >}}

* Mecánica única de 'desplazamiento' para traer un nivel completamente nuevo de profundidad a un shmup
* 18 misiones difíciles
* 8 hermosos ambientes
* Más de 40 tipos de enemigos encontrados a lo largo del juego.
* Mejora tu nave
* 4 naves controlables: una nave principal y tres naves alienígenas. Controla naves alienígenas todopoderosas que diezman las fuerzas aliadas con facilidad.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04
* Procesador: 2Ghz single core
* Memoria: 2 GB de RAM
* Gráficos: nVidia GeForce 8600 GT / ATI 2600 Pro
* Almacenamiento: 4 GB de espacio disponible
* Tarjeta de sonido: Any

{{< br >}}
{{< br >}}
