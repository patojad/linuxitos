---
title: 'Vambrace: Cold Soul'
date: '2020-03-23 22:35:00'
type: 'steam'
idiomas: ["español"]
category: ["rpg","fantasia"]
tags: ["Vambrace","RPG","Fantasia","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/904380/header.jpg'
download: 'https://t.me/c/1118272073/11769'
---

# Vambrace: Cold Soul

Es un roguelike de aventura y fantasía en medio de un helado paisaje. Planea tus expediciones subterráneas, viaja a la superficie de la ciudad maldita con tu grupo de héroes. ¡Usa poderes únicos, evita peligrosas trampas, encuentros extraños y sobrevive al combate mortal!

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/904380/extras/Synopsis_es.gif" >}}
{{< br >}}

El Rey de las Sombras ha maldecido a la gran ciudad de Icenaire. Ahora, bajo una plaga permanente de escarcha, sus antiguos residentes han regresado de la muerte como espectros. Los sobrevivientes se refugian en lugares subterráneos donde lideran una campaña desesperada contra este poder sobrenatural. Muy superados, se ven obligados a esconderse mientras el Rey de las Sombras crea un ejército de no-muertos sobre ellos. Un fatídico día, una misteriosa extraña aparece en la ciudad con un brazal encantado. Ella ahora puede ser su única esperanza...

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/904380/extras/Gameplay_es.gif" >}}
{{< br >}}

Eres Evelia Lyric, portadora del Aetherbrace y la única humana capaz de entrar en Icenaire. Los sobrevivientes te consideran su única esperanza contra el Rey de las Sombras. Solo un problema... estás superada, y la supervivencia no está garantizada.

Vambrace: Cold Soul es un juego guiado por la narrativa llena de personajes memorables, desafíos brutales y profunda estrategia. Para tener éxito, debes elegir líderes perspicaces, acampar para recuperarte, navegar por lugares extraños y sobrevivir a un combate mortal. ¿Hurgarás suministros para venderlos o los usarás para elaborar nuevos objetos para tu próxima expedición? La superficie de Icenaire es fría e implacable. Así que, prepara bien tu grupo antes de partir... no sea que te unas a los no-muertos de la ciudad maldita.

En este juego, la diferencia entre la vida y la muerte se reduce a una buena planificación e inteligentes tácticas.

{{< br >}}
{{< img src="https://steamcdn-a.akamaihd.net/steam/apps/904380/extras/Features_es.gif" >}}
{{< br >}}

Embárcate en una épica aventura de fantasía que abarca 7 intrigantes capítulos.
Completa misiones secundarias para desbloquear hasta 26 nuevos atuendos temáticos.
Crea tu grupo con 5 tipos de razas y 10 clases únicas de reclutas.
Elabora objetos y armaduras de metales preciosos encontrados en tus viajes.
Toma decisiones cruciales donde un pequeño error puede sellar el destino de tu grupo.
Prepárate bien o condena a tus compañeros a una muerte permanente.
Encuentra páginas del códice para ampliar la historia de Ethera y la tradición mítica.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu (64Bit)
* Memoria: 3 GB de RAM
* Gráficos: Any with hardware 3D acceleration
* Almacenamiento: 4850 MB de espacio disponible
* Tarjeta de sonido: Soundblaster / equivalent
* Notas adicionales: Earphones!

{{< br >}}
{{< br >}}
