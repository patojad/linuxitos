---
title: 'StarCraft Broodwar'
date: '2020-03-24 21:19:00'
type: 'otros'
idiomas: ["español"]
category: ["estrategia"]
tags: ["StarCraft","Estrategia","Otros"]
img: 'https://e.rpp-noticias.io/normal/2017/04/03/385638_378424.jpg'
download: 'https://t.me/c/1118272073/11808'
---

# StarCraft Broodwar

En marzo se reveló de 2017 que StarCraft: Brood War iba a ser remasterizado y que la versión que todos conocemos ya es gratis. Es decir, no tienes que recurrir a la piratería. Se puede descargar legalmente una copia del juego original. Puedes bajar aquí esta versión de StarCraft: Brood War 1.18 que fue revelada por el mismo Blizzard.

## Estos son los problemas corregidos respecto a otras versiones:

Agregado el modo de ventana de pantalla completa o modo ventana; usa Alt+Entrar para alternar entre modos.
Agregado soporte para UTF-8.
Agregada la restricción del cursor al estar en modo ventana al jugar; sin restricción al estar en menús.
Agregados Mapas Populares para facilitar el encontrar partidas o tipos de partidas.
Agregadas opciones para mostrar acciones por minuto.
Agregado el modo Observador.
Agregada la información de oponente al entrar a una sala de espera para una partida.
Agregada la grabación automática para repeticiones de jugadas.
Agregada una opción para mostrar el tiempo de juego.
Mejorada la velocidad de reacción del juego en multijugador al incrementar la tasa de rotación para igualar las velocidades LAN por medio de Battle.net.
Mejorado el diseño de la interfaz de usuario en secciones de Battle.net.
Mejorado el comportamiento de salas disponibles en la sección de ‘Unirse a un juego’.
Mejorada la compatibilidad con Windows 7, 8.1, y 10.
Mejoradas las capacidades anti trampas.
Mejoradas la instalación y el rendimiento de la instalación de parches.
Ahora hay soporte Beta para Mac disponible para 10.11 en adelante.
