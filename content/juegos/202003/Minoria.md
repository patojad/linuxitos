---
title: 'Minoria'
date: '2020-03-25 16:15:00'
type: 'gog'
idiomas: ["español"]
category: ["accion"]
tags: ["Minoria","Accion","GOG"]
img: 'https://images.gog-statics.com/5e6dc493268a05b62d7e4ad00c6446af679e22f0fb743dfdc374ea0762e2b2aa.jpg'
download: 'https://t.me/c/1118272073/11842'
---

# Minoria

Minoria es el último y más ambicioso juego de Bombservice, y una secuela espiritual de la serie Momodora , del mismo estudio.

{{< br >}}
{{< img src="https://images.gog-statics.com/d0707994e7bb6f3fd4213b6fe3b758727dc365e265c6de2a7faf3b0413acbba1.jpg" >}}
{{< br >}}

Los fanáticos de Momodora encontrarán aspectos familiares en los elementos de diseño y juego, ya que dominan los entresijos de paradas, esquiva y una variedad de hechizos diferentes.
La presentación ha evolucionado a un formato de alta definición, con una estética que mezcla fondos pintados a mano en 2D y personajes sombreados en celdas.

{{< br >}}
{{< img src="https://items.gog.com/minoria/Story_eng.png" >}}
{{< br >}}

La historia tiene lugar durante la cuarta guerra de brujas. Es una época de fervor religioso fanático. La Oficina Sagrada, una poderosa organización que lidera una Inquisición contra la herejía, purga a los pecadores que amenazan a la humanidad. Los responsables de llevar a cabo una ceremonia misteriosa que contradice las reglas de la Iglesia son etiquetados como "brujas".

Partieron con las hermanas Semilla y Fran, misioneras al servicio de la Iglesia, en su viaje para frustrar la ceremonia de las brujas y proteger a la gente común de la herejía que pone en peligro el status quo.

{{< br >}}
{{< img src="https://items.gog.com/minoria/key_features_eng.png" >}}
{{< br >}}

* Una poderosa historia de fantasía inspirada en la historia de la Europa medieval.
* Una estética visual única que combina arte 2D pintado a mano con cel-shading
* Controles fluidos que fomentan el compromiso cuidadoso y el juego paciente
* Combate ofensivo que permite a los jugadores aplastar a los enemigos con una combinación de espadas y hechizos.
* Opciones defensivas que incluyen esquivar rollos y paradas
* Un sistema de nivelación que ayuda a los jugadores a ajustar su personaje a la dificultad deseada.


{{< br >}}
{{< img src="https://items.gog.com/minoria/dev_msg_eng.png" >}}
{{< br >}}

¡Hola a todos! Soy rdein, el creador y director de Minoria.
En primer lugar, me gustaría compartir mi entusiasmo al anunciar este proyecto. Este es un universo que me importa mucho, desde el escenario hasta los personajes, y ha sido muy divertido explorar y desarrollar sus conceptos durante el proceso de desarrollo del juego.
Por primera vez en muchos años, comencé un proyecto de juego ambientado en un nuevo universo. Se sintió como un soplo de aire fresco para mí. Dicho esto, Minoria contiene algunas similitudes con mis proyectos anteriores (especialmente la última entrega de Momodora, Reverie Under the Moonlight).

¡Estoy orgulloso de lo que hemos estado haciendo para Minoria, y espero que disfruten la historia y los personajes que he preparado esta vez!

{{< br >}}
{{< img src="https://items.gog.com/minoria/pub_msg_eng.png" >}}
{{< br >}}

Hola amigos, Dan aquí de Dangen Entertainment. Ha sido difícil esperar pacientemente el anuncio de Minoria, sabiendo que será un juego increíble. Combina todo lo que amo de los juegos de Momodora con un mayor énfasis en una historia solemne. Me encanta cómo Bombservice se acerca a las imágenes esta vez también: es una toma única que se ve hermosa y muestra otro paso evolutivo en la experiencia de desarrollo del equipo, tal como lo han hecho los títulos anteriores.

Este juego es extraordinario. Un juego obligado si tienes apetito por plataformas de acción como yo.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* Procesador: Intel Core i5
* Memoria: 4 GB de RAM
* Gráficos: GeForce GTX 660
* DirectX: Versión 10
* Almacenamiento: 3 GB de espacio disponible

{{< br >}}
{{< br >}}
