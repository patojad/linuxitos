---
title: 'The Stanley Parable'
date: '2020-03-16 18:26:00'
type: 'steam'
idiomas: ["español"]
category: ["aventura"]
tags: ["The Stanley Parable","Aventura","Steam"]
img: 'https://steamcdn-a.akamaihd.net/steam/apps/221910/header.jpg'
download: 'https://t.me/c/1118272073/11661'
---

# The Stanley Parable

{{< br >}}

Es un juego de exploración en primera persona. Jugarás como Stanley y no jugarás como Stanley. Seguirás una historia, no seguirás una historia. Tendrá una opción, no tendrá otra opción. El juego terminará, el juego nunca terminará.

La parábola de Stanley es un juego de exploración en primera persona. Jugarás como Stanley y no jugarás como Stanley. Seguirás una historia, no seguirás una historia. Tendrá una opción, no tendrá otra opción. El juego terminará, el juego nunca terminará. La contradicción sigue a la contradicción, las reglas de cómo deberían funcionar los juegos se rompen y luego se rompen nuevamente. Este mundo no fue hecho para que lo entiendas.

Pero a medida que exploras, lentamente, el significado comienza a surgir, las paradojas pueden comenzar a tener sentido, tal vez eres poderoso después de todo. El juego no está aquí para pelear contigo; te está invitando a bailar.

Basado en el galardonado mod 2011 Source del mismo nombre, The Stanley Parable regresa con nuevo contenido, nuevas ideas, una nueva capa de pintura visual y el impresionante trabajo de voz de Kevan Brighting. Para una comprensión más completa y profunda de lo que es la parábola de Stanley, pruebe la demostración gratuita.

{{< br >}}
{{< br >}}

## Requisitos del Sistema

{{< br >}}

* SO: Ubuntu 12.04
* Procesador: Dual core from Intel or AMD at 2.8 GHz
* Memoria: 2 GB de RAM
* Gráficos: nVidia GeForce 8600/9600GT, ATI/AMD Radeon HD2600/3600 (Graphic Drivers: nVidia 310, AMD 12.11), OpenGL 2.1
* Almacenamiento: 4 GB de espacio disponible
* Tarjeta de sonido: OpenAL Compatible Sound Card

{{< br >}}
{{< br >}}
